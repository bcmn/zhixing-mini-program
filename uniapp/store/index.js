import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,
		userInfo: {},
		cityInfo: {
			// id: city_id,
			name: '山东',
		},
	},
	mutations: {
		login(state, provider) {

			state.hasLogin = true;
			state.userInfo = provider;
			uni.setStorage({ //缓存用户登陆状态
				key: 'userInfo',
				data: provider
			})
			console.log(state.userInfo);
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			uni.removeStorage({
				key: 'userInfo'
			})
		},
		setCityInfo(state, data) {
			state.cityInfo = data;
			uni.setStorageSync('cityInfo', data)
		},
		// 获取当前定位
		async initLocationFunc(state) {
			let this_ = this;
			console.log("获取地址");
			try {
				const [error, res] = await uni.getLocation({
					// #ifndef APP
					type: "gcj02",
					// #endif
				});
				console.log(error, res, "----");
				let loca = res.latitude + ',' + res.longitude
				uni.setStorageSync('lat', res.latitude)
				uni.setStorageSync('lng', res.longitude)
				// state.cityInfo.name = '临沂'
				uni.request({
					url: 'https://apis.map.qq.com/ws/geocoder/v1/?key=EABBZ-74X67-7GCXR-PNBQQ-22CWV-FRBVH&get_poi=1&poi_options=address_format=short;policy=4&location=' +
						loca,
					method: 'GET',
					success(res) {
						console.log(res)
						let data = res.data.result.address_component

						state.cityInfo.name = data.district
						uni.setStorageSync('cityInfo', {
							name: data.district
						})

					},
					fail(error) {
						console.log(error);
					}
				});
				// #ifdef MP
				if (!res) return store.commit("getAuthorize");
				// #endif
				if (error) {
					uni.showModal({
						title: "温馨提示",
						// content: JSON.stringify(error.errMsg)
						content: "获取位置失败，请检查是否开启定位！",
					});
					return;
				}
				// #ifdef APP-PLUS

				if (!res) return toast({
					title: "获取位置失败"
				});
				// #endif
				// 获取逆地理
				// commit('setCityInfo', someParam)
			} catch (err) {
				console.log(err);
				// toast({ title: err.errMsg })
				// throw new Error(err + '获取位置')
			}
		},
		// 位置授权
		async getAuthorize({
			dispatch
		}) {
			const [error, data] = await uni.showModal({
				title: "您已拒绝地理位置权限",
				content: "是否进入权限管理，调整授权？",
			});
			if (data.confirm) {
				const [error, data] = await uni.openSetting();
				if (data.authSetting["scope.userLocation"]) store.commit("initLocationFunc");
			}

		},
	},
	actions: {

	}
})

export default store