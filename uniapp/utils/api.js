import __config from 'config/env';
const request = (url, method, data, showLoading) => {
	return new Promise((resolve, reject) => {
		if (showLoading) {
			uni.showLoading({
				title: '加载中'
			});
		}
		let sendData = {};
		if (data) {
			sendData = data
		}
		if (uni.getStorageSync('lng') && sendData) {
			sendData.lng = uni.getStorageSync('lng')
		}
		if (uni.getStorageSync('lat') && sendData) {
			sendData.lat = uni.getStorageSync('lat')
		}
		console.log(sendData)
		uni.request({
			url: __config.basePath + url,
			method: method,
			data: sendData,
			withCredentials: true,
			header: {
				'token': uni.getStorageSync('userInfo') ? uni.getStorageSync('userInfo').token : '',
			},
			success(res) {
				if (res.data.code == 1) {
					resolve(res.data);
				} else if (res.statusCode == 404) {
					uni.showModal({
						title: '提示',
						content: '接口请求出错，请检查手机网络',
						success(res) {}
					});

					reject();
				} else if (res.statusCode == 502) {
					console.log(502)
					uni.showModal({
						title: '提示',
						content: '服务启维护中，请稍后再来',
						success(res) {}
					});
					reject();
				} else if (res.statusCode == 503) {
					console.log(503)
					uni.showModal({
						title: '提示',
						content: '503错误，服务未启动',
						success(res) {}
					});
					reject();
				} else {
					if (res.data.code == -1) {
						uni.showModal({
							title: '提示',
							content: res.data.msg,
							confirmText: '跳转登录',
							cancelText: '跳转首页',
							success(res) {
								if (res.confirm) {
									uni.navigateTo({
										url: "/pages/public/login"
									})
								} else if (res.cancel) {
									uni.switchTab({
										url: '/pages/index/index',
										success(result) {}
									})
								}
							}
						});
						// setTimeout(function () {
						// 	uni.hideLoading();
						// }, 1000);

						return;
					}
					console.log(res.data.msg)
					if (res.data.msg == '用户未登录，请先登录') {
						uni.showModal({
							title: '提示',
							content: res.data.msg,
							confirmText: '跳转登录',
							cancelText: '跳转首页',
							success(res) {
								if (res.confirm) {
									uni.redirectTo({
										url: "/pages/public/login",
										success(result) {
											uni.hideLoading()
										}
									})

								} else if (res.cancel) {
									uni.switchTab({
										url: '/pages/index/index',
										success(result) {
											uni.hideLoading()
										}
									})
								}
							}
						});
						setTimeout(function() {
							uni.hideLoading();
						}, 1000);

						return
					}
					uni.showModal({
						title: '提示',
						content: res.data.msg,
						success(res) {}
					});
					reject();
				}
			},
			fail(error) {
				console.error(12321)
				uni.showModal({
					title: '提示',
					content: '接口请求出错：' + error.errMsg,
					success(res) {}
				});
				reject(error);
			},
			complete(res) {
				uni.hideLoading();
			}
		});
	});
};

module.exports = {
	__config,
	request,
	weChatLogin: data => {
		//微信小程序登录接口
		return request('/user/third', 'POST', data, false);
	},
	shop_lists: data => {
		return request('/shop.goods/lists', 'GET', data, false, true);
	},
	ordersOrderGoodsDetail: data => {
		return request('/orders/orderGoodsDetail', 'GET', data, false, true);
	},
	userCenter: data => {
		//获取用户中心数据
		return request('/user/center', 'POST', data, false);
	},
	upload: data => {
		//图片上传
		return request('/upload/image', 'POST', data, false);
	},
	getPhone: data => {
		//授权获取用户的手机号码
		return request('/login/getPhone', 'POST', data, false);
	},
	mnpLogin: data => {
		//微信小程序登录
		return request('/login/mnpLogin', 'POST', data, false);
	},
	indexData: data => {
		//首页内容
		return request('/index/index', 'GET', data, false);
	},
	goodsListMet: data => {
		//首页内容
		return request('/goods/lists', 'GET', data, false);
	},

	detail: data => {
		//商品详情
		return request('/goods/detail', 'GET', data, false);
	},
	favorite: data => {
		//添加/取消收藏
		return request('/favorite/add', 'POST', data, false);
	},
	favoriteList: data => {
		//收藏列表
		return request('/favorite/lists', 'GET', data, false);
	},
	favoriteDelete: data => {
		//删除收藏
		return request('/favorite/delete', 'POST', data, false);
	},
	addressList: data => {
		//获取用户的收货地址列表
		return request('/address/lists', 'get', data, false);
	},
	addressAdd: data => {
		//添加地址
		return request('/address/add', 'POST', data, false);
	},
	addressEdit: data => {
		//编辑地址
		return request('/address/edit', 'POST', data, false);
	},
	setDefault: data => {
		//设置默认地址
		return request('/address/setDefault', 'GET', data, false);
	},
	addressDelete: data => {
		//设置默认地址
		return request('/address/delete', 'POST', data, false);
	},


	addressDetail: data => {
		//地址详情
		return request('/address/detail', 'GET', data, false);
	},
	repairAdd: data => {
		//申请维修
		return request('/repair/add', 'POST', data, false);
	},
	repairDetail: data => {
		//维修申请详情
		return request('/repair/detail', 'POST', data, false);
	},
	refundExpress: data => {
		//退货寄回
		return request('/refund/express', 'POST', data, false);
	},
	refundFinish: data => {
		//用户确认回收
		return request('/orders_recovery/finish', 'POST', data, false);
	},
	repairList: data => {
		//维修申请详情
		return request('/repair/lists', 'POST', data, false);
	},

	addCart: data => {
		//加入购物车
		return request('/cart/add', 'POST', data, false);
	},
	cartLists: data => {
		//购物车列表
		return request('/cart/lists', 'GET', data, false);
	},
	cartListsreduce: data => {
		//减少购物车数量
		return request('/cart/cart/reduce', 'GET', data, false);
	},


	createOrder: data => {
		//创建订单
		return request('/orders/createOrder', 'POST', data, false);
	},
	confirmOrder: data => {
		//订单详情信息
		return request('/orders/confirmOrder', 'POST', data, false);
	},

	couponLists: data => {
		//获取用户的优惠券列表
		return request('/coupon/lists', 'GET', data, false);
	},
	ordersLists: data => {
		//获取我的订单列表
		return request('/orders/lists', 'GET', data, false);
	},
	prepay: data => {
		//获取支付参数
		return request('/pay/prepay', 'POST', data, false);
	},
	recharge: data => {
		//充值
		return request('/recharge/recharge', 'POST', data, false);
	},


	ordersDetail: data => {
		//订单详情
		return request('/orders/detail', 'GET', data, false);
	},
	ordersDetail2: data => {
		//设备预约订单详情
		return request('/orders_learn/detail', 'GET', data, false);
	},
	finish: data => {
		//设备预约订单核销
		return request('/orders_learn/finish', 'POST', data, false);
	},
	rechargeConfig: data => {
		//获取充值配置
		return request('/recharge/config', 'GET', data, false);
	},
	indexPolicy: data => {
		//获取协议内容
		return request('/index/policy', 'GET', data, false);
	},
	repairLists: data => {
		//获取维修申请列表
		return request('/repair/lists', 'GET', data, false);
	},
	repairDetail: data => {
		//维修申请详情
		return request('/repair/detail', 'GET', data, false);
	},

	articleDetail: data => {
		//获取文章详情
		return request('/article/detail', 'GET', data, false);
	},
	hospitalLists: data => {
		//获取合作医院列表
		return request('/hospital/lists', 'GET', data, false);
	},
	storeLists: data => {
		//获取门店列表
		return request('/store/lists', 'GET', data, false);
	},

	applyAdd: data => {
		//加盟申请
		return request('/apply/add', 'POST', data, false);
	},
	indexMall: data => {
		//商场接口
		return request('/index/mall', 'POST', data, false);
	},

	feedback: data => {
		//意见反馈
		return request('/feedback/add', 'POST', data, false);
	},

	devicesInfo: data => {
		//获取设备详情
		return request('/devices/detail', 'GET', data, false);
	},
	devicesActive: data => {
		//激活设备
		return request('/devices/active', 'POST', data, false);
	},

	indexConfig: data => {
		//配置
		return request('/index/config', 'POST', data, false);
	},
	accountLoglists: data => {
		//获取用户资产变动记录
		return request('/accountLog/lists', 'GET', data, false);
	},
	usersetInfo: data => {
		//修改用户咨料
		return request('/user/setInfo', 'POST', data, false);
	},
	addressSetDefault: data => {
		//设置默认地址
		return request('/address/setDefault', 'POST', data, false);
	},
	ordersFinish: data => {
		//确认收货
		return request('/orders/finish', 'POST', data, false);
	},
	feedback: data => {
		//意见反馈
		return request('/feedback/add', 'POST', data, false);
	},
	deviceslists: data => {
		//设备列表
		return request('/devices/lists', 'GET', data, false);
	},

	getQrcode: data => {
		//获取推广二维码
		return request('/user/getQrcode', 'GET', data, false);
	},

	storeDetail: data => {
		//门店详情
		return request('/store/detail', 'GET', data, false);
	},
	hospitalDetail: data => {
		//医院详情
		return request('/hospital/detail', 'GET', data, false);
	},
	bankLists: data => {
		//获取用户的提现账户列表
		return request('/bank/lists', 'GET', data, false);
	},
	withdrawApply: data => {
		//申请提现
		return request('/withdraw/apply', 'POST', data, false);
	},
	bankDelete: data => {
		//删除提现账户
		return request('/bank/delete', 'POST', data, false);
	},
	bankAdd: data => {
		//添加提现账户
		return request('/bank/add', 'POST', data, false);
	},
	withdrawLists: data => {
		//提现记录
		return request('/withdraw/lists', 'get', data, false);
	},
	AddCreateOrder: data => {
		//购买次数或使用时长
		return request('/ordersRenew/createOrder', 'POST', data, false);
	},
	devicesTransfer: data => {
		//设备转让
		return request('/devices/transfer', 'POST', data, false);
	},
	devicesCreateOrder: data => {
		//设备预约
		return request('/orders_learn/createOrder', 'POST', data, false);
	},
	ordersDevicesLists: data => {
		//设备预约订单列表
		return request('/orders_learn/lists', 'get', data, false);
	},
	orders_devicesDelete: data => {
		//删除设备预约订单
		return request('/orders_learn/delete', 'post', data, false);
	},
	orders_Delete: data => {
		//删除订单
		return request('/orders/delete', 'post', data, false);
	},

	refundApply: data => {
		//申请售后
		return request('/refund/apply', 'POST', data, false);
	},
	refundDetail: data => {
		//获取订单退款详情
		return request('/refund/detail', 'POST', data, false);
	},

	cartClear: data => {
		//清空购物车
		return request('/cart/clear', 'POST', data, false);
	},
	cartDelete: data => {
		//删除购物车数据
		return request('/cart/delete', 'POST', data, false);
	},
	orders_recoveryLists: data => {
		//设备回收订单列表
		return request('/orders_recovery/lists', 'GET', data, false);
	},
	orders_recoveryDetail: data => {
		//设备回收订单详情
		return request('/orders_recovery/detail', 'GET', data, false);
	},
	storeOrdersLists: data => {
		//销售订单列表
		return request('/store/ordersLists', 'GET', data, false);
	},
	storeCccountLogs: data => {
		//门店-财务管理
		return request('/store/accountLogs', 'GET', data, false);
	},

	orders_recoveryFinish: data => {
		//确认回收
		return request('/orders_recovery/finish', 'POST', data, false);
	},
	orders_recoveryCancel: data => {
		//拒绝回收
		return request('/orders_recovery/cancel', 'POST', data, false);
	},
	orders_rentCreateOrder: data => {
		//设备续租
		return request('/orders_rent/createOrder', 'POST', data, false);
	},
	storeEdit: data => {
		//修改门店资料
		return request('/store/edit', 'POST', data, false);
	},
	getRecommendLists: data => {
		//获取下级员工列表
		return request('/user/getRecommendLists', 'GET', data, false);
	},
	ordersCancel: data => {
		//取消订单
		return request('/orders/cancel', 'POST', data, false);
	},
	ordersExpress: data => {
		//获取订单物流轨迹
		return request('/orders/express', 'GET', data, false);
	},
	orders_learnCancel: data => {
		//取消订单
		return request('/orders_learn/cancel', 'POST', data, false);
	},
	logsLists: data => {
		//获取订单物流轨迹
		return request('/repair/logsLists', 'GET', data, false);
	},
	addDevices: data => {
		// 设备入库
		return request('/devices/add', 'POST', data, false);
	}

};