import { baseUrl } from '@/api/http.js'
import axios from '../api/config'
const API_BASE = baseUrl
// const API_BASE = '' //Vue.config.js 里跨域处理所需要地址

export default {
  // 首页-轮播
  // banner_list(data) {
  //   return axios.post(`${API_BASE}/api/Index/banner_list`, data)
  // },
  config_info(data) {
    return axios.get(`${API_BASE}/site/config`, data)
  },
  banner_list(data) {
    return axios.get(`${API_BASE}/site/index`, data)
  },
  about(data) {
    return axios.get(`${API_BASE}/site/about`, data)
  },
  send(data) {
    return axios.post(`${API_BASE}/sms/smsCode`, data)
  },

  login(data) {
    return axios.post(`${API_BASE}/login/account`, data)
  },
  goodsLists(data) {
    return axios.get(`${API_BASE}/goods/lists`, { params: data })
  },
  favoriteLists(data) {
    return axios.get(`${API_BASE}/favorite/lists`, { params: data })
  },
  ordersLists(data) {
    return axios.get(`${API_BASE}/orders/lists`, { params: data })
  },
  ordersDetail(data) {
    return axios.get(`${API_BASE}/orders/detail`, { params: data })
  },
  orders_Delete(data) {
    return axios.post(`${API_BASE}/orders/delete`, data)
  },
  ordersCancel(data) {
    return axios.post(`${API_BASE}/orders/cancel`, data)
  },
  detail(data) {
    return axios.get(`${API_BASE}/goods/detail`, { params: data })
  },
  favorite(data) {
    return axios.post(`${API_BASE}/favorite/add`, data)
  },
  cartLists(data) {
    return axios.get(`${API_BASE}/cart/lists`, { params: data })
  },
  addressList(data) {
    return axios.get(`${API_BASE}/address/lists`, { params: data })
  },
  addressDetail(data) {
    return axios.get(`${API_BASE}/address/detail`, { params: data })
  },
  addressAdd(data) {
    return axios.post(`${API_BASE}/address/add`, data)
  },
  addressEdit(data) {
    return axios.post(`${API_BASE}/address/edit`, data)
  },
  addressDelete(data) {
    return axios.post(`${API_BASE}/address/delete`, data)
  },
  addressSetDefault(data) {
    return axios.post(`${API_BASE}/address/setDefault`, data)
  },
  applyAdd(data) {
    return axios.post(`${API_BASE}/apply/add`, data)
  },
  userCenter(data) {
    return axios.post(`${API_BASE}/user/center`, data)
  },
  confirmOrder(data) {
    return axios.post(`${API_BASE}/orders/confirmOrder`, data)
  },
  addCart(data) {
    return axios.post(`${API_BASE}/cart/add`, data)
  },
  couponLists(data) {
    return axios.get(`${API_BASE}/coupon/lists`, { params: data })
  },
  createOrder(data) {
    return axios.post(`${API_BASE}/orders/createOrder`, data)
  },
  prepay(data) {
    return axios.post(`${API_BASE}/pay/prepay`, data)
  },
  cartDelete(data) {
    return axios.post(`${API_BASE}/cart/delete`, data)
  },
}
