const TerserPlugin = require('terser-webpack-plugin')
module.exports = {
  outputDir: 'web',
  assetsDir: 'static', //  outputDir的静态资源(js、css、img、fonts)目录
  // publicPath: './', // 静态资源路径（默认/，如果不改打包后会白屏）
  publicPath:'/web',
  devServer: {
    host: 'localhost',
    port: 8080,
    // 跨域
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: 'http://jxtx.zhousi.hdlkeji.com', //要跨域的地址，接口对接使用
        changeOrigin: true, // 允许跨域
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: '',
        },
        ws: false,
      },
    },
  },
  transpileDependencies: true,
  lintOnSave: false,
  //生产环境是不需要sourceMap的，如下配置可以去除
  //问题： vue项目打包之后js文件夹中，会自动生成一些map文件，占用相当一部分空间
  //sourceMap资源映射文件，存的是打包前后的代码位置，方便开发使用，这个占用相当一部分空间。
  //map文件的作用在于：项目打包后，代码都是经过压缩加密的，如果运行时报错，输出的错误信息无法准确得知是哪里的代码报错，有了map就可以像未加密的代码一样，准确的输出是哪一行哪一列有错。
  productionSourceMap: false, //清除sourceMap（可以减小一半大小如：压缩包7M，使用后3M多）

  //   打包后禁止在控制台输出console.log
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push(
        new TerserPlugin({
          terserOptions: {
            ecma: undefined,
            warnings: false,
            parse: {},
            compress: {
              drop_console: true,
              drop_debugger: false,
              pure_funcs: ['console.log'], // 移除console
            },
          },
        })
      )
    }
  },
}
