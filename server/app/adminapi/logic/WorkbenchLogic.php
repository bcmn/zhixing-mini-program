<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\adminapi\logic;


use app\common\enum\orders\OrdersEnum;
use app\common\enum\user\UserApplyEnum;
use app\common\logic\BaseLogic;
use app\common\model\orders\Orders;
use app\common\model\user\User;
use app\common\model\user\UserApply;
use app\common\service\ConfigService;
use app\common\service\FileService;


/**
 * 工作台
 * Class WorkbenchLogic
 * @package app\adminapi\logic
 */
class WorkbenchLogic extends BaseLogic
{
    /**
     * @notes 工作套
     * @param $adminInfo
     * @return array
     * @author 段誉
     * @date 2021/12/29 15:58
     */
    public static function index()
    {
        return [
            // 版本信息
            'version' => self::versionInfo(),
            // 今日数据
            'today' => self::today(),
            // 全部数据
            'total' => self::total(),
            // 近15日访客数
            'visitor' => self::visitor(),
            // 服务支持
            'support' => self::support()
        ];
    }


    /**
     * @notes 常用功能
     * @return array[]
     * @author 段誉
     * @date 2021/12/29 16:40
     */
    public static function menu(): array
    {
        return [
            [
                'name' => '管理员',
                'image' => FileService::getFileUrl(config('project.default_image.menu_admin')),
                'url' => '/permission/admin'
            ],
            [
                'name' => '角色管理',
                'image' => FileService::getFileUrl(config('project.default_image.menu_role')),
                'url' => '/permission/role'
            ],
            [
                'name' => '部门管理',
                'image' => FileService::getFileUrl(config('project.default_image.menu_dept')),
                'url' => '/organization/department'
            ],
            [
                'name' => '字典管理',
                'image' => FileService::getFileUrl(config('project.default_image.menu_dict')),
                'url' => '/dev_tools/dict'
            ],
            [
                'name' => '代码生成器',
                'image' => FileService::getFileUrl(config('project.default_image.menu_generator')),
                'url' => '/dev_tools/code'
            ],
            [
                'name' => '素材中心',
                'image' => FileService::getFileUrl(config('project.default_image.menu_file')),
                'url' => '/material/index'
            ],
            [
                'name' => '菜单权限',
                'image' => FileService::getFileUrl(config('project.default_image.menu_auth')),
                'url' => '/permission/menu'
            ],
            [
                'name' => '网站信息',
                'image' => FileService::getFileUrl(config('project.default_image.menu_web')),
                'url' => '/setting/website/information'
            ],
        ];
    }


    /**
     * @notes 版本信息
     * @return array
     * @author 段誉
     * @date 2021/12/29 16:08
     */
    public static function versionInfo(): array
    {
        return [
            'version' => config('project.version'),
            'website' => config('project.website.url'),
            'name' => ConfigService::get('website', 'name'),
            'based' => 'vue3.x、ElementUI、MySQL',
            'channel' => [
                'website' => 'https://www.likeadmin.cn',
                'gitee' => 'https://gitee.com/likeadmin/likeadmin_php',
            ]
        ];
    }


    /**
     * @notes 今日数据
     * @return int[]
     * @date 2021/12/29 16:15
     */
    public static function today(): array
    {
        return [
            'time' => date('Y-m-d H:i:s'),
            // 订单量
            'orders_num' => Orders::whereTime('create_time', 'today')->count(),
            // 总销售额
            'sales_num' => Orders::whereTime('create_time', 'today')->sum('order_amount'),
            // 今日新增用户量
            'user_num' => User::whereTime('create_time', 'today')->count(),
            // 门店
            'store_num' => UserApply::where('status', '=', UserApplyEnum::STATUS_PASS)->where('type', '=', 1)->whereTime('create_time', 'today')->count(),
            // 合伙人
            'partner_num' => UserApply::where('status', '=', UserApplyEnum::STATUS_PASS)->where('type', '=', 2)->whereTime('create_time', 'today')->count()
        ];
    }

    /**
     * @notes 总数据
     * @return int[]
     * @date 2021/12/29 16:15
     */
    public static function total(): array
    {
        return [
            'time' => date('Y-m-d H:i:s'),
            // 订单量
            'orders_num' => Orders::count(),
            // 总销售额
            'sales_num' => Orders::sum('order_amount'),
            // 今日新增用户量
            'user_num' => User::count(),
            // 门店
            'store_num' => UserApply::where('status', '=', UserApplyEnum::STATUS_PASS)->where('type', '=', 40)->count(),
            // 合伙人
            'partner_num' => UserApply::where('status', '=', UserApplyEnum::STATUS_PASS)->where('type', '=', 30)->count()
        ];
    }


    /**
     * @notes 访问数
     * @return array
     * @author 段誉
     * @date 2021/12/29 16:57
     */
    public static function visitor(): array
    {
        $num = [];
        $date = [];
        for ($i = 0; $i < 15; $i++) {
            $where_start = strtotime("- " . $i . "day");
            $date[$i] = date('Y/m/d', $where_start);
            $start_time = strtotime($date[$i] . ' 00:00:00');
            $end_time = strtotime($date[$i] . ' 23:59:59');
            $num[$i] = Orders::where('status', '=', OrdersEnum::STATUS_FINISH)->whereBetween('create_time', [$start_time, $end_time])->sum('order_amount');
        }

        return [
            'date' => $date,
            'list' => [
                ['name' => '销售额', 'data' => $num]
            ]
        ];
    }


    /**
     * @notes 服务支持
     * @return array[]
     * @author 段誉
     * @date 2022/7/18 11:18
     */
    public static function support()
    {
        return [
            [
                'image' => FileService::getFileUrl(config('project.default_image.qq_group')),
                'title' => '官方公众号',
                'desc' => '关注官方公众号',
            ],
            [
                'image' => FileService::getFileUrl(config('project.default_image.customer_service')),
                'title' => '添加企业客服微信',
                'desc' => '想了解更多请添加客服',
            ]
        ];
    }

}