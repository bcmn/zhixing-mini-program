<?php

namespace app\adminapi\logic\advertise;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\advertise\Advertise;

/**
 * 广告逻辑
 * Class AdvertiseLogic
 */
class AdvertiseLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Advertise::create([
                'title' => $params['title'],
                'image' => $params['image'] ?? '',
                'type' => $params['type'] ?? '',
                'cate_id' => $params['cate_id'] ?? 0,
                'url' => $params['url'] ?? '',
                'url_type' => $params['url_type'] ?? '',
                'content' => $params['content'] ?? '',
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Advertise::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'image' => $params['image'] ?? '',
                'type' => $params['type'] ?? '',
                'cate_id' => $params['cate_id'] ?? 0,
                'url' => $params['url'] ?? '',
                'url_type' => $params['url_type'] ?? '',
                'content' => $params['content'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Advertise::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Advertise::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Advertise::findOrEmpty($params['id'])->toArray();
    }

}