<?php

namespace app\adminapi\logic\advertise;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\advertise\AdvertiseCate;

/**
 * 广告类目逻辑
 * Class AdvertiseCateLogic
 */
class AdvertiseCateLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            AdvertiseCate::create([
                'title' => $params['title'],
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            AdvertiseCate::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        AdvertiseCate::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }


    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        AdvertiseCate::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return AdvertiseCate::findOrEmpty($params['id'])->toArray();
    }

    /**
     * @return mixed
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData(): array
    {
        return AdvertiseCate::where(['status' => DefaultEnum::SHOW])
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
    }
}