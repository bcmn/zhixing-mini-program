<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\adminapi\logic\setting\web;


use app\common\logic\BaseLogic;
use app\common\service\ConfigService;
use app\common\service\FileService;


/**
 * 网站设置
 * Class WebSettingLogic
 * @package app\adminapi\logic\setting
 */
class WebSettingLogic extends BaseLogic
{

    /**
     * @notes 获取网站信息
     * @return array
     * @author 段誉
     * @date 2021/12/28 15:43
     */
    public static function getWebsiteInfo(): array
    {
        return [
            'name' => ConfigService::get('website', 'name'),
            'web_favicon' => FileService::getFileUrl(ConfigService::get('website', 'web_favicon')),
            'web_logo' => FileService::getFileUrl(ConfigService::get('website', 'web_logo')),
            'login_image' => FileService::getFileUrl(ConfigService::get('website', 'login_image')),
            'shop_name' => ConfigService::get('website', 'shop_name'),
            'shop_logo' => FileService::getFileUrl(ConfigService::get('website', 'shop_logo')),
            'share_image' => FileService::getFileUrl(ConfigService::get('website', 'share_image')),
            'join_image' => FileService::getFileUrl(ConfigService::get('website', 'join_image')),

            'pc_logo' => FileService::getFileUrl(ConfigService::get('website', 'pc_logo')),
            'pc_title' => ConfigService::get('website', 'pc_title', ''),
            'pc_ico' => FileService::getFileUrl(ConfigService::get('website', 'pc_ico')),
            'pc_desc' => ConfigService::get('website', 'pc_desc', ''),
            'pc_keywords' => ConfigService::get('website', 'pc_keywords', ''),
            'delivery_name' => ConfigService::get('website', 'delivery_name', ''),
            'delivery_mobile' => ConfigService::get('website', 'delivery_mobile', ''),
            'delivery_address' => ConfigService::get('website', 'delivery_address', ''),
        ];
    }


    /**
     * @notes 设置网站信息
     * @param array $params
     * @author 段誉
     * @date 2021/12/28 15:43
     */
    public static function setWebsiteInfo(array $params)
    {
        $favicon = FileService::setFileUrl($params['web_favicon']);
        $logo = FileService::setFileUrl($params['web_logo']);
        $login = FileService::setFileUrl($params['login_image']);
        $shopLogo = FileService::setFileUrl($params['shop_logo']);
        $pcLogo = FileService::setFileUrl($params['pc_logo']);
        $pcIco = FileService::setFileUrl($params['pc_ico'] ?? '');
        $share_image = FileService::setFileUrl($params['share_image'] ?? '');
        $join_image = FileService::setFileUrl($params['join_image'] ?? '');

        ConfigService::set('website', 'name', $params['name']);
        ConfigService::set('website', 'web_favicon', $favicon);
        ConfigService::set('website', 'web_logo', $logo);
        ConfigService::set('website', 'login_image', $login);
        ConfigService::set('website', 'shop_name', $params['shop_name']);
        ConfigService::set('website', 'shop_logo', $shopLogo);
        ConfigService::set('website', 'pc_logo', $pcLogo);
        ConfigService::set('website', 'share_image', $share_image);
        ConfigService::set('website', 'join_image', $join_image);

        ConfigService::set('website', 'pc_title', $params['pc_title']);
        ConfigService::set('website', 'pc_ico', $pcIco);
        ConfigService::set('website', 'pc_desc', $params['pc_desc'] ?? '');
        ConfigService::set('website', 'pc_keywords', $params['pc_keywords'] ?? '');
        ConfigService::set('website', 'delivery_name', $params['delivery_name'] ?? '');
        ConfigService::set('website', 'delivery_mobile', $params['delivery_mobile'] ?? '');
        ConfigService::set('website', 'delivery_address', $params['delivery_address'] ?? '');
    }


    /**
     * @notes 获取版权备案
     * @return array
     * @author 段誉
     * @date 2021/12/28 16:09
     */
    public static function getCopyright(): array
    {
        return ConfigService::get('copyright', 'config', []);
    }


    /**
     * @notes 设置版权备案
     * @param array $params
     * @return bool
     * @author 段誉
     * @date 2022/8/8 16:33
     */
    public static function setCopyright(array $params)
    {
        try {
            if (!is_array($params['config'])) {
                throw new \Exception('参数异常');
            }
            ConfigService::set('copyright', 'config', $params['config'] ?? []);
            return true;
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
    }


    /**
     * @notes 设置政策协议
     * @param array $params
     * @author ljj
     * @date 2022/2/15 10:59 上午
     */
    public static function setAgreement(array $params)
    {
        $serviceContent = clear_file_domain($params['service_content'] ?? '');
        $privacyContent = clear_file_domain($params['privacy_content'] ?? '');
        $aboutContent = clear_file_domain($params['about_content'] ?? '');
        $rechargeContent = clear_file_domain($params['recharge_content'] ?? '');
        $partnerContent = clear_file_domain($params['partner_content'] ?? '');
        $storeContent = clear_file_domain($params['store_content'] ?? '');
        ConfigService::set('agreement', 'service_title', $params['service_title'] ?? '');
        ConfigService::set('agreement', 'service_content', $serviceContent);
        ConfigService::set('agreement', 'privacy_title', $params['privacy_title'] ?? '');
        ConfigService::set('agreement', 'privacy_content', $privacyContent);
        ConfigService::set('agreement', 'about_title', $params['about_title'] ?? '');
        ConfigService::set('agreement', 'about_content', $aboutContent);
        ConfigService::set('agreement', 'recharge_title', $params['recharge_title'] ?? '');
        ConfigService::set('agreement', 'recharge_content', $rechargeContent);
        ConfigService::set('agreement', 'partner_title', $params['partner_title'] ?? '');
        ConfigService::set('agreement', 'partner_content', $partnerContent);
        ConfigService::set('agreement', 'store_title', $params['store_title'] ?? '');
        ConfigService::set('agreement', 'store_content', $storeContent);
    }


    /**
     * @notes 获取政策协议
     * @return array
     * @author ljj
     * @date 2022/2/15 11:15 上午
     */
    public static function getAgreement(): array
    {
        $config = [
            'service_title' => ConfigService::get('agreement', 'service_title'),
            'service_content' => ConfigService::get('agreement', 'service_content'),
            'privacy_title' => ConfigService::get('agreement', 'privacy_title'),
            'privacy_content' => ConfigService::get('agreement', 'privacy_content'),
            'about_title' => ConfigService::get('agreement', 'about_title'),
            'about_content' => ConfigService::get('agreement', 'about_content'),
            'recharge_title' => ConfigService::get('agreement', 'recharge_title'),
            'recharge_content' => ConfigService::get('agreement', 'recharge_content'),
            'partner_title' => ConfigService::get('agreement', 'partner_title'),
            'partner_content' => ConfigService::get('agreement', 'partner_content'),
            'store_title' => ConfigService::get('agreement', 'store_title'),
            'store_content' => ConfigService::get('agreement', 'store_content'),
        ];

        $config['service_content'] = get_file_domain($config['service_content']);
        $config['privacy_content'] = get_file_domain($config['privacy_content']);
        $config['about_content'] = get_file_domain($config['about_content']);
        $config['recharge_content'] = get_file_domain($config['recharge_content']);
        $config['partner_content'] = get_file_domain($config['partner_content']);
        $config['store_content'] = get_file_domain($config['store_content']);

        return $config;
    }


    /**
     * @notes 获取网站信息
     * @return array
     * @author 段誉
     * @date 2021/12/28 15:43
     */
    public static function getSiteInfo(): array
    {
        return [
            'name' => ConfigService::get('site', 'name'),
            'web_favicon' => FileService::getFileUrl(ConfigService::get('site', 'web_favicon')),
            'web_logo' => FileService::getFileUrl(ConfigService::get('site', 'web_logo')),
            'about_logo' => FileService::getFileUrl(ConfigService::get('site', 'about_logo')),
            'about_content' => get_file_domain(ConfigService::get('site', 'about_content')),
            'shop_name' => ConfigService::get('site', 'shop_name'),

            'brand_title' => ConfigService::get('site', 'brand_title'),
            'brand_image1' => FileService::getFileUrl(ConfigService::get('site', 'brand_image1')),
            'brand_image2' => FileService::getFileUrl(ConfigService::get('site', 'brand_image2')),
            'brand_image3' => FileService::getFileUrl(ConfigService::get('site', 'brand_image3')),
            'brand_image4' => FileService::getFileUrl(ConfigService::get('site', 'brand_image4')),
            'brand_text1' => ConfigService::get('site', 'brand_text1'),
            'brand_text2' => ConfigService::get('site', 'brand_text2'),
            'brand_text3' => ConfigService::get('site', 'brand_text3'),
            'brand_text4' => ConfigService::get('site', 'brand_text4'),
            'brand_note1' => ConfigService::get('site', 'brand_note1'),
            'brand_note2' => ConfigService::get('site', 'brand_note2'),
            'brand_note3' => ConfigService::get('site', 'brand_note3'),
            'brand_note4' => ConfigService::get('site', 'brand_note4'),

            'service_title' => ConfigService::get('site', 'service_title'),
            'service_image1' => FileService::getFileUrl(ConfigService::get('site', 'service_image1')),
            'service_image2' => FileService::getFileUrl(ConfigService::get('site', 'service_image2')),
            'service_image3' => FileService::getFileUrl(ConfigService::get('site', 'service_image3')),
            'service_image4' => FileService::getFileUrl(ConfigService::get('site', 'service_image4')),
            'service_text1' => ConfigService::get('site', 'service_text1'),
            'service_text2' => ConfigService::get('site', 'service_text2'),
            'service_text3' => ConfigService::get('site', 'service_text3'),
            'service_text4' => ConfigService::get('site', 'service_text4'),

            'honor_title' => ConfigService::get('site', 'honor_title'),
            'honor_num1' => ConfigService::get('site', 'honor_num1'),
            'honor_num2' => ConfigService::get('site', 'honor_num2'),
            'honor_num3' => ConfigService::get('site', 'honor_num3'),
            'honor_num4' => ConfigService::get('site', 'honor_num4'),
            'honor_text1' => ConfigService::get('site', 'honor_text1'),
            'honor_text2' => ConfigService::get('site', 'honor_text2'),
            'honor_text3' => ConfigService::get('site', 'honor_text3'),
            'honor_text4' => ConfigService::get('site', 'honor_text4'),

            'culture_title' => ConfigService::get('site', 'culture_title'),
            'culture_image1' => FileService::getFileUrl(ConfigService::get('site', 'culture_image1')),
            'culture_image2' => FileService::getFileUrl(ConfigService::get('site', 'culture_image2')),
            'culture_image3' => FileService::getFileUrl(ConfigService::get('site', 'culture_image3')),
            'culture_image4' => FileService::getFileUrl(ConfigService::get('site', 'culture_image4')),
            'culture_image5' => FileService::getFileUrl(ConfigService::get('site', 'culture_image5')),
            'culture_image6' => FileService::getFileUrl(ConfigService::get('site', 'culture_image6')),
            'culture_text1' => ConfigService::get('site', 'culture_text1'),
            'culture_text2' => ConfigService::get('site', 'culture_text2'),
            'culture_text3' => ConfigService::get('site', 'culture_text3'),
            'culture_text4' => ConfigService::get('site', 'culture_text4'),
            'culture_text5' => ConfigService::get('site', 'culture_text5'),
            'culture_text6' => ConfigService::get('site', 'culture_text6'),
            'culture_note1' => ConfigService::get('site', 'culture_note1'),
            'culture_note2' => ConfigService::get('site', 'culture_note2'),
            'culture_note3' => ConfigService::get('site', 'culture_note3'),
            'culture_note4' => ConfigService::get('site', 'culture_note4'),
            'culture_note5' => ConfigService::get('site', 'culture_note5'),
            'culture_note6' => ConfigService::get('site', 'culture_note6'),

            'contact_title' => ConfigService::get('site', 'contact_title'),
            'contact_image1' => FileService::getFileUrl(ConfigService::get('site', 'contact_image1')),
            'contact_image2' => FileService::getFileUrl(ConfigService::get('site', 'contact_image2')),
            'contact_text1' => ConfigService::get('site', 'contact_text1'),
            'contact_text2' => ConfigService::get('site', 'contact_text2'),
            'contact_note1' => ConfigService::get('site', 'contact_note1'),
            'contact_note2' => ConfigService::get('site', 'contact_note2'),

            'footer_qrcode' => FileService::getFileUrl(ConfigService::get('site', 'footer_qrcode')),
            'copyright' => ConfigService::get('site', 'copyright'),
            'icp' => ConfigService::get('site', 'icp'),
        ];
    }


    /**
     * @notes 设置网站信息
     * @param array $params
     * @author 段誉
     * @date 2021/12/28 15:43
     */
    public static function setSiteInfo(array $params)
    {
        ConfigService::set('site', 'name', $params['name']);
        ConfigService::set('site', 'web_favicon', FileService::setFileUrl($params['web_favicon'] ?? ''));
        ConfigService::set('site', 'web_logo', FileService::setFileUrl($params['web_logo'] ?? ''));
        ConfigService::set('site', 'about_logo', FileService::setFileUrl($params['about_logo'] ?? ''));
        ConfigService::set('site', 'about_content', clear_file_domain($params['about_content'] ?? ''));

        ConfigService::set('site', 'brand_title', $params['brand_title']);
        ConfigService::set('site', 'brand_image1', FileService::setFileUrl($params['brand_image1'] ?? ''));
        ConfigService::set('site', 'brand_image2', FileService::setFileUrl($params['brand_image2'] ?? ''));
        ConfigService::set('site', 'brand_image3', FileService::setFileUrl($params['brand_image3'] ?? ''));
        ConfigService::set('site', 'brand_image4', FileService::setFileUrl($params['brand_image4'] ?? ''));
        ConfigService::set('site', 'brand_text1', $params['brand_text1']);
        ConfigService::set('site', 'brand_text2', $params['brand_text2']);
        ConfigService::set('site', 'brand_text3', $params['brand_text3']);
        ConfigService::set('site', 'brand_text4', $params['brand_text4']);
        ConfigService::set('site', 'brand_note1', $params['brand_note1']);
        ConfigService::set('site', 'brand_note2', $params['brand_note2']);
        ConfigService::set('site', 'brand_note3', $params['brand_note3']);
        ConfigService::set('site', 'brand_note4', $params['brand_note4']);

        ConfigService::set('site', 'service_title', $params['service_title']);
        ConfigService::set('site', 'service_image1', FileService::setFileUrl($params['service_image1'] ?? ''));
        ConfigService::set('site', 'service_image2', FileService::setFileUrl($params['service_image2'] ?? ''));
        ConfigService::set('site', 'service_image3', FileService::setFileUrl($params['service_image3'] ?? ''));
        ConfigService::set('site', 'service_image4', FileService::setFileUrl($params['service_image4'] ?? ''));
        ConfigService::set('site', 'service_text1', $params['service_text1']);
        ConfigService::set('site', 'service_text2', $params['service_text2']);
        ConfigService::set('site', 'service_text3', $params['service_text3']);
        ConfigService::set('site', 'service_text4', $params['service_text4']);

        ConfigService::set('site', 'honor_title', $params['honor_title']);
        ConfigService::set('site', 'honor_num1', $params['honor_num1']);
        ConfigService::set('site', 'honor_num2', $params['honor_num2']);
        ConfigService::set('site', 'honor_num3', $params['honor_num3']);
        ConfigService::set('site', 'honor_num4', $params['honor_num4']);
        ConfigService::set('site', 'honor_text1', $params['honor_text1']);
        ConfigService::set('site', 'honor_text2', $params['honor_text2']);
        ConfigService::set('site', 'honor_text3', $params['honor_text3']);
        ConfigService::set('site', 'honor_text4', $params['honor_text4']);

        ConfigService::set('site', 'culture_title', $params['culture_title']);
        ConfigService::set('site', 'culture_image1', FileService::setFileUrl($params['culture_image1'] ?? ''));
        ConfigService::set('site', 'culture_image2', FileService::setFileUrl($params['culture_image2'] ?? ''));
        ConfigService::set('site', 'culture_image3', FileService::setFileUrl($params['culture_image3'] ?? ''));
        ConfigService::set('site', 'culture_image4', FileService::setFileUrl($params['culture_image4'] ?? ''));
        ConfigService::set('site', 'culture_image5', FileService::setFileUrl($params['culture_image5'] ?? ''));
        ConfigService::set('site', 'culture_image6', FileService::setFileUrl($params['culture_image6'] ?? ''));
        ConfigService::set('site', 'culture_text1', $params['culture_text1']);
        ConfigService::set('site', 'culture_text2', $params['culture_text2']);
        ConfigService::set('site', 'culture_text3', $params['culture_text3']);
        ConfigService::set('site', 'culture_text4', $params['culture_text4']);
        ConfigService::set('site', 'culture_text5', $params['culture_text5']);
        ConfigService::set('site', 'culture_text6', $params['culture_text6']);
        ConfigService::set('site', 'culture_note1', $params['culture_note1']);
        ConfigService::set('site', 'culture_note2', $params['culture_note2']);
        ConfigService::set('site', 'culture_note3', $params['culture_note3']);
        ConfigService::set('site', 'culture_note4', $params['culture_note4']);
        ConfigService::set('site', 'culture_note5', $params['culture_note5']);
        ConfigService::set('site', 'culture_note6', $params['culture_note6']);

        ConfigService::set('site', 'contact_title', $params['contact_title']);
        ConfigService::set('site', 'contact_image1', FileService::setFileUrl($params['contact_image1'] ?? ''));
        ConfigService::set('site', 'contact_image2', FileService::setFileUrl($params['contact_image2'] ?? ''));
        ConfigService::set('site', 'contact_text1', $params['contact_text1']);
        ConfigService::set('site', 'contact_text2', $params['contact_text2']);
        ConfigService::set('site', 'contact_note1', $params['contact_note1']);
        ConfigService::set('site', 'contact_note2', $params['contact_note2']);
        ConfigService::set('site', 'footer_qrcode', FileService::setFileUrl($params['footer_qrcode'] ?? ''));
        ConfigService::set('site', 'copyright', $params['copyright']);
        ConfigService::set('site', 'icp', $params['icp']);
    }
}