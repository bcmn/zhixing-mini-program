<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\adminapi\logic\finance;


use app\common\enum\RefundEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\orders\Orders;
use app\common\model\refund\RefundLog;
use app\common\model\refund\RefundRecord;
use app\common\model\user\User;
use app\common\service\pay\WeChatPayService;


/**
 * 退款
 * Class RefundLogic
 * @package app\adminapi\logic\finance
 */
class RefundLogic extends BaseLogic
{

    public static function confirm($params): bool
    {
        $records = RefundRecord::where('id', '=', $params['id'])->findOrEmpty();
        $refund_status = '';
        switch ($params['status']) {
            case 1:
                $refund_status = $records['refund_type'] == RefundEnum::TYPE_MONEY ? RefundEnum::REFUND_PAY : RefundEnum::REFUND_CONFIRM;
                break;
            case 3:
                $refund_status = RefundEnum::REFUND_PAY;
                break;
            case 4:
                $refund_status = RefundEnum::REFUND_SUCCESS;
                break;
            case 5:
                $refund_status = RefundEnum::REFUND_ERROR;
                break;
        }


        if (!$records->isEmpty() && $refund_status) {
            $records->refund_status = $refund_status;
            $records->reason = $params['reason'] ?? '';
            $records->save();

            if ($refund_status == RefundEnum::REFUND_SUCCESS) {
                //执行退款
                if ($records['refund_type'] == RefundEnum::REFUND_WEIXIN) {
                    //微信退款
                    $payService = (new WeChatPayService(1, $records['user_id'] ?? null));
                    $payService->refund([
                        'transaction_id' => $records['transaction_id'],
                        'refund_sn' => $records['sn'],
                        'refund_amount' => $records['refund_amount'],
                        'total_amount' => $records['order_amount'],
                    ]);
                } else {
                    //余额退款
                    $userInfo = User::where('id', '=', $records['user_id'])->findOrEmpty();
                    if (!$userInfo->isEmpty()) {
                        $userInfo->user_money += $records['refund_amount'];
                        $userInfo->save();
                        AccountLogLogic::add(
                            $records['user_id'],
                            AccountLogEnum::UM_INC_ORDERS_REFUND,
                            AccountLogEnum::INC,
                            $records['refund_amount'],
                            $records['sn'],
                            '订单售后退款'
                        );
                    }
                }
            }

            $orderInfo = Orders::where('id', '=', $records['order_id'])->findOrEmpty()->toArray();
            \app\common\logic\RefundLogic::log($orderInfo, $records['id'], $records['refund_amount'], $params['admin_id'], $records->refund_status);
        }
        return true;
    }

    /**
     * @notes 退款统计
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 段誉
     * @date 2023/3/3 12:09
     */
    public static function stat()
    {
        $records = RefundRecord::select()->toArray();

        $total = 0;
        $ing = 0;
        $success = 0;
        $error = 0;

        foreach ($records as $record) {
            $total += $record['refund_amount'];
            switch ($record['refund_status']) {
                case RefundEnum::REFUND_ING:
                    $ing += $record['refund_amount'];
                    break;
                case RefundEnum::REFUND_SUCCESS:
                    $success += $record['refund_amount'];
                    break;
                case RefundEnum::REFUND_ERROR:
                    $error += $record['refund_amount'];
                    break;
            }
        }

        return [
            'total' => round($total, 2),
            'ing' => round($ing, 2),
            'success' => round($success, 2),
            'error' => round($error, 2),
        ];
    }


    /**
     * @notes 退款日志
     * @param $recordId
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @date 2023/3/3 14:25
     */
    public static function refundLog($recordId)
    {
        return (new RefundLog())
            ->order(['id' => 'desc'])
            ->where('record_id', $recordId)
            ->hidden(['refund_msg'])
            ->append(['handler', 'refund_status_text'])
            ->select()
            ->toArray();
    }

}