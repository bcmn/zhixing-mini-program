<?php

namespace app\adminapi\logic\business;

use app\common\logic\BaseLogic;
use app\common\model\business\Orders;

/**
 * 订单逻辑
 * Class BusinessOrdersLogic
 */
class BusinessOrdersLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Orders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Orders::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Orders::findOrEmpty($id)->toArray();
    }

}