<?php

namespace app\adminapi\logic\business;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\business\Business;
use app\common\model\business\Hospital;

/**
 * 服务项目逻辑
 * Class BusinessLogic
 */
class BusinessLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Business::create([
                'title' => $params['title'],
                'notes' => $params['notes'] ?? '',
                'content' => $params['content'] ?? '',
                'amount' => $params['amount'],
                'icon' => $params['icon'],
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Business::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'notes' => $params['notes'] ?? '',
                'content' => $params['content'] ?? '',
                'amount' => $params['amount'],
                'icon' => $params['icon'],
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Business::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Business::destroy($params['id']);
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Business::findOrEmpty($id)->toArray();
    }


    /**
     * @return mixed
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData($hid): array
    {
        $res = Hospital::where('id', '=', $hid)->value('services');
        if (empty($res)) {
            $lists = Business::where(['status' => DefaultEnum::SHOW])
                ->order(['sort' => 'desc', 'id' => 'desc'])
                ->field('id,title,notes,icon,amount')
                ->select()
                ->toArray();
            foreach ($lists as $key => $item) {
                $lists[$key]['is_check'] = 0;
            }
        } else {
            $lists = json_decode($res, true);
        }
        return $lists;
    }

}