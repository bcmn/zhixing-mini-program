<?php

namespace app\adminapi\logic\business;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\business\Hospital;

/**
 * 医院逻辑
 * Class HospitalLogic
 */
class HospitalLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Hospital::create([
                'name' => $params['name'],
                'image' => $params['image'] ?? '',
                'images' => $params['images'] ?? '',
                'phone' => $params['phone'] ?? '',
                'business_time' => $params['business_time'] ?? '',
                'address' => $params['address'],
                'lng' => $params['lng'],
                'lat' => $params['lat'],
                'content' => $params['content'] ?? '',
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Hospital::update([
                'id' => $params['id'],
                'user_id' => $params['user_id'],
                'name' => $params['name'],
                'image' => $params['image'] ?? '',
                'images' => $params['images'] ?? '',
                'phone' => $params['phone'] ?? '',
                'business_time' => $params['business_time'] ?? '',
                'address' => $params['address'],
                'lng' => $params['lng'],
                'lat' => $params['lat'],
                'content' => $params['content'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Hospital::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }


    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Hospital::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Hospital::findOrEmpty($params['id'])->toArray();
    }

}