<?php

namespace app\adminapi\logic\package;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\package\Package;

/**
 * 套餐逻辑
 * Class PackageLogic
 */
class PackageLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Package::create([
                'title' => $params['title'],
                'notes' => $params['notes'] ?? '',
                'image' => $params['image'] ?? '',
                'content' => $params['content'] ?? '',
                'amount' => $params['amount'],
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Package::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'notes' => $params['notes'] ?? '',
                'image' => $params['image'] ?? '',
                'content' => $params['content'] ?? '',
                'amount' => $params['amount'],
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Package::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Package::destroy($params['id']);
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Package::findOrEmpty($id)->toArray();
    }

}