<?php

namespace app\adminapi\logic\goods;

use app\common\enum\DefaultEnum;
use app\common\enum\goods\GoodsEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsAttribute;
use app\common\model\goods\GoodsCate;
use app\common\model\goods\GoodsSku;

/**
 * 商品逻辑
 */
class GoodsLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            $top_id = GoodsCate::where('id', '=', $params['cate_id'])->value('cate_id');
            $sku_type = $params['sku_type'] ?? 1;
            $goods_type = $params['type'] ?? GoodsEnum::TYPE_NORMAL;
            $insert = Goods::create([
                'name' => $params['name'],
                'type' => $goods_type,
                'cate_id' => $params['cate_id'],
                'top_id' => $top_id,
                'goods_id' => $params['goods_id'] ?? 0,
                'function_type' => $params['function_type'] ?? 'A',
                'desc' => $params['desc'] ?? '',
                'image' => $params['image'] ?? '',
                'images' => $params['images'] ?? '',
                'content' => $params['content'] ?? '',
                'notes' => $params['notes'] ?? '',
                'sku_type' => $sku_type,
                'lease_type' => $goods_type == GoodsEnum::TYPE_RENT ? ($params['lease_type'] ?? 1) : 0,
                'security_amount' => $params['security_amount'] ?? 0,
                'price' => $params['price'] ?? 0,
                'num_price' => $params['num_price'] ?? 0,
                'market_price' => $params['market_price'] ?? 0,
                'cost_price' => $params['cost_price'] ?? 0,
                'learn_price' => $params['learn_price'] ?? 0,
                'stock' => $params['stock'] ?? 0,
                'give_integral' => $params['give_integral'] ?? 0,
                'views' => $params['views'] ?? 1,
                'api_id' => $params['api_id'] ?? '',
                'api_key' => $params['api_key'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'percent_store' => $params['percent_store'] ?? '',
                'percent_partner' => $params['percent_partner'] ?? '',
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            if ($sku_type == 2) {
                self::setAttribute($insert->id, $params);
                self::setSku($insert->id, $params);
            }
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            $top_id = GoodsCate::where('id', '=', $params['cate_id'])->value('cate_id');
            $sku_type = $params['sku_type'] ?? 1;
            $goods_type = $params['type'] ?? GoodsEnum::TYPE_NORMAL;
            Goods::update([
                'id' => $params['id'],
                'type' => $params['type'] ?? GoodsEnum::TYPE_NORMAL,
                'name' => $params['name'],
                'cate_id' => $params['cate_id'],
                'top_id' => $top_id,
                'goods_id' => $params['goods_id'] ?? 0,
                'function_type' => $params['function_type'] ?? 'A',
                'desc' => $params['desc'] ?? '',
                'image' => $params['image'] ?? '',
                'images' => $params['images'] ?? '',
                'content' => $params['content'] ?? '',
                'notes' => $params['notes'] ?? '',
                'sku_type' => $sku_type,
                'lease_type' => $goods_type == GoodsEnum::TYPE_RENT ? ($params['lease_type'] ?? 1) : 0,
                'security_amount' => $params['security_amount'] ?? 0,
                'price' => $params['price'] ?? 0,
                'num_price' => $params['num_price'] ?? 0,
                'market_price' => $params['market_price'] ?? 0,
                'cost_price' => $params['cost_price'] ?? 0,
                'learn_price' => $params['learn_price'] ?? 0,
                'stock' => $params['stock'] ?? 0,
                'give_integral' => $params['give_integral'] ?? 0,
                'views' => $params['views'] ?? 1,
                'api_id' => $params['api_id'] ?? '',
                'api_key' => $params['api_key'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW,
                'percent_store' => $params['percent_store'] ?? '',
                'percent_partner' => $params['percent_partner'] ?? '',
            ]);
            if ($sku_type == 2) {
                self::setAttribute($params['id'], $params);
                self::setSku($params['id'], $params);
            }
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * Date: 2023/10/15 19:45
     * Notes:保存规格信息
     */
    protected static function setAttribute($id, $data): void
    {
        if (!empty($data['attribute'])) {
            $saveData = $data['attribute'];
            foreach ($saveData as $key => $item) {
                $saveData[$key]['goods_id'] = $id;
                $result = GoodsAttribute::where('goods_id', '=', $id)
                    ->where('title', '=', $item['title'])
                    ->findOrEmpty();
                if ($result) {
                    $saveData[$key]['id'] = $result['id'];
                }
            }
            (new GoodsAttribute())->saveAll($saveData);
        }
    }

    /**
     * @param $id
     * @param $data
     * Date: 2023/10/15 21:29
     * Notes:设置规格属性
     */
    protected static function setSku($id, $data): void
    {
        if (!empty($data['skuLists'])) {
            //删除旧的规格项
            $oldSku = GoodsSku::where('goods_id', '=', $id)->column('id');
            if (!empty($oldSku)) {
                GoodsSku::destroy($oldSku);
            }

            $saveData = $data['skuLists'];
            $goodsData = [
                'id' => $id,
                'price' => 0,
                'num_price' => 0,
                'market_price' => 0,
                'cost_price' => 0,
                'learn_price' => 0,
                'stock' => 0,
                'give_integral' => 0,
            ];
            foreach ($saveData as $key => $item) {
                $saveData[$key]['goods_id'] = $id;
                $saveData[$key]['name'] = implode(',', $item['attribute']);
                $result = GoodsSku::where('goods_id', '=', $id)
                    ->where('attribute', '=', json_encode($item['attribute'], true))
                    ->findOrEmpty();
                if ($result) {
                    $saveData[$key]['id'] = $result['id'];
                }

                $goodsData['stock'] += $item['stock'];
                if ($goodsData['price'] == 0 || $item['price'] < $goodsData['price']) {
                    $goodsData['price'] = $item['price'];
                    $goodsData['num_price'] = $item['num_price'] ?? 0;
                    $goodsData['market_price'] = $item['market_price'] ?? 0;
                    $goodsData['cost_price'] = $item['cost_price'] ?? 0;
                    $goodsData['learn_price'] = $item['learn_price'] ?? 0;
                    $goodsData['give_integral'] = $item['give_integral'] ?? 0;
                }
            }
            (new GoodsSku())->saveAll($saveData);
            (new Goods())->update($goodsData);
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Goods::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }


    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Goods::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        $detail = Goods::findOrEmpty($params['id'])->toArray();
        $detail['attribute'] = GoodsAttribute::where('goods_id', '=', $params['id'])
            ->field('title,values')
            ->select();
        $detail['skuLists'] = GoodsSku::where('goods_id', '=', $params['id'])
            ->field('attribute,image,price,num_price,market_price,cost_price,learn_price,stock,give_integral')
            ->select();
        return $detail;
    }

    /**
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData(): array
    {
        $lists = Goods::where(['status' => DefaultEnum::SHOW])
            ->where('type', 'in', [GoodsEnum::TYPE_NORMAL, GoodsEnum::TYPE_RENT])
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
        return $lists;
    }

}