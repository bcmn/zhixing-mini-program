<?php

namespace app\adminapi\logic\goods;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\service\MachineService;
use think\facade\Db;

/**
 * 商品分类逻辑
 */
class GoodsDevicesLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
//            $insertData = [];
//            $goods = Goods::where('id', '=', $params['goods_id'])->findOrEmpty();
//            $prefix = $params['prefix'] ?? '';
//
//            $start_sn = (int)$params['start_sn'];
//            $end_sn = (int)$params['end_sn'];
//            $length = ($end_sn - $start_sn) + 1;
//            for ($i = 0; $i < $length; $i++) {
//                $insertData[$i]['goods_id'] = $params['goods_id'];
//                $insertData[$i]['goods_name'] = $goods['name'];
//                $insertData[$i]['goods_image'] = $goods['image'];
//                $insertData[$i]['sn'] = $prefix . ($start_sn + $i);
//                $insertData[$i]['lease_type'] = $goods['lease_type'];
//                $insertData[$i]['security_amount'] = $goods['security_amount'];
//                $insertData[$i]['price'] = $goods['price'];
//                $insertData[$i]['learn_price'] = $goods['learn_price'];
//                $insertData[$i]['is_active'] = 0;
//                $insertData[$i]['sort'] = 0;
//                $insertData[$i]['status'] = DefaultEnum::SHOW;
//            }
//            (new GoodsDevices())->saveAll($insertData);

            $device = GoodsDevices::where('sn', '=', $params['sn'])
                ->findOrEmpty();
            if (!$device->isEmpty()) {
                self::$error = '设备已在库，无需重复入库';
                return false;
            }

            $goods = Goods::where('id', '=', $params['goods_id'])->findOrEmpty();
            if ($goods->isEmpty()) {
                self::$error = '查询不到商品信息';
                return false;
            }

            if (empty($goods['api_id'])) {
                self::$error = '商品缺少onenet平台ID，无法创建，请补充后重试';
                return false;
            }

            Db::startTrans();
            $title = generate_sn(GoodsDevices::class, 'title');
            GoodsDevices::create([
                'goods_id' => $params['goods_id'],
                'goods_name' => $goods['name'],
                'goods_image' => $goods['image'],
                'title' => $title,
                'lease_type' => $goods['lease_type'],
                'security_amount' => $goods['security_amount'],
                'price' => $goods['price'],
                'learn_price' => $goods['learn_price'],
                'is_active' => 0,
                'sn' => $params['sn'] ?? '',
                'imsi' => $params['imsi'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);

            $data = [
                'product_id' => $goods['api_id'],
                'device_name' => $title,
                'imei' => $params['sn'],
                'imsi' => $params['imsi']
            ];
            $result = MachineService::createDevices($data);

            if (false === $result) {
                Db::rollback();
                self::$error = MachineService::$error;
                return false;
            }

            Db::commit();
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            GoodsDevices::update([
                'id' => $params['id'],
                'goods_id' => $params['goods_id'],
                'sn' => $params['sn'] ?? '',
                'imsi' => $params['imsi'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        GoodsDevices::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): bool
    {
        $device = GoodsDevices::where('id', '=', $params['id'])
            ->findOrEmpty();
        if ($device->isEmpty()) {
            self::$error = '查询不到设备';
            return false;
        }

        GoodsDevices::destroy($params['id']);

        $result = MachineService::deleteDevices(['imei' => $device['sn']]);
        if (false === $result) {
            Db::rollback();
            self::$error = MachineService::$error;
            return false;
        }

        return true;
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：重置设备
     */
    public static function reset(array $params): bool
    {
        $device = GoodsDevices::where('id', '=', $params['id'])
            ->findOrEmpty();
        if ($device->isEmpty()) {
            self::$error = '查询不到设备';
            return false;
        }

        $device->user_id = '';
        $device->order_id = '';
        $device->order_goods_id = '';
        $device->order_sn = '';
        $device->is_active = 0;
        $device->active_time = 0;
        $device->end_time = 0;
        $device->lng = '';
        $device->lat = '';
        $device->save();

        return true;
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return GoodsDevices::findOrEmpty($id)->toArray();
    }

    /**
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData(): array
    {
        return GoodsDevices::where(['status' => DefaultEnum::SHOW])
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
    }

}