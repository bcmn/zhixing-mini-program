<?php

namespace app\adminapi\logic\coupon;

use app\common\enum\CouponEnum;
use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\coupon\Coupon;
use app\common\model\coupon\CouponRecord;
use app\common\model\goods\GoodsCate;
use app\common\model\user\User;

/**
 * 商品分类逻辑
 */
class CouponLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Coupon::create([
                'title' => $params['title'],
                'price' => $params['price'] ?? 0.01,
                'full' => $params['full'] ?? 0,
                'num' => $params['num'] ?? 100,
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Coupon::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'price' => $params['price'] ?? 0.01,
                'full' => $params['full'] ?? 0,
                'num' => $params['num'] ?? 100,
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Coupon::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Coupon::destroy($params['id']);
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Coupon::findOrEmpty($id)->toArray();
    }

    /**
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData(): array
    {
        return Coupon::where(['status' => DefaultEnum::SHOW])
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
    }

    /**
     * Date: 2023/10/27 21:57
     * Notes:发放优惠券
     */
    public static function grant($params): bool|string
    {
        $coupon = Coupon::where('id', '=', $params['id'])->findOrEmpty();
        if ($coupon->isEmpty()) {
            self::$error = '查询不到优惠券信息';
            return false;
        }
        $grantNum = CouponRecord::where('coupon_id', '=', $coupon['id'])->count();
        $leaveNum = $coupon['num'] - $grantNum;
        $number = $params['number'] ?? 1;
        if ($leaveNum < $number) {
            self::$error = '优惠券数量不足';
            return false;
        }
        $userInfo = User::where('mobile', '=', $params['phone'])->findOrEmpty();
        if ($userInfo->isEmpty()) {
            self::$error = '查询不到用户信息';
            return false;
        }

        $saveData = [];
        for ($i = 0; $i < $number; $i++) {
            $saveData[$i]['user_id'] = $userInfo['id'];
            $saveData[$i]['coupon_id'] = $coupon['id'];
            $saveData[$i]['sn'] = generate_sn(CouponRecord::class, 'sn');
            $saveData[$i]['title'] = $coupon['title'];
            $saveData[$i]['price'] = $coupon['price'];
            $saveData[$i]['full'] = $coupon['full'];
            $saveData[$i]['date'] = $coupon['date'];
            $saveData[$i]['order_id'] = 0;
            $saveData[$i]['status'] = CouponEnum::STATUS_WAIT;
        }

        try {
            (new CouponRecord())->saveAll($saveData);
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
        return true;
    }
}