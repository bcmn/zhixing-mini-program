<?php

namespace app\adminapi\logic\user;

use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\user\User;
use app\common\model\user\Withdraw;

/**
 * 提现逻辑
 * Class OrdersCartLogic
 */
class WithdrawLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Withdraw::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);

        // 拒绝时更新拒绝原因
        if ($params['field'] == 'status' && $params['val'] == 2) {
            $withdraw = Withdraw::findOrEmpty($params['id']);
            $withdraw->reason = $params['reason'] ?? '';
            $withdraw->save();

            // 返还用户余额
            $user = User::findOrEmpty($withdraw['user_id']);
            $user->user_money += $withdraw['amount'];
            $user->save();

            AccountLogLogic::add($withdraw['user_id'], AccountLogEnum::UM_INC_WITHDRAW, AccountLogEnum::INC, $withdraw['amount'], $withdraw['sn'], '提现退回');
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Withdraw::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Withdraw::findOrEmpty($id)->toArray();
    }

}