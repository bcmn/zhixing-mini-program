<?php

namespace app\adminapi\logic\user;

use app\common\enum\user\UserApplyEnum;
use app\common\enum\user\UserEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\User;
use app\common\model\user\UserApply;

/**
 * 加盟申请逻辑
 */
class UserApplyLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        UserApply::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);

        if ($params['field'] == 'status') {
            $user_id = UserApply::where('id', '=', $params['id'])->value('user_id');
            $apply = UserApply::where('id', '=', $params['id'])->findOrEmpty();
            if (!empty($user_id)) {
                $level = $params['val'] == UserApplyEnum::STATUS_PASS ? $apply['type'] : UserEnum::LEVEL_NORMAL;
                User::update([
                    'id' => $user_id,
                    'level' => $level
                ]);
            }
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        UserApply::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserApply::findOrEmpty($id)->toArray();
    }

}