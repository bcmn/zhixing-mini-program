<?php

namespace app\adminapi\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserRepair;
use app\common\model\user\UserRepairLogs;

/**
 * 维修记录逻辑
 */
class UserRepairLogsLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function add(array $params): bool
    {
        try {
            UserRepairLogs::create([
                'content' => $params['content'],
                'images' => $params['images'] ?? '',
                'repair_id' => $params['repair_id'],
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            UserRepairLogs::update([
                'id' => $params['id'],
                'content' => $params['content'],
                'images' => $params['images'],
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        UserRepairLogs::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserRepairLogs::findOrEmpty($id)->toArray();
    }

}