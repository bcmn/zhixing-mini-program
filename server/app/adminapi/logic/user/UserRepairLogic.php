<?php

namespace app\adminapi\logic\user;

use app\common\enum\user\UserApplyEnum;
use app\common\enum\user\UserEnum;
use app\common\enum\user\UserRepairEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\User;
use app\common\model\user\UserApply;
use app\common\model\user\UserRepair;

/**
 * 维修逻辑
 */
class UserRepairLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        UserRepair::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);

        if ($params['field'] == 'status') {
            $user_id = UserRepair::where('id', '=', $params['id'])->value('user_id');
            $apply = UserRepair::where('id', '=', $params['id'])->findOrEmpty();
            if (!empty($user_id)) {
                $level = $params['val'] == UserRepairEnum::STATUS_PASS ? $apply['type'] : UserEnum::LEVEL_NORMAL;
                User::update([
                    'id' => $user_id,
                    'level' => $level
                ]);
            }
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        UserRepair::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserRepair::findOrEmpty($id)->toArray();
    }

}