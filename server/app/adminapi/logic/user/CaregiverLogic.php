<?php

namespace app\adminapi\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\caregiver\Caregiver;
use app\common\model\user\User;

/**
 * 服务项目逻辑
 * Class GoodsCateLogic
 */
class CaregiverLogic extends BaseLogic
{
    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Caregiver::update([
                'id' => $params['id'],
                'image' => $params['image'] ?? '',
                'name' => $params['name'] ?? '',
                'sex' => $params['sex'] ?? '',
                'age' => $params['age'],
                'mobile' => $params['mobile'],
                'idcard_name' => $params['idcard_name'] ?? '',
                'idcard' => $params['idcard'] ?? '',
                'idcard_front' => $params['idcard_front'] ?? '',
                'idcard_back' => $params['idcard_back'] ?? '',
                'notes' => $params['notes'] ?? '',
                'album' => $params['album'] ?? '',
                'license' => $params['license'] ?? '',
                'status' => $params['status'] ?? DefaultEnum::SHOW,
                'score' => $params['score'] ?? 5,
                'orders_num' => $params['orders_num'] ?? 0,
                'complaint_num' => $params['complaint_num'] ?? 0,
            ]);

            $type = 0;
            if ($params['status'] == DefaultEnum::SHOW) {
                $type = 1;
            }
            User::update([
                'id' => $params['user_id'],
                'type' => $type
            ]);

            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Caregiver::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Caregiver::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Caregiver::findOrEmpty($params['id'])->toArray();
    }

}