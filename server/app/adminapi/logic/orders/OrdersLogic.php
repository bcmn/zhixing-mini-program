<?php

namespace app\adminapi\logic\orders;

use app\common\enum\goods\GoodsEnum;
use app\common\enum\orders\OrdersEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\service\Kuaidi100Service;
use think\facade\Db;

/**
 * 订单逻辑
 */
class OrdersLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Orders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Orders::destroy($params['id']);
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Orders::where(['id' => $id])
            ->with(['userInfo', 'goods'])
            ->find()
            ->toArray();
    }

    /**
     * @param $params
     * Date: 2023/10/21 14:25
     * Notes:订单发货(原发货系统，已弃用)
     */
    public static function delivery($params): bool|string
    {
        try {
            $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
            if ($ordersInfo->isEmpty()) {
                self::$error = '订单不存在，请确认';
                return false;
            }
            if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
                self::$error = '订单状态异常，请确认';
                return false;
            }

            Db::startTrans();
            $ordersInfo->status = OrdersEnum::STATUS_DELIVER;
            $ordersInfo->express_time = time();
            $ordersInfo->express_company = $params['express_company'];
            $ordersInfo->express_sn = $params['express_sn'];
            $ordersInfo->save();

            if (!empty($params['goods'])) {
                foreach ($params['goods'] as $item) {
                    if (in_array($item['goods_type'], [GoodsEnum::TYPE_NORMAL, GoodsEnum::TYPE_RENT])) {
                        if (empty($item['devices'])) {
                            self::$error = '未填写设备编码';
                            return false;
                        }

                        $devices = GoodsDevices::where('goods_id', '=', $item['goods_id'])
                            ->where('sn', '=', trim($item['devices']))
                            ->where('user_id', '=', 0)
                            ->findOrEmpty();
                        if ($devices->isEmpty()) {
                            self::$error = '设备' . $item['devices'] . '不存在或已激活，请确认';
                            Db::rollback();
                            return false;
                        }

                        $orderGoods = OrdersGoods::where('id', '=', $item['id'])
                            ->findOrEmpty();
                        $orderGoods->devices_sn = trim($item['devices']);
                        $orderGoods->save();

                        // 更新设备信息
                        $updateData = [
                            'id' => $devices['id'],
                            'user_id' => $ordersInfo['user_id'],
                            'order_id' => $ordersInfo['id'],
                            'order_sn' => $ordersInfo['sn'],
                            'order_goods_id' => $orderGoods['id'],
                            'is_active' => 0,
                            'sku_id' => $item['sku_id'],
                            'sku_name' => $item['sku_name'],
                            'imsi' => $item['imsi'],
                        ];

                        //查找商品的配件
                        $partIds = Goods::where('goods_id', '=', $orderGoods['goods_id'])->column('id');
                        if (!empty($partIds)) {
                            $partLists = OrdersGoods::where('order_id', '=', $orderGoods['order_id'])
                                ->where('goods_type', '=', GoodsEnum::TYPE_PART)
                                ->where('goods_id', 'in', $partIds)
                                ->select()
                                ->toArray();
                            if (!empty($partLists)) {
                                foreach ($partLists as $v) {
                                    $updateData[$v['function_type'] . '_name'] = $v['goods_name'];
                                    $updateData[$v['function_type'] . '_num'] = $v['lease_type'] == 1 ? $v['num'] : $v['num'] * 60 * 60;
                                }
                            }
                        }
                        GoodsDevices::update($updateData);
                    }
                }
            }

            //打印快递单
            Kuaidi100Service::print($params['id']);

            Db::commit();
            return true;
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
    }

    /**
     * Date: 2023/11/25 17:15
     * Notes:打印快递单
     */
    public static function printOrder($id)
    {
        Kuaidi100Service::print($id);
        return true;
    }

    /**
     * @param $params
     * Date: 2023/10/21 16:02
     * Notes:确认订单
     */
    public static function confirm($params)
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '订单无效，请确认';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->confirm_time = time();
        $ordersInfo->save();
        return true;
    }

    /**
     * @param $params
     * Date: 2023/10/21 16:02
     * Notes:取消订单
     */
    public static function cancel($params)
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '订单无效，请确认';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }

}