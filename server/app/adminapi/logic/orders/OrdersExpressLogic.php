<?php

namespace app\adminapi\logic\orders;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\orders\OrdersExpress;

/**
 * 快递公司逻辑
 */
class OrdersExpressLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            OrdersExpress::create([
                'title' => $params['title'],
                'code' => $params['code'],
                'tempid' => $params['tempid'] ?? '',
                'notes' => $params['notes'] ?? '',
                'sort' => 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            OrdersExpress::update([
                'id' => $params['id'],
                'title' => $params['title'],
                'code' => $params['code'],
                'tempid' => $params['tempid'] ?? '',
                'notes' => $params['notes'] ?? '',
                'sort' => $params['sort'] ?? 0,
                'status' => $params['status'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        OrdersExpress::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }


    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        OrdersExpress::destroy($params['id']);
    }


    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return OrdersExpress::findOrEmpty($params['id'])->toArray();
    }

    /**
     * @return mixed
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData(): array
    {
        return OrdersExpress::where(['status' => DefaultEnum::SHOW])
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
    }
}