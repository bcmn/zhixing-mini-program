<?php

namespace app\adminapi\lists\coupon;

use app\adminapi\lists\BaseAdminDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\coupon\Coupon;

/**
 * 优惠券列表
 */
class CouponLists extends BaseAdminDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['title']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere(): array
    {
        $where = [];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Coupon::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('*,id as get_num')
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->select()
            ->toArray();
    }


    /**
     * @notes 获取数量
     * @return int
     * @date 2022/5/26 9:48
     */
    public function count(): int
    {
        return Coupon::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}