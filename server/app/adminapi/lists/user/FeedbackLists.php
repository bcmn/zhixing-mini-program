<?php

namespace app\adminapi\lists\user;

use app\adminapi\lists\BaseAdminDataLists;
use app\common\lists\ListsExcelInterface;
use app\common\lists\ListsSearchInterface;
use app\common\model\user\UserFeedback;

/**
 * 陪护师申请列表
 * Class GoodsLists
 */
class FeedbackLists extends BaseAdminDataLists implements ListsSearchInterface, ListsExcelInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        if (isset($this->params['keywords']) && !empty($this->params['keywords'])) {
            $where[] = ['content', 'like', '%' . $this->params['keywords'] . '%'];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return UserFeedback::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('*, status as status_text')
            ->with(['userInfo'])
            ->limit($this->limitOffset, $this->limitLength)
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @notes 获取数量
     * @return int
     * @author 段誉
     * @date 2022/5/26 9:48
     */
    public function count(): int
    {
        return UserFeedback::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }


    /**
     * @notes 导出文件名
     * @return string
     * @date 2022/11/24 16:17
     */
    public function setFileName(): string
    {
        return '意见反馈';
    }


    /**
     * @notes 导出字段
     * @return string[]
     * @date 2022/11/24 16:17
     */
    public function setExcelFields(): array
    {
        return [
            'id' => 'ID',
            'user_id' => '用户ID',
            'content' => '内容',
            'create_time' => '添加时间',
        ];
    }

}