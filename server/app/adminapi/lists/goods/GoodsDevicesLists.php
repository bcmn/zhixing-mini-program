<?php

namespace app\adminapi\lists\goods;

use app\adminapi\lists\BaseAdminDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\goods\GoodsDevices;

/**
 * 商品设备列表
 */
class GoodsDevicesLists extends BaseAdminDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '=' => ['is_active'],
            '%like%' => ['sn']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere(): array
    {
        $where = [];

        if (isset($this->params['goods_id']) && !empty($this->params['goods_id'])) {
            $where[] = ['goods_id', '=', $this->params['goods_id']];
        }

        if (isset($this->params['start_time']) && !empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }

        if (isset($this->params['end_time']) && !empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return GoodsDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->with('userInfo')
            ->field('*')
            ->order(['sort' => 'desc', 'id' => 'desc'])
            ->limit($this->limitOffset, $this->limitLength)
            ->select()
            ->toArray();
    }


    /**
     * @notes 获取数量
     * @return int
     * @date 2022/5/26 9:48
     */
    public function count(): int
    {
        return GoodsDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}