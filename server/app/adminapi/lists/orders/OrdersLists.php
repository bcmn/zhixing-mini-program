<?php

namespace app\adminapi\lists\orders;

use app\adminapi\lists\BaseAdminDataLists;
use app\common\lists\ListsExcelInterface;
use app\common\lists\ListsSearchInterface;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;

/**
 * 订单列表
 */
class OrdersLists extends BaseAdminDataLists implements ListsSearchInterface, ListsExcelInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '=' => ['sn'],
            '%like%' => ['name', 'mobile', 'urgent_name', 'urgent_phone']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [];

        if (isset($this->params['goods_id']) && !empty($this->params['goods_id'])) {
            $order_ids = OrdersGoods::where('goods_id', '=', $this->params['goods_id'])->column('order_id');
            $where[] = ['id', 'in', $order_ids];
        }

        if (isset($this->params['start_time']) && !empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (isset($this->params['end_time']) && !empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('*,status as status_text')
            ->with(['userInfo', 'goods', 'express'])
            ->limit($this->limitOffset, $this->limitLength)
            ->order(['id' => 'desc'])
            ->select()
            ->toArray();
    }


    /**
     * @notes 获取数量
     * @return int
     * @date 2022/5/26 9:48
     */
    public function count(): int
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }


    /**
     * @notes 导出文件名
     * @return string
     * @date 2022/11/24 16:17
     */
    public function setFileName(): string
    {
        return '订单列表';
    }


    /**
     * @notes 导出字段
     * @return string[]
     * @date 2022/11/24 16:17
     */
    public function setExcelFields(): array
    {
        return [
            'id' => 'ID',
            'sn' => '订单编号',
            'order_amount' => '订单价格',
            'create_time' => '添加时间',
        ];
    }

}