<?php

namespace app\adminapi\validate\goods;

use app\common\validate\BaseValidate;

/**
 * 商品分类验证
 */
class GoodsCateValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写分类标题',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): GoodsCateValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): GoodsCateValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/14 15:13
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    /**
     * Date: 2023/10/14 15:13
     * Notes:更新字段场景
     */
    public function sceneChange(): GoodsCateValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): GoodsCateValidate
    {
        return $this->only(['id']);
    }

}