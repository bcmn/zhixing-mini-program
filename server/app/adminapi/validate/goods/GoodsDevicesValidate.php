<?php

namespace app\adminapi\validate\goods;

use app\common\validate\BaseValidate;

/**
 * 商品设备验证
 */
class GoodsDevicesValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'goods_id' => 'require',
//        'start_sn' => 'require',
//        'end_sn' => 'require',
        'sn' => 'require',
        'imsi' => 'require'
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'goods_id.require' => '未选择设备商品',
//        'start_sn.require' => '未填写设备开始编码',
//        'end_sn.require' => '未填写设备结束编码',
        'sn.require' => '未填写设备编码',
        'imsi.require' => '未填写物联卡号',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): GoodsDevicesValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): GoodsDevicesValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/14 15:13
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
        return $this->remove('start_sn', true)
            ->remove('end_sn', true)
            ->append('sn', 'require');
    }

    /**
     * Date: 2023/10/14 15:13
     * Notes:更新字段场景
     */
    public function sceneChange(): GoodsDevicesValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): GoodsDevicesValidate
    {
        return $this->only(['id']);
    }

}