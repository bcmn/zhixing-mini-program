<?php

namespace app\adminapi\validate\goods;

use app\common\validate\BaseValidate;

/**
 * 商品验证
 */
class GoodsValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'name' => 'require',
        'cate_id' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'name.require' => '未填写商品名称',
        'cate_id.require' => '未选择商品分类',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): GoodsValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): GoodsValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/14 15:13
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    /**
     * Date: 2023/10/14 15:13
     * Notes:更新字段场景
     */
    public function sceneChange(): GoodsValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): GoodsValidate
    {
        return $this->only(['id']);
    }

}