<?php

namespace app\adminapi\validate\user;

use app\common\validate\BaseValidate;

/**
 * 维修记录验证
 */
class UserRepairLogsValidate extends BaseValidate
{
    protected $rule = [
        'repair_id' => 'require',
        'content' => 'require',
    ];

    protected $message = [
        'repair_id.require' => '参数缺失',
        'content.require' => '参数缺失',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd()
    {
        return $this->remove('id', true);
    }

    public function sceneChange()
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail()
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete()
    {
        return $this->only(['id']);
    }

}