<?php

namespace app\adminapi\validate\user;

use app\common\validate\BaseValidate;

/**
 * 服务对象
 * Class CustomerValidate
 */
class CustomerValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'name' => 'require',
        'mobile' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'name.require' => '未填写姓名',
        'mobile.require' => '未填写手机号码',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd()
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail()
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete()
    {
        return $this->only(['id']);
    }

}