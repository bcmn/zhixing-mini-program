<?php

namespace app\adminapi\validate\user;

use app\common\validate\BaseValidate;

/**
 * 陪护师申请验证
 * Class UserAddressValidate
 */
class CaregiverValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'image' => 'require',
        'name' => 'require',
        'mobile' => 'require',
        'idcard_name' => 'require',
        'idcard' => 'require',
        'idcard_front' => 'require',
        'idcard_back' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'image.require' => '未上传形象照片',
        'name.require' => '未填写姓名',
        'mobile.require' => '未填写手机号码',
        'idcard_name.require' => '未填写身份证姓名',
        'idcard.require' => '未填写身份证号码',
        'idcard_front.require' => '未上传身份证正面',
        'idcard_back.require' => '未上传身份证反面',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd()
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail()
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
        return $this->remove('user_id', true);
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete()
    {
        return $this->only(['id']);
    }

}