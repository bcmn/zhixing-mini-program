<?php

namespace app\adminapi\validate\coupon;

use app\common\validate\BaseValidate;

/**
 * 优惠券验证
 */
class CouponListsValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写分类标题',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): CouponListsValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): CouponListsValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/14 15:13
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    /**
     * Date: 2023/10/14 15:13
     * Notes:更新字段场景
     */
    public function sceneChange(): CouponListsValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): CouponListsValidate
    {
        return $this->only(['id']);
    }

}