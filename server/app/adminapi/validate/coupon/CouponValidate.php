<?php

namespace app\adminapi\validate\coupon;

use app\common\validate\BaseValidate;

/**
 * 优惠券验证
 */
class CouponValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
        'price' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写优惠券标题',
        'price.require' => '未填写优惠金额',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): CouponValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): CouponValidate
    {
        return $this->only(['id']);
    }

    /**
     * Date: 2023/10/27 21:58
     * Notes:发放场景
     */
    public function sceneGrant(): CouponValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/14 15:13
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    /**
     * Date: 2023/10/14 15:13
     * Notes:更新字段场景
     */
    public function sceneChange(): CouponValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): CouponValidate
    {
        return $this->only(['id']);
    }

}