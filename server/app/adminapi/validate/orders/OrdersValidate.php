<?php

namespace app\adminapi\validate\orders;

use app\common\enum\orders\OrdersEnum;
use app\common\validate\BaseValidate;

/**
 * 服务订单验证
 * Class OrdersValidate
 */
class OrdersValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'express_company' => 'require',
        'express_sn' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'express_company.require' => '未选择快递公司',
        'express_sn.require' => '未填写快递编码',
    ];


    /**
     * Date: 2023/10/21 15:23
     * Notes:订单发货
     */
    public function sceneDelivery(): OrdersValidate
    {
        return $this->append('status', 'checkDelivery');
    }

    /**
     * Date: 2023/10/21 15:54
     * Notes:确认收货
     */
    public function sceneConfirm(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('status', 'checkConfirm');
    }

    /**
     * Date: 2023/10/21 16:00
     * Notes:取消订单场景
     */
    public function sceneCancel(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('status', 'checkCancel');
    }

    public function sceneChange(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersValidate
    {
        return $this->only(['id']);
    }


    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @param $value
     * Date: 2023/10/21 15:56
     * Notes:确认当前状态是否可确认收货
     */
    public function checkConfirm($value): bool|string
    {
        if ($value != OrdersEnum::STATUS_DELIVER) {
            return '订单状态有误，请确认';
        }
        return true;
    }

    /**
     * @param $value
     * Date: 2023/10/21 15:59
     * Notes:确认是否发货
     */
    public function checkDelivery($value): bool|string
    {
        if ($value != OrdersEnum::STATUS_PAY) {
            return '订单状态有误，请确认';
        }
        return true;
    }

    /**
     * @param $value
     * Date: 2023/10/21 16:00
     * Notes:取消订单场景
     */
    public function checkCancel($value): bool|string
    {
        if ($value != OrdersEnum::STATUS_WAIT) {
            return '订单状态有误，请确认';
        }
        return true;
    }


}