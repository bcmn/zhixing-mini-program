<?php

namespace app\adminapi\validate\orders;

use app\common\validate\BaseValidate;

/**
 * 快递公司验证
 */
class OrdersExpressValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
        'code' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写快递公司名称',
        'code.require' => '未填写快递公司编码',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): OrdersExpressValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersExpressValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): OrdersExpressValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersExpressValidate
    {
        return $this->only(['id']);
    }

}