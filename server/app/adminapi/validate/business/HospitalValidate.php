<?php

namespace app\adminapi\validate\business;

use app\common\validate\BaseValidate;

/**
 * 服务订单验证
 * Class OrdersValidate
 */
class HospitalValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'name' => 'require',
//        'content' => 'require',
        'address' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'name.require' => '未填写医院名称',
//        'content.require' => '未填写医院介绍',
        'address.require' => '未填写医院地址',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): HospitalValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): HospitalValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): HospitalValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): HospitalValidate
    {
        return $this->only(['id']);
    }

}