<?php

namespace app\adminapi\validate\business;

use app\common\validate\BaseValidate;

/**
 * 门店验证
 */
class StoreValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'name' => 'require',
        'phone' => 'require',
        'address' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'name.require' => '未填写门店名称',
        'phone.require' => '未填写门店联系电话',
        'address.require' => '未选择门店地址',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): StoreValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): StoreValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): StoreValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): StoreValidate
    {
        return $this->only(['id']);
    }

}