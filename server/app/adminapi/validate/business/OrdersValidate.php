<?php

namespace app\adminapi\validate\business;

use app\common\validate\BaseValidate;

/**
 * 服务订单验证
 * Class OrdersValidate
 */
class OrdersValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'caregiver_id' => 'require',
        'hospital_id' => 'require',
        'appoint_time' => 'require',
        'customer_id' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'caregiver_id.require' => '未选择陪护师',
        'hospital_id.require' => '未选择医院',
        'appoint_time.require' => '未选择预约时间',
        'customer_id.require' => '未选择就诊人',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd()
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail()
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }


    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete()
    {
        return $this->only(['id']);
    }

}