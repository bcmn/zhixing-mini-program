<?php

namespace app\adminapi\validate\business;

use app\common\validate\BaseValidate;

/**
 * 服务项目验证
 * Class BusinessValidate
 */
class BusinessValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
        'amount' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写服务标题',
        'amount.require' => '请填写服务价格',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): BusinessValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): BusinessValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): BusinessValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): BusinessValidate
    {
        return $this->only(['id']);
    }

}