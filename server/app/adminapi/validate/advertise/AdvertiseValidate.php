<?php

namespace app\adminapi\validate\advertise;

use app\common\validate\BaseValidate;

/**
 * 广告内容验证
 * Class AdvertiseValidate
 */
class AdvertiseValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'title' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'title.require' => '未填写标题',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): AdvertiseValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): AdvertiseValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): AdvertiseValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): AdvertiseValidate
    {
        return $this->only(['id']);
    }

}