<?php

namespace app\adminapi\controller\advertise;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\advertise\AdvertiseCateLists;
use app\adminapi\logic\advertise\AdvertiseCateLogic;
use app\adminapi\validate\advertise\AdvertiseCateValidate;
use think\response\Json;

/**
 * 广告类目控制器
 * Class CateController
 */
class CateController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new AdvertiseCateLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new AdvertiseCateValidate())->post()->goCheck('add');
        $result = AdvertiseCateLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(AdvertiseCateLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new AdvertiseCateValidate())->post()->goCheck('edit');
        $result = AdvertiseCateLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(AdvertiseCateLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new AdvertiseCateValidate())->post()->goCheck('change');
        AdvertiseCateLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }



    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new AdvertiseCateValidate())->post()->goCheck('delete');
        AdvertiseCateLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new AdvertiseCateValidate())->goCheck('detail');
        $result = AdvertiseCateLogic::detail($params);
        return $this->data($result);
    }


    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = AdvertiseCateLogic::getAllData();
        return $this->data($result);
    }

}