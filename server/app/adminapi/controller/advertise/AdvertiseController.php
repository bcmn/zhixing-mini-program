<?php

namespace app\adminapi\controller\advertise;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\advertise\AdvertiseLists;
use app\adminapi\logic\advertise\AdvertiseLogic;
use app\adminapi\validate\advertise\AdvertiseValidate;
use think\response\Json;

/**
 * 广告控制器
 * Class AdvertiseController
 */
class AdvertiseController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new AdvertiseLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new AdvertiseValidate())->post()->goCheck('add');
        $result = AdvertiseLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(AdvertiseLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new AdvertiseValidate())->post()->goCheck('edit');
        $result = AdvertiseLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(AdvertiseLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new AdvertiseValidate())->post()->goCheck('change');
        AdvertiseLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new AdvertiseValidate())->post()->goCheck('delete');
        AdvertiseLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new AdvertiseValidate())->goCheck('detail');
        $result = AdvertiseLogic::detail($params);
        return $this->data($result);
    }

}