<?php

namespace app\adminapi\controller\package;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\goods\GoodsLists;
use app\adminapi\lists\package\PackageLists;
use app\adminapi\logic\goods\GoodsCateLogic;
use app\adminapi\logic\package\PackageLogic;
use app\adminapi\validate\goods\GoodsValidate;
use app\adminapi\validate\package\PackageValidate;
use think\response\Json;

/**
 * 医旅套餐控制器
 * Class PackageController
 */
class PackageController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new PackageLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new PackageValidate())->post()->goCheck('add');
        $result = PackageLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(PackageLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new PackageValidate())->post()->goCheck('edit');
        $result = PackageLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(PackageLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new PackageValidate())->post()->goCheck('change');
        PackageLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new PackageValidate())->post()->goCheck('delete');
        PackageLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new PackageValidate())->goCheck('detail');
        $result = PackageLogic::detail($params);
        return $this->data($result);
    }

}