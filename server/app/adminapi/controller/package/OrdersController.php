<?php

namespace app\adminapi\controller\package;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\package\PackageOrdersLists;
use app\adminapi\logic\package\PackageOrdersLogic;
use app\adminapi\validate\package\PackageOrdersValidate;
use think\response\Json;

/**
 * 医旅套餐订单控制器
 * Class OrdersController
 */
class OrdersController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new PackageOrdersLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new PackageOrdersValidate())->post()->goCheck('change');
        PackageOrdersLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new PackageOrdersValidate())->post()->goCheck('delete');
        PackageOrdersLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new PackageOrdersValidate())->goCheck('detail');
        $result = PackageOrdersLogic::detail($params);
        return $this->data($result);
    }

}