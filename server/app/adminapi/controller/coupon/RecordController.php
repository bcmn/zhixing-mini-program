<?php

namespace app\adminapi\controller\coupon;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\coupon\CouponRecordLists;
use app\adminapi\lists\goods\GoodsCateLists;
use app\adminapi\logic\goods\GoodsCateLogic;
use app\adminapi\validate\goods\GoodsCateValidate;
use think\response\Json;

/**
 * 优惠券控制器
 */
class RecordController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new CouponRecordLists());
    }

}