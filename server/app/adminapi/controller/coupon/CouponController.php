<?php

namespace app\adminapi\controller\coupon;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\coupon\CouponLists;
use app\adminapi\lists\goods\GoodsCateLists;
use app\adminapi\logic\coupon\CouponLogic;
use app\adminapi\logic\goods\GoodsCateLogic;
use app\adminapi\validate\coupon\CouponValidate;
use app\adminapi\validate\goods\GoodsCateValidate;
use think\response\Json;

/**
 * 优惠券控制器
 */
class CouponController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new CouponLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new CouponValidate())->post()->goCheck('add');
        $result = CouponLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(CouponLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new CouponValidate())->post()->goCheck('edit');
        $result = CouponLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(CouponLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new CouponValidate())->post()->goCheck('change');
        CouponLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new CouponValidate())->post()->goCheck('delete');
        CouponLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new CouponValidate())->goCheck('detail');
        $result = CouponLogic::detail($params);
        return $this->data($result);
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = CouponLogic::getAllData();
        return $this->data($result);
    }

    /**
     * Date: 2023/10/27 21:56
     * Notes:发放优惠券
     */
    public function grant(): Json
    {
        $params = (new CouponValidate())->post()->goCheck('grant');
        $result = CouponLogic::grant($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(CouponLogic::getError());
    }
}