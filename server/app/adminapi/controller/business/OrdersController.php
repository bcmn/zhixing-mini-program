<?php

namespace app\adminapi\controller\business;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\goods\BusinessOrdersLists;
use app\adminapi\logic\goods\BusinessOrdersLogic;
use app\adminapi\validate\goods\BusinessOrdersValidate;
use think\response\Json;

/**
 * 医旅套餐订单控制器
 * Class OrdersController
 */
class OrdersController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new BusinessOrdersLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new BusinessOrdersValidate())->post()->goCheck('change');
        BusinessOrdersLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new BusinessOrdersValidate())->post()->goCheck('delete');
        BusinessOrdersLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new BusinessOrdersValidate())->goCheck('detail');
        $result = BusinessOrdersLogic::detail($params);
        return $this->data($result);
    }

}