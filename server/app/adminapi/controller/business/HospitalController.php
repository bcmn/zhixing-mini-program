<?php

namespace app\adminapi\controller\business;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\business\HospitalLists;
use app\adminapi\logic\business\HospitalLogic;
use app\adminapi\validate\business\HospitalValidate;
use think\response\Json;

/**
 * 医院控制器
 * Class HospitalController
 */
class HospitalController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new HospitalLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new HospitalValidate())->post()->goCheck('add');
        $result = HospitalLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(HospitalLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new HospitalValidate())->post()->goCheck('edit');
        $result = HospitalLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(HospitalLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new HospitalValidate())->post()->goCheck('change');
        HospitalLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new HospitalValidate())->post()->goCheck('delete');
        HospitalLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new HospitalValidate())->goCheck('detail');
        $result = HospitalLogic::detail($params);
        return $this->data($result);
    }

}