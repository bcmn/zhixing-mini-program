<?php

namespace app\adminapi\controller\business;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\logic\business\StoreLogic;
use app\adminapi\validate\business\StoreValidate;
use app\api\lists\store\StoreLists;
use think\response\Json;

/**
 * 门店控制器
 */
class StoreController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new StoreLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new StoreValidate())->post()->goCheck('add');
        $result = StoreLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(StoreLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new StoreValidate())->post()->goCheck('edit');
        $result = StoreLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(StoreLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new StoreValidate())->post()->goCheck('change');
        StoreLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new StoreValidate())->post()->goCheck('delete');
        StoreLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new StoreValidate())->goCheck('detail');
        $result = StoreLogic::detail($params);
        return $this->data($result);
    }

}