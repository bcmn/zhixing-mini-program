<?php

namespace app\adminapi\controller\business;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\goods\GoodsLists;
use app\adminapi\logic\goods\GoodsCateLogic;
use app\adminapi\validate\goods\GoodsValidate;
use think\response\Json;

/**
 * 服务项目控制器
 * Class BusinessController
 */
class BusinessController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new GoodsLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new GoodsValidate())->post()->goCheck('add');
        $result = GoodsCateLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(GoodsCateLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new GoodsValidate())->post()->goCheck('edit');
        $result = GoodsCateLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(GoodsCateLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new GoodsValidate())->post()->goCheck('change');
        GoodsCateLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new GoodsValidate())->post()->goCheck('delete');
        GoodsCateLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new GoodsValidate())->goCheck('detail');
        $result = GoodsCateLogic::detail($params);
        return $this->data($result);
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = GoodsCateLogic::getAllData($this->request->param('hid'));
        return $this->data($result);
    }

}