<?php

namespace app\adminapi\controller\orders;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\orders\OrdersRentLists;
use app\common\model\orders\OrdersRent;
use think\response\Json;

/**
 * 设备续租订单控制器
 */
class RentController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersRentLists());
    }

    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = $this->request->param();
        OrdersRent::destroy($params['id']);
        return $this->success('删除成功', [], 1, 1);
    }

}