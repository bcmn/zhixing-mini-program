<?php

namespace app\adminapi\controller\orders;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\orders\OrdersExpressLists;
use app\adminapi\logic\orders\OrdersExpressLogic;
use app\adminapi\validate\orders\OrdersExpressValidate;
use think\response\Json;

/**
 * 快递公司控制器
 */
class ExpressController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersExpressLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new OrdersExpressValidate())->post()->goCheck('add');
        $result = OrdersExpressLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(OrdersExpressLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new OrdersExpressValidate())->post()->goCheck('edit');
        $result = OrdersExpressLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(OrdersExpressLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new OrdersExpressValidate())->post()->goCheck('change');
        OrdersExpressLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new OrdersExpressValidate())->post()->goCheck('delete');
        OrdersExpressLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new OrdersExpressValidate())->goCheck('detail');
        $result = OrdersExpressLogic::detail($params);
        return $this->data($result);
    }


    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = OrdersExpressLogic::getAllData();
        return $this->data($result);
    }

}