<?php

namespace app\adminapi\controller\orders;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\orders\OrdersLists;
use app\adminapi\logic\orders\OrdersLogic;
use app\adminapi\validate\orders\OrdersValidate;
use app\common\logic\DeliveryLogic;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersExpress;
use app\common\model\orders\OrdersGoods;
use app\common\service\wechat\WeChatMessageService;
use think\response\Json;

/**
 * 订单控制器
 */
class OrdersController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('change');
        OrdersLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('delete');
        OrdersLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new OrdersValidate())->goCheck('detail');
        $result = OrdersLogic::detail($params);
        return $this->data($result);
    }

    /**
     * Date: 2023/10/21 14:26
     * Notes:订单发货（手动发货）
     */
    public function delivery(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('delivery');
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        $result = DeliveryLogic::delivery($ordersInfo, 2, ['express_company' => $params['express_company'], 'express_sn' => $params['express_sn']]);
        if (true === $result) {
            $orderInfo = Orders::where('id', $params['id'])->findOrEmpty();
            $goods = OrdersGoods::where('order_id', $orderInfo['id'])->column('goods_name');
            $goodsName = implode(',', $goods);
            $expressCompany = OrdersExpress::where('id', $orderInfo['express_company'])->value('title');
            if (!$orderInfo->isEmpty()) {
                $wechatMessage = new WeChatMessageService($orderInfo['user_id']);
                $wechatMessage->send([
                    'template_id' => 'BPaw0PZJqX00kCWwUypHyqukOqP2Mphgbziu6E0ef8Q',
                    'url' => '',
                    'data' => [
                        'character_string1' => ['value' => $orderInfo['sn']],   //订单编号
                        'thing12' => ['value' => $goodsName], //商品
                        'character_string2' => ['value' => $orderInfo['express_sn']],    //快递编号
                        'thing11' => ['value' => $expressCompany], //快递公司
                        'time7' => ['value' => date('Y-m-d H:i:s')], //发货时间
                    ]
                ]);
            }

            return $this->success('发货成功', [], 1, 1);
        }
        return $this->fail(DeliveryLogic::getError());
    }

    public function printOrder(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('delete');
        OrdersLogic::printOrder($params['id']);
        return $this->success('打印请求已发送', [], 1, 1);
    }

    /**
     * Date: 2023/10/21 14:26
     * Notes:订单确认收货
     */
    public function confirm(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('confirm');
        $result = OrdersLogic::confirm($params);
        if (true === $result) {
            return $this->success('订单确认成功', [], 1, 1);
        }
        return $this->fail(OrdersLogic::getError());
    }

    /**
     * Date: 2023/10/21 14:26
     * Notes: 取消订单
     */
    public function cancel(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('cancel');
        $result = OrdersLogic::cancel($params);
        if (true === $result) {
            return $this->success('订单取消成功', [], 1, 1);
        }
        return $this->fail(OrdersLogic::getError());
    }
}