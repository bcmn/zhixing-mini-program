<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\UserRepairLists;
use app\adminapi\logic\user\UserRepairLogic;
use app\adminapi\validate\user\UserRepairValidate;
use app\common\model\user\UserRepairLogs;
use think\response\Json;

/**
 * 维修申请管理
 */
class RepairController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new UserRepairLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new UserRepairValidate())->post()->goCheck('change');
        UserRepairLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }
}