<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\CaregiverLists;
use app\adminapi\lists\user\CustomerLists;
use app\adminapi\logic\user\CaregiverLogic;
use app\adminapi\logic\user\CustomerLogic;
use app\adminapi\validate\user\CaregiverValidate;
use app\adminapi\validate\user\CustomerValidate;
use think\response\Json;

/**
 * 客户控制器
 * Class CustomerController
 */
class CustomerController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new CustomerLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new CustomerValidate())->post()->goCheck('change');
        CustomerLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new CustomerValidate())->post()->goCheck('delete');
        CustomerLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new CustomerValidate())->goCheck('detail');
        $result = CustomerLogic::detail($params);
        return $this->data($result);
    }

}