<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\UserApplyLists;
use app\adminapi\logic\user\UserApplyLogic;
use app\adminapi\validate\user\UserApplyValidate;
use think\response\Json;

/**
 * 加盟申请管理
 */
class ApplyController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new UserApplyLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new UserApplyValidate())->post()->goCheck('change');
        UserApplyLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }

}