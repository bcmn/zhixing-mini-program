<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\WithdrawLists;
use app\adminapi\validate\user\WithdrawValidate;
use app\adminapi\logic\user\WithdrawLogic;
use think\response\Json;

/**
 * 提现管理
 * Class WithdrawController
 */
class WithdrawController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new WithdrawLists());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new WithdrawValidate())->post()->goCheck('change');
        WithdrawLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }

}