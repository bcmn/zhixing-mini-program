<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\UserRepairLists;
use app\adminapi\lists\user\UserRepairLogsLists;
use app\adminapi\logic\user\UserRepairLogic;
use app\adminapi\logic\user\UserRepairLogsLogic;
use app\adminapi\validate\user\UserRepairLogsValidate;
use app\adminapi\validate\user\UserRepairValidate;
use app\common\model\user\UserRepairLogs;
use think\response\Json;

/**
 * 维修申请管理
 */
class RepairLogsController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new UserRepairLogsLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new UserRepairLogsValidate())->post()->goCheck('add');
        $result = UserRepairLogsLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(UserRepairLogsLogic::getError());
    }

    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new UserRepairLogsValidate())->post()->goCheck('edit');
        $result = UserRepairLogsLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(UserRepairLogsLogic::getError());
    }

    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new UserRepairLogsValidate())->post()->goCheck('delete');
        UserRepairLogsLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new UserRepairLogsValidate())->goCheck('detail');
        $result = UserRepairLogsLogic::detail($params);
        return $this->data($result);
    }
}