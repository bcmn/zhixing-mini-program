<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\FeedbackLists;
use think\response\Json;

/**
 * 意见反馈管理
 */
class FeedbackController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new FeedbackLists());
    }

}