<?php

namespace app\adminapi\controller\user;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\user\CaregiverLists;
use app\adminapi\logic\user\CaregiverLogic;
use app\adminapi\validate\user\CaregiverValidate;
use think\response\Json;

/**
 * 陪护师控制器
 * Class CaregiverController
 */
class CaregiverController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new CaregiverLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new CaregiverValidate())->post()->goCheck('add');
        $result = CaregiverLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(CaregiverLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new CaregiverValidate())->post()->goCheck('edit');
        $result = CaregiverLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(CaregiverLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new CaregiverValidate())->post()->goCheck('change');
        CaregiverLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new CaregiverValidate())->post()->goCheck('delete');
        CaregiverLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new CaregiverValidate())->goCheck('detail');
        $result = CaregiverLogic::detail($params);
        return $this->data($result);
    }

}