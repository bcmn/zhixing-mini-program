<?php

namespace app\adminapi\controller\goods;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\goods\GoodsCateLists;
use app\adminapi\logic\goods\GoodsCateLogic;
use app\adminapi\validate\goods\GoodsCateValidate;
use think\response\Json;

/**
 * 商品分类控制器
 */
class CateController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new GoodsCateLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new GoodsCateValidate())->post()->goCheck('add');
        $result = GoodsCateLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(GoodsCateLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new GoodsCateValidate())->post()->goCheck('edit');
        $result = GoodsCateLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(GoodsCateLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new GoodsCateValidate())->post()->goCheck('change');
        GoodsCateLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new GoodsCateValidate())->post()->goCheck('delete');
        GoodsCateLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new GoodsCateValidate())->goCheck('detail');
        $result = GoodsCateLogic::detail($params);
        return $this->data($result);
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = GoodsCateLogic::getAllData();
        return $this->data($result);
    }
}