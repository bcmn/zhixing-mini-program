<?php

namespace app\adminapi\controller\goods;

use app\adminapi\controller\BaseAdminController;
use app\adminapi\lists\goods\GoodsDevicesLists;
use app\adminapi\logic\goods\GoodsDevicesLogic;
use app\adminapi\validate\goods\GoodsDevicesValidate;
use think\response\Json;

/**
 * 商品设备控制器
 */
class DevicesController extends BaseAdminController
{

    /**
     * Date: 2023/5/17 23:48
     * Notes：列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new GoodsDevicesLists());
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('add');
        $result = GoodsDevicesLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(GoodsDevicesLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('edit');
        $result = GoodsDevicesLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(GoodsDevicesLogic::getError());
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:54
     * Notes：更新字段
     */
    public function change(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('change');
        GoodsDevicesLogic::change($params);
        return $this->success('更新成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：删除
     */
    public function delete(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('delete');
        GoodsDevicesLogic::delete($params);
        return $this->success('删除成功', [], 1, 1);
    }

    /**
     * Date: 2023/5/17 23:49
     * Notes：重置设备
     */
    public function reset(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('delete');
        GoodsDevicesLogic::reset($params);
        return $this->success('重置成功', [], 1, 1);
    }


    /**
     * Date: 2023/5/17 23:48
     * Notes：详情
     */
    public function detail(): Json
    {
        $params = (new GoodsDevicesValidate())->goCheck('detail');
        $result = GoodsDevicesLogic::detail($params);
        return $this->data($result);
    }

    /**
     * @return Json
     * Date: 2023/9/7 20:44
     * Notes：获取全部数据
     */
    public function all(): Json
    {
        $result = GoodsDevicesLogic::getAllData();
        return $this->data($result);
    }
}