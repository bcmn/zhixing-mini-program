<?php

namespace app\api\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserAddress;

/**
 * 用户地址逻辑
 */
class UserAddressLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            $is_default = $params['is_default'] ?? 0;
            if ($is_default == 1) {
                UserAddress::update(['is_default' => 0], ['user_id' => $params['user_id']]);
            }

            UserAddress::create([
                'user_id' => $params['user_id'],
                'name' => $params['name'] ?? '',
                'phone' => $params['phone'] ?? '',
                'province' => $params['province'],
                'city' => $params['city'],
                'district' => $params['district'],
                'address' => $params['address'],
                'is_default' => $is_default,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            $is_default = $params['is_default'] ?? 0;
            if ($is_default == 1) {
                UserAddress::update(['is_default' => 0], ['user_id' => $params['user_id']]);
            }
            
            UserAddress::update([
                'id' => $params['id'],
                'user_id' => $params['user_id'],
                'name' => $params['name'] ?? '',
                'phone' => $params['phone'] ?? '',
                'province' => $params['province'],
                'city' => $params['city'],
                'district' => $params['district'],
                'address' => $params['address'],
                'is_default' => $params['is_default'] ?? 0,
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserAddress::field('*')
            ->where('id', '=', $id)
            ->findOrEmpty()
            ->toArray();
    }


    /**
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete($id): void
    {
        UserAddress::destroy($id);
    }

    public static function setDefault($params): void
    {
        UserAddress::where('user_id', '=', $params['user_id'])->update(['is_default' => 0]);
        UserAddress::where('id', '=', $params['id'])->update(['is_default' => 1]);
    }

}