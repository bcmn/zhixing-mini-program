<?php

namespace app\api\logic\user;

use app\common\enum\user\AccountLogEnum;
use app\common\enum\WithdrawEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\user\User;
use app\common\model\user\Withdraw;
use app\common\service\ConfigService;

/**
 * 提现申请逻辑
 */
class WithdrawLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            $withdraw_fee = ConfigService::get('withdraw', 'withdraw_fee');
            $amount = $params['amount'];
            $amount_fee = $amount * ($withdraw_fee / 100);
            $amount_true = $amount - $amount_fee;

            $sn = 'W' . date('YmdHis') . random_int(10000, 99999);
            Withdraw::create([
                'sn' => $sn,
                'user_id' => $params['user_id'],
                'bank' => 'wx',
                'bank_card' => $params['bank_card'] ?? '',
                'name' => $params['name'] ?? '',
                'mobile' => $params['mobile'] ?? '',
                'amount' => $amount,
                'fee' => $withdraw_fee ?? 0,
                'amount_fee' => $amount_fee,
                'amount_true' => $amount_true,
                'status' => WithdrawEnum::STATUS_WAIT
            ]);

            //扣除用户余额
            $user = User::findOrEmpty($params['user_id']);
            $user->user_money -= $params['amount'];
            $user->save();

            AccountLogLogic::add($params['user_id'], AccountLogEnum::UM_DEC_WITHDRAW, AccountLogEnum::DEC, $params['amount'], $sn, '提现支出');

            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

}