<?php

namespace app\api\logic\user;

use app\common\enum\user\UserApplyEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserRepair;

/**
 * 申请维修逻辑
 */
class UserRepairLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            UserRepair::create([
                'user_id' => $params['user_id'],
                'sn' => $params['sn'],
                'title' => $params['title'] ?? '',
                'images' => $params['images'] ?? '',
                'content' => $params['content'] ?? '',
                'name' => $params['name'] ?? '',
                'phone' => $params['phone'] ?? '',
                'status' => UserApplyEnum::STATUS_WAIT
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserRepair::field('*')
            ->where('id', '=', $id)
            ->findOrEmpty()
            ->toArray();
    }

}