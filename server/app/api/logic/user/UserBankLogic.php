<?php

namespace app\api\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserBank;

/**
 * 用户地址逻辑
 */
class UserBankLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            UserBank::create([
                'user_id' => $params['user_id'],
                'name' => $params['name'],
                'bank_name' => $params['bank_name'],
                'bank_card' => $params['bank_card'],
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            UserBank::update([
                'id' => $params['id'],
                'user_id' => $params['user_id'],
                'name' => $params['name'],
                'bank_name' => $params['bank_name'],
                'bank_card' => $params['bank_card'],
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserBank::field('*')
            ->where('id', '=', $id)
            ->findOrEmpty()
            ->toArray();
    }


    /**
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete($id): void
    {
        UserBank::destroy($id);
    }

}