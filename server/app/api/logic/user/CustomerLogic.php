<?php

namespace app\api\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\customer\Customer;

/**
 * 申请陪护师逻辑
 */
class CustomerLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Customer::create([
                'user_id' => $params['user_id'],
                'name' => $params['name'] ?? '',
                'sex' => $params['sex'] ?? '',
                'age' => $params['age'] ?? '',
                'mobile' => $params['mobile'] ?? '',
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Customer::update([
                'id' => $params['id'],
                'name' => $params['name'] ?? '',
                'sex' => $params['sex'] ?? '',
                'age' => $params['age'] ?? '',
                'mobile' => $params['mobile'] ?? '',
                'status' => $params['mobile'] ?? DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Customer::destroy($params['id']);
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Customer::field('id,name,sex,age,mobile,status,status as status_text')
            ->where('id', '=', $params['id'])
            ->findOrEmpty()
            ->toArray();
    }
}