<?php

namespace app\api\logic\user;

use app\common\enum\CaregiverEnum;
use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsCate;
use app\common\model\caregiver\Caregiver;

/**
 * 申请陪护师逻辑
 */
class CaregiverLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            Caregiver::create([
                'user_id' => $params['user_id'],
                'image' => $params['image'] ?? '',
                'name' => $params['name'] ?? '',
                'sex' => $params['sex'] ?? '',
                'age' => $params['age'] ?? '',
                'mobile' => $params['mobile'] ?? '',
                'album' => $params['album'] ?? '',
                'license' => $params['license'] ?? 0,
                'idcard_name' => $params['idcard_name'] ?? '',
                'idcard' => $params['idcard'] ?? '',
                'idcard_front' => $params['idcard_front'] ?? '',
                'idcard_back' => $params['idcard_back'] ?? '',
                'status' => CaregiverEnum::STATUS_WAIT
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * @return bool
     * Date: 2023/9/6 21:17
     * Notes：编辑
     */
    public static function edit(array $params): bool
    {
        try {
            Caregiver::update([
                'id' => $params['id'],
                'image' => $params['image'] ?? '',
                'name' => $params['name'] ?? '',
                'sex' => $params['sex'] ?? '',
                'age' => $params['age'] ?? '',
                'mobile' => $params['mobile'] ?? '',
                'album' => $params['album'] ?? '',
                'license' => $params['license'] ?? 0,
                'idcard_name' => $params['idcard_name'] ?? '',
                'idcard' => $params['idcard'] ?? '',
                'idcard_front' => $params['idcard_front'] ?? '',
                'idcard_back' => $params['idcard_back'] ?? '',
                'status' => CaregiverEnum::STATUS_WAIT
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        $detail = Caregiver::field('id,image,name,sex,age,mobile,idcard_name,idcard,idcard_front,idcard_back,notes,album,license,score,orders_num,complaint_num,status,status as status_text')
            ->where('id', '=', $params['user_id'])
            ->findOrEmpty()
            ->toArray();
        $business = Goods::where('status', '=', DefaultEnum::SHOW)
            ->limit(10)
            ->select();
        $hospital = GoodsCate::where('status', '=', DefaultEnum::SHOW)
            ->limit(10)
            ->select();
        return ['detail' => $detail, 'business' => $business, 'hospital' => $hospital];
    }
}