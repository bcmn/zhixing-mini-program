<?php

namespace app\api\logic\user;

use app\common\enum\user\UserApplyEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserApply;

/**
 * 加盟申请逻辑
 */
class UserApplyLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            $result = UserApply::where('user_id', '=', $params['user_id'])->findOrEmpty();
            if (!$result->isEmpty()) {
                UserApply::update([
                    'id' => $result['id'],
                    'type' => $params['type'] ?? 1,
                    'name' => $params['name'] ?? '',
                    'phone' => $params['phone'] ?? '',
                    'address' => $params['address'],
                    'status' => UserApplyEnum::STATUS_WAIT
                ]);
            } else {
                UserApply::create([
                    'user_id' => $params['user_id'],
                    'type' => $params['type'] ?? 1,
                    'name' => $params['name'] ?? '',
                    'phone' => $params['phone'] ?? '',
                    'address' => $params['address'],
                    'status' => UserApplyEnum::STATUS_WAIT
                ]);
            }
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($user_id): array
    {
        return UserApply::field('*')
            ->where('user_id', '=', $user_id)
            ->findOrEmpty()
            ->toArray();
    }

}