<?php

namespace app\api\logic\user;

use app\common\enum\DefaultEnum;
use app\common\enum\user\UserApplyEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\user\UserApply;
use app\common\model\user\UserFavorite;

/**
 * 收藏逻辑
 */
class UserFavoriteLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            $res = UserFavorite::where('user_id', '=', $params['user_id'])
                ->where('goods_id', '=', $params['goods_id'])
                ->findOrEmpty();
            if ($res->isEmpty()) {
                $goods = Goods::where('id', '=', $params['goods_id'])->findOrEmpty();
                UserFavorite::create([
                    'user_id' => $params['user_id'],
                    'goods_id' => $params['goods_id'] ?? '',
                    'content' => json_encode($goods, true),
                    'status' => DefaultEnum::SHOW,
                ]);
                self::$error = '收藏成功';
            } else {
                UserFavorite::destroy($res['id']);
                self::$error = '取消收藏';
            }
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param $id
     * Date: 2023/10/23 23:22
     * Notes:删除收藏
     */
    public static function delete($id): void
    {
        UserFavorite::destroy($id);
    }

}