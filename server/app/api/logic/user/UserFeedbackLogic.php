<?php

namespace app\api\logic\user;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\user\UserFeedback;

/**
 * 用户反馈逻辑
 */
class UserFeedbackLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool
    {
        try {
            UserFeedback::create([
                'user_id' => $params['user_id'],
                'content' => $params['content'],
                'images' => $params['images'] ?? '',
                'status' => DefaultEnum::SHOW
            ]);
            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return UserFeedback::field('*')
            ->where('id', '=', $id)
            ->findOrEmpty()
            ->toArray();
    }

}