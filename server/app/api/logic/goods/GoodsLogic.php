<?php

namespace app\api\logic\goods;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsAttribute;
use app\common\model\goods\GoodsSku;
use app\common\model\user\UserFavorite;

/**
 * 商品逻辑
 */
class GoodsLogic extends BaseLogic
{
    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id, $user_id): array
    {
        $detail = Goods::where('id', '=', $id)
            ->field('*,cate_id as cate')
            ->findOrEmpty()
            ->toArray();
        $detail['is_favorite'] = 0;
        if (!empty($user_id)) {
            $detail['is_favorite'] = UserFavorite::where('user_id', '=', $user_id)
                ->where('goods_id', '=', $id)
                ->count();
        }

        $detail['attribute'] = GoodsAttribute::where('goods_id', '=', $id)
            ->field('title,values')
            ->order('id asc')
            ->select();
        $detail['sku'] = GoodsSku::where('goods_id', '=', $id)
            ->field('id,image,attribute,price,market_price,cost_price,stock')
            ->order('id asc')
            ->select();

        return $detail;
    }

    /**
     * @param $goods_id
     * @param $sku_id
     * Date: 2024/4/8 15:32
     * Notes: 获取商品最新价格
     * @return array
     */
    public static function getLatestPrice($goods_id, $sku_id): array
    {
        if (empty($sku_id)) {
            $goods = Goods::where('id', '=', $goods_id)->findOrEmpty();
        } else {
            $goods = GoodsSku::where('goods_id', '=', $goods_id)
                ->where('id', '=', $sku_id)
                ->value('price');
        }
        $price = $goods['price'] ?? 0;
        $learn_price = $goods['learn_price'] ?? 0;
        $security_amount = $goods['security_amount'] ?? 0;
        return [
            'price' => $price,
            'learn_price' => $learn_price,
            'security_amount' => $security_amount,
        ];
    }

}