<?php

namespace app\api\logic\goods;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsCate;

/**
 * 医院逻辑
 */
class HospitalLogic extends BaseLogic
{
    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        $detail = GoodsCate::field('id,name,logo,level,video,business,content,address,lng,lat,services')
            ->findOrEmpty($params['id'])
            ->toArray();

        $services = json_decode($detail['services'], true);
        $has = [];
        foreach ($services as $item) {
            if ($item['price'] > 0) {
                $has[] = $item['id'];
            }
        }

        $business = Goods::where('status', '=', DefaultEnum::SHOW)
            ->where('is_index', '=', 0)
            ->where('id', 'in', $has)
            ->field('id,icon,title,notes,amount')
            ->select();

        return [
            'detail' => $detail,
            'business' => $business
        ];
    }

}