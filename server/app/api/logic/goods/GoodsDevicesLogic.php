<?php

namespace app\api\logic\goods;

use app\common\enum\DefaultEnum;
use app\common\enum\goods\GoodsDevicesEnum;
use app\common\enum\goods\GoodsEnum;
use app\common\enum\orders\OrdersEnum;
use app\common\logic\BaseLogic;
use app\common\logic\CommissionLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\goods\GoodsDevicesTransfer;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersLearn;
use app\common\model\orders\OrdersGoods;
use app\common\model\refund\RefundRecord;
use app\common\model\user\User;
use app\common\service\MachineService;
use think\facade\Db;
use think\response\Json;

/**
 * 设备逻辑
 */
class GoodsDevicesLogic extends BaseLogic
{
    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($sn, $user_id): array
    {
        $detail = GoodsDevices::where('sn', '=', $sn)
            ->field('id,user_id,goods_id,order_id,order_goods_id,order_sn,goods_name,sku_name,security_amount,lease_type,learn_price,sn,is_active,status,active_time,end_time,A_name,A_num,A_num_used,B_name,B_num,B_num_used,C_name,C_num,C_num_used,D_name,D_num,D_num_used,E_name,E_num,E_num_used')
            ->with(['userInfo', 'goodsInfo', 'ordersInfo', 'orderGoodsInfo'])
            ->findOrEmpty();

        // 设备待激活，绑定用户订单信息
        if ($detail['is_active'] == 0 && $user_id > 0) {
            $orders = self::getActiveOrders($detail, $user_id);
            if ($orders) {
                $detail['ordersInfo'] = $orders['ordersInfo'];
                $detail['orderGoodsInfo'] = $orders['orderGoodsInfo'];
                $detail['userInfo'] = User::where('id', '=', $user_id)->field('id,avatar,nickname,mobile')->findOrEmpty()->toArray();
            }
        }

        $detail['is_end'] = $detail['end_time'] > time() ? 0 : 1;
        $detail['sales_num'] = OrdersLearn::where('devices_id', '=', $detail['id'])->count();
        $detail['end_time'] = $detail['end_time'] > 0 ? date('Y-m-d H:i:s', $detail['end_time']) : '';

        $detail['services_status'] = RefundRecord::where('devices_sn', '=', $detail['sn'])->order('id desc')->count();
        $detail['refund_status'] = RefundRecord::where('devices_sn', '=', $detail['sn'])->order('id desc')->value('refund_status');
        $detail['refund_type'] = RefundRecord::where('devices_sn', '=', $detail['sn'])->order('id desc')->value('refund_type');

        $latestPrice = GoodsLogic::getLatestPrice($detail['goods_id'], $detail['sku_id']);
        $detail['price'] = $latestPrice['price'];
        $detail['learn_price'] = $latestPrice['learn_price'];
        $detail['security_amount'] = $latestPrice['security_amount'];

        return $detail->toArray();
    }

    /**
     * @param $params
     * Date: 2023/11/3 23:02
     * Notes:激活绑定设备
     */
    public static function active($params): bool|string
    {
        $devices = GoodsDevices::where('sn', '=', $params['sn'])->findOrEmpty();
        if ($devices->isEmpty()) {
            self::$error = '查询不到设备信息';
            return false;
        }

        if ($devices['is_active'] == GoodsDevicesEnum::ACTIVE_YES) {
            self::$error = '设备已激活绑定，无法重复绑定';
            return false;
        }

//        if (!empty($devices['user_id']) && $devices['user_id'] != $params['user_id']) {
//            self::$error = '这不是您的机器，无法绑定';
//            return false;
//        }

        $orders = self::getActiveOrders($devices, $params['user_id']);
        if ($orders === false) {
            return false;
        }
        $orderGoodsInfo = $orders['orderGoodsInfo'];
        $ordersInfo = $orders['ordersInfo'];

        Orders::update(['status' => OrdersEnum::STATUS_FINISH, 'is_active' => GoodsDevicesEnum::ACTIVE_YES], ['id' => $ordersInfo['id']]);

        $devices->order_id = $ordersInfo['id'];
        $devices->order_goods_id = $orderGoodsInfo['id'];
        $devices->order_sn = $ordersInfo['sn'];
        $devices->user_id = $params['user_id'];
        $devices->is_active = GoodsDevicesEnum::ACTIVE_YES;
        $devices->lease_type = $orderGoodsInfo['lease_type'];
        $devices->end_time = time() + $orderGoodsInfo['num'] * 31 * 24 * 60 * 60;
        $devices->active_time = time();
        $devices->lng = $params['lng'] ?? '';
        $devices->lat = $params['lat'] ?? '';
        $devices->save();

        //更新设备商品及设备配件商品的销量
        Goods::where('id', $orderGoodsInfo['goods_id'])->inc('sales', $orderGoodsInfo['num'])->save();
        $partIds = Goods::where('goods_id', '=', $orderGoodsInfo['goods_id'])->column('id');
        if (!empty($partIds)) {
            $partLists = OrdersGoods::where('order_id', '=', $orderGoodsInfo['order_id'])
                ->where('goods_type', '=', GoodsEnum::TYPE_PART)
                ->where('goods_id', 'in', $partIds)
                ->select()
                ->toArray();
            if (!empty($partLists)) {
                foreach ($partLists as $v) {
                    Goods::where('id', $v['goods_id'])->inc('sales', $v['num'])->save();
                }
            }
        }

        //订单分红
        CommissionLogic::settlement(['id' => $ordersInfo['id'], 'type' => 'orders']);
        return true;
    }

    /**
     * @param $params
     * @param $user_id
     * Date: 2024/1/24 21:33
     * Notes:获取用户待激活的订单
     */
    public static function getActiveOrders($params, $user_id)
    {
        $orders = (new Orders())->alias('o')
            ->join('orders_goods og', 'o.id=og.order_id')
            ->field('o.id as order_id,og.id as order_goods_id')
            ->where('o.user_id', $user_id)
            ->where('og.goods_id', $params['goods_id'])
            ->where('og.lease_type', $params['lease_type'])
            ->where('o.is_active', GoodsDevicesEnum::ACTIVE_NO)
            ->where('o.pay_status', 1)
            ->whereIn('o.status', [OrdersEnum::STATUS_PAY, OrdersEnum::STATUS_DELIVER, OrdersEnum::STATUS_FINISH])
            ->order('o.id asc')
            ->findOrEmpty();

        if ($orders->isEmpty()) {
            self::$error = '查询不到商品信息，请确认是否有待激活设备';
            return false;
        }

        // 查找当前用户扫码商品的订单商品
        $orderGoodsInfo = OrdersGoods::where('id', $orders['order_goods_id'])
            ->findOrEmpty();

        // 查找用户待收货的订单
        $ordersInfo = Orders::where('id', $orders['order_id'])
            ->findOrEmpty();

        return ['ordersInfo' => $ordersInfo, 'orderGoodsInfo' => $orderGoodsInfo];
    }

    /**
     * @param $params
     * Date: 2023/11/3 23:02
     * Notes:激活绑定设备，带订单号旧版，已弃用
     */
    public static function active_old($params): bool|string
    {
        $devices = GoodsDevices::where('sn', '=', $params['sn'])->findOrEmpty();
        if ($devices->isEmpty()) {
            self::$error = '查询不到设备信息';
            return false;
        }

        if ($devices['is_active'] == GoodsDevicesEnum::ACTIVE_YES) {
            self::$error = '设备已激活绑定，无法重复绑定';
            return false;
        }

//        if (!empty($devices['user_id']) && $devices['user_id'] != $params['user_id']) {
//            self::$error = '这不是您的机器，无法绑定';
//            return false;
//        }

        $orderGoodsInfo = OrdersGoods::where('id', '=', $devices['order_goods_id'])->findOrEmpty();
        if ($orderGoodsInfo->isEmpty()) {
            self::$error = '查询不到商品信息，请确认';
            return false;
        }

        $order_sn = !empty($params['order_sn']) ? $params['order_sn'] : (!empty($devices['order_sn']) ? $devices['order_sn'] : '');
        if (empty($order_sn)) {
            self::$error = '缺少订单信息';
            return false;
        }
        Db::startTrans();

        $order = Orders::where('sn', '=', $order_sn)->findOrEmpty();
        if ($order->isEmpty()) {
            self::$error = '缺少订单信息';
            return false;
        }

        $order->status = OrdersEnum::STATUS_FINISH;
        $order->save();

        $devices->user_id = empty($devices['user_id']) ? $params['user_id'] : $devices['user_id'];
        $devices->is_active = GoodsDevicesEnum::ACTIVE_YES;
        $devices->lease_type = $orderGoodsInfo['lease_type'];
        $devices->end_time = time() + $orderGoodsInfo['num'] * 31 * 24 * 60 * 60;
        $devices->active_time = time();
        $devices->lng = $params['lng'] ?? '';
        $devices->lat = $params['lat'] ?? '';
        $devices->save();

        //更新设备商品及设备配件商品的销量
        Goods::where('id', $orderGoodsInfo['goods_id'])->inc('sales', $orderGoodsInfo['num'])->save();
        $partIds = Goods::where('goods_id', '=', $orderGoodsInfo['goods_id'])->column('id');
        if (!empty($partIds)) {
            $partLists = OrdersGoods::where('order_id', '=', $orderGoodsInfo['order_id'])
                ->where('goods_type', '=', GoodsEnum::TYPE_PART)
                ->where('goods_id', 'in', $partIds)
                ->select()
                ->toArray();
            if (!empty($partLists)) {
                foreach ($partLists as $v) {
                    Goods::where('id', $v['goods_id'])->inc('sales', $v['num'])->save();
                }
            }
        }

        //订单分红
        CommissionLogic::settlement(['id' => $order['id'], 'type' => 'orders']);

        Db::commit();
        return true;
    }

    /**
     * Date: 2023/11/15 14:46
     * Notes:设备转让
     */
    public static function transfer($params): bool|string
    {
        $device = GoodsDevices::where('sn', '=', $params['sn'])
            ->where('user_id', '=', $params['user_id'])
            ->findOrEmpty();
        if ($device->isEmpty()) {
            self::$error = '查询不到设备信息';
            return false;
        }

        $receive = User::where('mobile', '=', $params['mobile'])->findOrEmpty();
        if ($receive->isEmpty()) {
            self::$error = '用户不存在';
            return false;
        }

        GoodsDevicesTransfer::create([
            'devices_id' => $device['id'],
            'sn' => $device['sn'],
            'user_id' => $device['user_id'],
            'receive_id' => $receive['id']
        ]);

        GoodsDevices::update([
            'id' => $device['id'],
            'user_id' => $receive['id']
        ]);

        return true;
    }

    /**
     * @param $params
     * Date: 2024/2/13 8:26
     * Notes:设备录入
     */
    public static function add($params): bool|string
    {
        $devices_manager = User::where('id', '=', $params['user_id'])->value('devices_manager');
        if ($devices_manager == 0) {
            self::$error = '没有设备入库权限';
            return false;
        }

        $device = GoodsDevices::where('sn', '=', $params['sn'])
            ->findOrEmpty();
        if (!$device->isEmpty()) {
            self::$error = '设备已在库，无需重复入库';
            return false;
        }

        $goods = Goods::where('id', '=', $params['goods_id'])->findOrEmpty();
        if ($goods->isEmpty()) {
            self::$error = '查询不到商品信息';
            return false;
        }

        if (empty($goods['api_id'])) {
            self::$error = '商品缺少onenet平台ID，无法创建，请补充后重试';
            return false;
        }

        $insertData['goods_id'] = $params['goods_id'];
        $insertData['goods_name'] = $goods['name'];
        $insertData['goods_image'] = $goods['image'];
        $insertData['title'] = generate_sn(GoodsDevices::class, 'title');
        $insertData['sn'] = $params['sn'];
        $insertData['imsi'] = $params['imsi'];
        $insertData['lease_type'] = $goods['lease_type'];
        $insertData['security_amount'] = $goods['security_amount'];
        $insertData['price'] = $goods['price'];
        $insertData['learn_price'] = $goods['learn_price'];
        $insertData['is_active'] = 0;
        $insertData['sort'] = 0;
        $insertData['status'] = DefaultEnum::SHOW;
        $insertData['create_id'] = $params['user_id'];

        Db::startTrans();
        GoodsDevices::create($insertData);

        $data = [
            'product_id' => $goods['api_id'],
            'device_name' => generate_sn(GoodsDevices::class, 'title'),
            'imei' => $params['sn'],
            'imsi' => $params['imsi']
        ];
        $result = MachineService::createDevices($data);

        if (false === $result) {
            Db::rollback();
            self::$error = MachineService::$error;
            return false;
        }

        Db::commit();
        return true;
    }
}