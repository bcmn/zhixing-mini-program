<?php

namespace app\api\logic\goods;

use app\common\enum\OrdersEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsSku;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\user\User;
use app\common\model\user\UserAddress;
use app\common\service\pay\WeChatPayService;
use think\facade\Db;

/**
 * 服务订单
 * Class GoodsCateLogic
 */
class OrdersLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     * goods：[{goods_id:1,sku_id:2,num:1},{goods_id:1,sku_id:2,num:1}]
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            //处理商品信息
            $goodsInfo = json_decode($params['goods'], true);
            $goods = [];
            $goods_amount = 0;
            foreach ($goodsInfo as $key => $item) {
                $goodsDetail = Goods::where('id', '=', $item['goods_id'])->findOrEmpty();
                $skuDetail = '';
                if ($item['sku_id'] > 0) {
                    $skuDetail = GoodsSku::where('id', '=', $item['sku_id'])->findOrEmpty();
                }
                $goods[$key]['orders_id'] = 0;
                $goods[$key]['goods_id'] = $item['goods_id'];
                $goods[$key]['goods_name'] = $goodsDetail['name'];
                $goods[$key]['goods_type'] = $goodsDetail['type'];
                $goods[$key]['sku_id'] = $item['sku_id'];
                $goods[$key]['sku_name'] = $skuDetail ? $skuDetail['name'] : '';
                $goods[$key]['num'] = $item['num'];
                $goods[$key]['use_num'] = $item['use_num'];
                $goods[$key]['num_price'] = $skuDetail ? $skuDetail['num_price'] : $goodsDetail['num_price'];
                $goods[$key]['price'] = $skuDetail ? $skuDetail['price'] : $goodsDetail['price'];
                $goods[$key]['total_price'] = $goods[$key]['num'] * $goods[$key]['price'] + $goods[$key]['use_num'] * $goods[$key]['num_price'];
                $goods[$key]['goods'] = json_encode($goodsDetail);
                $goods[$key]['sku'] = $skuDetail ? json_encode($skuDetail) : '';
                $goods[$key]['status'] = 1;

                $goods_amount += $goods[$key]['total_price'];
            }

            //处理优惠券
            $coupon_amount = 0;

            //处理余额支付
            $balance_amount = 0;

            //处理积分抵扣
            $integral_amount = 0;

            //处理邮费
            $postage = 0;

            $order_amount = $goods_amount - $coupon_amount - $balance_amount - $integral_amount + $postage;

            // 读取收货地址信息
            $addressInfo = UserAddress::where('id', '=', $params['address_id'])->findOrEmpty();

            $sn = generate_sn(Orders::class, 'sn');
            $result = Orders::create([
                'sn' => $sn,
                'user_id' => $params['user_id'],
                'goods_amount' => $goods_amount,
                'coupon_amount' => $coupon_amount,
                'balance_amount' => $balance_amount,
                'integral_amount' => $integral_amount,
                'postage' => $postage,
                'order_amount' => $order_amount,
                'address_id' => $params['address_id'],
                'name' => $addressInfo['name'],
                'phone' => $addressInfo['phone'],
                'address' => $addressInfo['address'],
                'notes' => $params['notes'] ?? '',
                'status' => OrdersEnum::STATUS_WAIT
            ]);

            //处理订单商品
            foreach ($goods as $key => $item) {
                $goods[$key]['orders_id'] = $result->id;
            }
            (new OrdersGoods())->saveAll($goods);

            return $result->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Orders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Orders::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Orders::field('*')->findOrEmpty($id)->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 分配订单
     */
    public static function allocation($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->caregiver_id = $params['caregiver_id'];
        $ordersInfo->allocation_time = time();
        $ordersInfo->save();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        Db::startTrans();
        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->finish_time = time();
        $ordersInfo->save();

        //给陪护师增加余额
        if (!empty($ordersInfo['caregiver_id'])) {
            $caregiver = User::findOrEmpty($ordersInfo['caregiver_id']);
            $caregiver->user_money += $ordersInfo['pay_amount'];
            $caregiver->save();

            AccountLogLogic::add($ordersInfo['caregiver_id'], AccountLogEnum::UM_INC_ORDERS, AccountLogEnum::INC, $ordersInfo['pay_amount'], $ordersInfo['sn'], '订单收入');
        }
        Db::commit();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }
}