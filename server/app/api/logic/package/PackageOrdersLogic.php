<?php

namespace app\api\logic\package;

use app\common\enum\OrdersEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\Orders;
use app\common\model\customer\Customer;
use app\common\model\package\PackageOrders;

/**
 * 服务订单
 * Class GoodsCateLogic
 */
class PackageOrdersLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool|array
    {
        try {
            $package = Goods::where('id', '=', $params['package_id'])->findOrEmpty();
            if ($package->isEmpty()) {
                self::$error = '查询不到项目信息';
                return false;
            }
            $sn = generate_sn(Orders::class, 'sn');
            $result = PackageOrders::create([
                'sn' => $sn,
                'user_id' => $params['user_id'],
                'package_id' => $params['package_id'],
                'customer_id' => $params['customer_id'],
                'customer_name' => Customer::where('id', '=', $params['customer_id'])->value('name'),
                'customer_mobile' => Customer::where('id', '=', $params['customer_id'])->value('mobile'),
                'order_amount' => $package['amount'],
                'status' => OrdersEnum::STATUS_WAIT
            ]);

            return $result->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        PackageOrders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        PackageOrders::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return PackageOrders::findOrEmpty($id)->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 分配订单
     */
    public static function allocation($params): bool
    {
        $ordersInfo = PackageOrders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->caregiver_id = $params['caregiver_id'];
        $ordersInfo->allocation_time = time();
        $ordersInfo->save();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = PackageOrders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->finish_time = time();
        $ordersInfo->save();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = PackageOrders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }
}