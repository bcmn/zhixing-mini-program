<?php

namespace app\api\logic\package;

use app\common\logic\BaseLogic;
use app\common\model\package\Package;

/**
 * 服务套餐逻辑
 * Class PackageLogic
 */
class PackageLogic extends BaseLogic
{

    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Package::findOrEmpty($params['id'])->toArray();
    }

}