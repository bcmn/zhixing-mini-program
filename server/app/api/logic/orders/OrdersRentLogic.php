<?php

namespace app\api\logic\orders;

use app\common\enum\orders\OrdersRentEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\OrdersRent;

/**
 * 续租订单
 */
class OrdersRentLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            $devices = GoodsDevices::where('sn', '=', $params['sn'])
                ->where('is_active', '=', 1)
                ->findOrEmpty();
            if ($devices->isEmpty()) {
                self::$error = '查询不到设备信息';
                return false;
            }

            $goodsInfo = Goods::where('id', '=', $devices['goods_id'])->findOrEmpty();
            if ($goodsInfo->isEmpty()) {
                self::$error = '商品信息出错';
                return false;
            }

            $goods_amount = $devices['price'];
            $goods_num = $params['num'] ?? 1;
            $insert = OrdersRent::create([
                'sn' => generate_sn(OrdersRent::class, 'sn'),
                'devices_id' => $devices['id'],
                'devices_sn' => $devices['sn'],
                'lease_type' => $devices['lease_type'] ?? 0,
                'user_id' => $params['user_id'],
                'goods_amount' => $goods_amount,
                'num' => $goods_num,
                'order_amount' => $goods_amount * $goods_num,
                'status' => OrdersRentEnum::STATUS_WAIT
            ]);
            return $insert->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

}