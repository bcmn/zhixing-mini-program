<?php

namespace app\api\logic\orders;

use app\common\enum\orders\OrdersDevicesEnum;
use app\common\enum\orders\OrdersEnum;
use app\common\enum\orders\OrdersRenewEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\business\Hospital;
use app\common\model\business\Store;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersDevices;
use app\common\model\orders\OrdersRenew;
use app\common\model\user\User;

/**
 * 设备预约订单(用户)
 */
class OrdersDevicesLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            $devices = GoodsDevices::where('sn', '=', $params['sn'])
                ->where('is_active', '=', 1)
                ->findOrEmpty();
            if ($devices->isEmpty()) {
                self::$error = '查询不到设备信息';
                return false;
            }

            $goodsInfo = Goods::where('id', '=', $devices['goods_id'])->findOrEmpty();
            if ($goodsInfo->isEmpty()) {
                self::$error = '商品信息出错';
                return false;
            }

            $goods_amount = $devices['learn_price'];
            $goods_num = $params['num'] ?? 1;
            $type = $params['type'] ?? 1;
            $insert = OrdersDevices::create([
                'sn' => generate_sn(OrdersRenew::class, 'sn'),
                'devices_id' => $devices['id'],
                'goods_id' => $devices['goods_id'],
                'user_id' => $params['user_id'],
                'lease_type' => $goodsInfo['lease_type'],
                'type' => $type,
                'appoint_time' => !empty($params['time']) ? strtotime($params['time']) : time() + 24 * 60 * 60,
                'goods_amount' => $goods_amount,
                'num' => $goods_num,
                'order_amount' => $goods_amount * $goods_num,
                'notes' => $params['notes'] ?? '',
                'status' => OrdersRenewEnum::STATUS_WAIT,
                'store_user_id' => $devices['user_id'],
                'store_id' => $type == 1 ? Store::where('user_id', '=', $devices['user_id'])->value('id') : Hospital::where('user_id', '=', $devices['user_id'])->value('id'),
            ]);
            return $insert->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        OrdersDevices::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        $detail = OrdersDevices::field('*,status as status_text')
            ->with(['userInfo', 'devices', 'goodsInfo'])
            ->findOrEmpty($id);
        if (!$detail->isEmpty()) {
            if ($detail['type'] == 2) {
                $detail['hospital'] = Hospital::where('id', '=', $detail['store_id'])->findOrEmpty();
            } else {
                $detail['store'] = Hospital::where('id', '=', $detail['store_id'])->findOrEmpty();
            }
        }
        return $detail->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = OrdersDevices::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersDevicesEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersDevicesEnum::STATUS_SUCCESS;
        $ordersInfo->confirm_time = time();
        $ordersInfo->save();

        //给用户增加积分
        if ($ordersInfo['give_integral'] > 0) {
            $userInfo = User::where('id', '=', $ordersInfo['user_id'])->findOrEmpty();
            $userInfo->user_integral += $ordersInfo['give_integral'];
            $userInfo->save();

            // 增加积分记录
            AccountLogLogic::add(
                $userInfo['id'],
                AccountLogEnum::UI_INC_BUY_GOODS,
                AccountLogEnum::INC,
                $ordersInfo['give_integral'],
                $ordersInfo['sn'],
                '购物赠送积分'
            );
        }

        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }

}