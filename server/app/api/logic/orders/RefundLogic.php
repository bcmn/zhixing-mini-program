<?php

namespace app\api\logic\orders;

use app\common\enum\orders\OrdersEnum;
use app\common\enum\RefundEnum;
use app\common\logic\BaseLogic;
use app\common\model\business\Store;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\orders\OrdersRecovery;
use app\common\model\refund\RefundRecord;
use think\facade\Db;
use think\response\Json;

/**
 * 订单退款
 */
class RefundLogic extends BaseLogic
{
    /**
     * Date: 2023/11/21 21:13
     * Notes:退款申请
     */
    public static function refund($params): Json|bool
    {
        Db::startTrans();
        $orderInfo = Orders::where('id', '=', $params['order_id'])->findOrEmpty();
        if ($orderInfo->isEmpty()) {
            self::$error = '查询不到订单信息';
            return false;
        }

        $orderInfo->status = OrdersEnum::STATUS_REFUND;
        $orderInfo->save();

        $orderGoods = OrdersGoods::where('id', '=', $params['order_goods_id'])->findOrEmpty();
        if ($orderGoods->isEmpty()) {
            self::$error = '查询不到商品信息';
            return false;
        }

        $refund_type = $params['refund_type'] ?? 1;
        if ($refund_type == RefundEnum::TYPE_SECURITY || $refund_type == RefundEnum::TYPE_SECURITY_STORE) {
            // 退压金
            $refund_amount = $orderGoods['security_amount'];
        } else {
            // 退订单商品总额
            $refund_amount = $orderGoods['price'] * $orderGoods['num'];
        }

        $store_id = $params['store_id'] ?? 0;
        RefundRecord::create([
            'sn' => generate_sn(RefundRecord::class, 'sn'),
            'user_id' => $params['user_id'],
            'order_id' => $orderInfo['id'],
            'order_sn' => $orderInfo['sn'],
            'devices_sn' => GoodsDevices::where('order_goods_id', '=', $orderGoods['id'])->value('sn'),
            'order_type' => 'order',
            'order_amount' => $orderInfo['order_amount'],
            'goods_amount' => $orderGoods['total_price'],
            'refund_amount' => $refund_amount,
            'refund_type' => $refund_type,
            'store_id' => $store_id,
            'store_user_id' => Store::where('id', '=', $store_id)->value('user_id') ?? 0,
            'notes' => $params['notes'] ?? '',
            'images' => $params['images'] ?? '',
            'order_goods_id' => $params['order_goods_id'] ?? 0,
            'refund_way' => $orderInfo['pay_way'],
            'transaction_id' => $orderInfo['transaction_id']
        ]);

        Db::commit();
        return true;
    }

    /**
     * Date: 2023/11/21 22:18
     * Notes:订单退款详情
     */
    public static function detail($params)
    {
        $orderGoodsInfo = OrdersGoods::where('id', '=', $params['order_goods_id'])->findOrEmpty();
        if ($orderGoodsInfo->isEmpty()) {
            self::$error = '查询不到订单商品信息';
            return false;
        }

        $detail = RefundRecord::where('order_goods_id', '=', $orderGoodsInfo['id'])
            ->field('*,refund_type as refund_type_text,refund_status as refund_status_text')
            ->order('id', 'desc')
            ->findOrEmpty()
            ->toArray();
        $detail['goodsInfo'] = OrdersGoods::where('id', '=', $detail['order_goods_id'])->findOrEmpty();
        $detail['orderInfo'] = Orders::where('id', '=', $detail['order_id'])->findOrEmpty();
        $detail['devicesInfo'] = GoodsDevices::where('id', '=', $detail['devices_sn'])->findOrEmpty();

        return $detail;
    }

    /**
     * @param $params
     * Date: 2023/11/30 22:18
     * Notes:确认货物寄回
     */
    public static function express($params)
    {
        if (empty($params['id'])) {
            self::$error = '缺少退货记录ID';
            return false;
        }
        $detail = RefundRecord::where('id', '=', $params['id'])->findOrEmpty();

        if ($detail->isEmpty()) {
            self::$error = '查询不到退货记录';
            return false;
        }

        if ($detail['refund_status'] != RefundEnum::REFUND_CONFIRM) {
            self::$error = '退货状态异常';
            return false;
        }

        if (empty($params['company']) || empty($params['number'])) {
            self::$error = '缺少快递信息';
            return false;
        }

        $detail->express_company = $params['company'];
        $detail->express_no = $params['number'];
        $detail->refund_status = RefundEnum::REFUND_POSTAGE;
        $detail->save();
        return true;
    }


    /**
     * @param $params
     * Date: 2023/12/15 21:10
     * Notes:退款操作
     */
    public static function confirm($params): bool
    {
        $records = RefundRecord::where('id', '=', $params['id'])->findOrEmpty();
        $refund_status = '';
        switch ($params['status']) {
            case 1:
                $refund_status = $records['refund_type'] == RefundEnum::TYPE_MONEY ? RefundEnum::REFUND_PAY : RefundEnum::REFUND_CONFIRM;
                break;
            case 3:
                $refund_status = RefundEnum::REFUND_PAY;
                break;
            case 4:
                $refund_status = RefundEnum::REFUND_SUCCESS;
                break;
            case 5:
                $refund_status = RefundEnum::REFUND_ERROR;
                break;
        }

        if (!$records->isEmpty() && $refund_status) {
            $records->refund_status = $refund_status;
            $records->save();

            $orderInfo = Orders::where('id', '=', $records['order_id'])->findOrEmpty()->toArray();
            \app\common\logic\RefundLogic::log($orderInfo, $records['id'], $records['refund_amount'], $params['user_id'], $records->refund_status);
        }
        return true;
    }
}