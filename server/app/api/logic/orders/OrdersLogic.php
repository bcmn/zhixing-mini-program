<?php

namespace app\api\logic\orders;

use app\common\enum\CouponEnum;
use app\common\enum\orders\OrdersEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\coupon\CouponRecord;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsSku;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\user\User;
use app\common\model\user\UserAddress;
use app\common\service\ConfigService;
use app\common\service\pay\WeChatPayService;
use think\facade\Db;

/**
 * 服务订单
 * Class GoodsCateLogic
 */
class OrdersLogic extends BaseLogic
{

    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     * goods： [{"goods_id":16,"sku_id":5,"num":1},{"goods_id":13,"sku_id":0,"num":1}]
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            Db::startTrans();
            //处理商品信息
            $goodsInfo = json_decode($params['goods'], true);
            $goods = [];
            $goods_amount = 0;  //商品总金额，普通商品的购买金额，租赁商品的租金
            $security_amount = 0;  //租赁商品的压金
            $give_integral = 0; //赔送积分数量
            foreach ($goodsInfo as $key => $item) {
                $goodsDetail = Goods::where('id', '=', $item['goods_id'])->findOrEmpty();
                $skuDetail = '';
                if ($item['sku_id'] > 0) {
                    $skuDetail = GoodsSku::where('id', '=', $item['sku_id'])->findOrEmpty();
                }
                $goods[$key]['user_id'] = $params['user_id'];
                $goods[$key]['order_id'] = 0;
                $goods[$key]['goods_id'] = $item['goods_id'];
                $goods[$key]['goods_name'] = $goodsDetail['name'];
                $goods[$key]['goods_image'] = $goodsDetail['image'];
                $goods[$key]['goods_type'] = $goodsDetail['type'];
                $goods[$key]['lease_type'] = $goodsDetail['lease_type'];
                $goods[$key]['function_type'] = $goodsDetail['function_type'];
                $goods[$key]['sku_id'] = $item['sku_id'];
                $goods[$key]['sku_name'] = $skuDetail ? $skuDetail['name'] : '';
                $goods[$key]['sku_image'] = $skuDetail ? $skuDetail['image'] : '';
                $goods[$key]['num'] = $item['num'];
                $goods[$key]['security_amount'] = $goodsDetail['security_amount'];
                $goods[$key]['give_integral'] = $goodsDetail['give_integral'];
                $goods[$key]['price'] = $skuDetail ? $skuDetail['price'] : $goodsDetail['price'];
                $goods[$key]['total_price'] = sprintf('%.2f', $goods[$key]['num'] * $goods[$key]['price']);
                $goods[$key]['total_amount'] = sprintf('%.2f', $goods[$key]['num'] * $goods[$key]['price'] + $goods[$key]['security_amount']);
                $goods[$key]['goods'] = json_encode($goodsDetail);
                $goods[$key]['sku'] = $skuDetail ? json_encode($skuDetail) : '';
                $goods[$key]['status'] = 1;

                $goods_amount += $goods[$key]['price'] * $item['num'];
                $security_amount += $goods[$key]['security_amount'];
                $give_integral += $goods[$key]['give_integral'];
            }

            $userInfo = User::where('id', '=', $params['user_id'])->findOrEmpty();
            //处理优惠券
            $coupon_amount = 0;
            if (!empty($params['coupon_id'])) {
                $coupon = CouponRecord::where('id', '=', $params['coupon_id'])->findOrEmpty();
                if ($coupon->isEmpty()) {
                    self::$error = '优惠券无效';
                    return false;
                }
                if ($goods_amount < $coupon['price']) {
                    self::$error = '优惠券不满足使用条件';
                    return false;
                }
                $coupon_amount = $coupon['price'];
            }

            //处理邮费
            $postage = 0;

            $order_amount = round($goods_amount + $security_amount + $postage - $coupon_amount, 2);

            //处理积分抵扣
            $integral_amount = 0;
            $use_integral = 0;
            if (isset($params['use_integral']) && $params['use_integral'] == 1) {
                $use_integral = $userInfo['user_integral'];
                $integral_price = (double)ConfigService::get('integral', 'price', 1);
                $integral_amount = round($use_integral * $integral_price, 2);
                // 如果超出订单金额，重新计算需要消耗的积分数量
                if ($integral_amount > $order_amount) {
                    $integral_amount = $order_amount;
                    $use_integral = (int)($integral_amount / $integral_price);
                }
            }

            $order_amount = $order_amount - $integral_amount;
            $order_amount = max(0, $order_amount);

            // 读取收货地址信息
            $addressInfo = UserAddress::where('id', '=', $params['address_id'])->findOrEmpty();

            $sn = generate_sn(Orders::class, 'sn');
            $result = Orders::create([
                'sn' => $sn,
                'user_id' => $params['user_id'],
                'goods_amount' => $goods_amount,
                'security_amount' => $security_amount,
                'coupon_id' => $params['coupon_id'] ?? 0,
                'coupon_amount' => $coupon_amount,
                'use_integral' => $use_integral,
                'integral_amount' => $integral_amount,
                'postage' => $postage,
                'order_amount' => $order_amount,
                'address_id' => $params['address_id'],
                'name' => $addressInfo['name'],
                'phone' => $addressInfo['phone'],
                'urgent_name' => $params['urgent_name'] ?? '',
                'urgent_phone' => $params['urgent_phone'] ?? '',
                'address' => $addressInfo['province'] . $addressInfo['city'] . $addressInfo['district'] . $addressInfo['address'],
                'give_integral' => $give_integral,
                'notes' => $params['notes'] ?? '',
                'status' => OrdersEnum::STATUS_WAIT,
                'first_leader' => User::where('id', '=', $params['user_id'])->value('first_leader')
            ]);

            //更新用户紧急联系方式
            User::update([
                'id' => $userInfo['id'],
                'urgent_name' => $params['urgent_name'],
                'urgent_phone' => $params['urgent_phone'],
            ]);

            //处理订单商品
            foreach ($goods as $key => $item) {
                $goods[$key]['order_id'] = $result->id;
                $goods[$key]['order_sn'] = $sn;
            }
            (new OrdersGoods())->saveAll($goods);

            //处理优惠券
            if (isset($coupon) && $coupon_amount > 0) {
                CouponRecord::update([
                    'id' => $coupon['id'],
                    'order_id' => $result->id,
                    'status' => CouponEnum::STATUS_USED
                ]);
            }

            //处理积分扣除
            if ($use_integral > 0) {
                User::update([
                    'id' => $userInfo['id'],
                    'user_integral' => $userInfo['user_integral'] - $use_integral
                ]);
                AccountLogLogic::add(
                    $userInfo['id'],
                    AccountLogEnum::UI_DEC_BUY_GOODS,
                    AccountLogEnum::DEC,
                    $use_integral,
                    $result->id,
                    '下单使用积分抵扣'
                );
            }

            Db::commit();
            return $result->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param $params
     * 订单确认页数据
     *  goods： [{"goods_id":16,"sku_id":5,"num":1},{"goods_id":13,"sku_id":0,"num":1}]
     */
    public static function confirmOrder($params): array
    {
        $goodsInfo = json_decode($params['goods'], true);
        $goods = [];
        $goods_amount = 0;  //商品总金额，普通商品的购买金额，租赁商品的租金
        $security_amount = 0;  //租赁商品的压金
        foreach ($goodsInfo as $key => $item) {
            $goodsDetail = Goods::where('id', '=', $item['goods_id'])->findOrEmpty();
            $skuDetail = '';
            if ($item['sku_id'] > 0) {
                $skuDetail = GoodsSku::where('id', '=', $item['sku_id'])->findOrEmpty();
            }
            $goods[$key]['lease_type'] = $goodsDetail['lease_type'];
            $goods[$key]['goods_id'] = $item['goods_id'];
            $goods[$key]['goods_name'] = $goodsDetail['name'];
            $goods[$key]['goods_type'] = $goodsDetail['type'];
            $goods[$key]['sku_id'] = $item['sku_id'];
            $goods[$key]['sku_name'] = $skuDetail ? $skuDetail['name'] : '';
            $goods[$key]['image'] = $skuDetail ? $skuDetail['image'] : $goodsDetail['image'];
            $goods[$key]['num'] = $item['num'];
            $goods[$key]['security_amount'] = $goodsDetail['security_amount'];
            $goods[$key]['price'] = $skuDetail ? $skuDetail['price'] : $goodsDetail['price'];
            $goods[$key]['total_price'] = $goods[$key]['num'] * $goods[$key]['price'] + $goods[$key]['security_amount'];

            $goods_amount += $goods[$key]['price'];
            $security_amount += $goods[$key]['security_amount'];
        }

        //优惠券信息
        $coupon_amount = 0;
        if (!empty($params['coupon_id'])) {
            $coupon = CouponRecord::where('id', '=', $params['coupon_id'])->findOrEmpty();
            if (!empty($coupon)) {
                $coupon_amount = $coupon['price'];
            }
        }

        $order_amount = round($goods_amount + $security_amount - $coupon_amount, 2);

        //处理积分抵扣
        $use_integral = User::where('id', '=', $params['user_id'])->value('user_integral');
        $integral_price = (double)ConfigService::get('integral', 'price', 1);
        $integral_amount = round($use_integral * $integral_price, 2);
        // 如果超出订单金额，重新计算需要消耗的积分数量
        if ($integral_amount > $order_amount) {
            $integral_amount = $order_amount;
            $use_integral = (int)($integral_amount / $integral_price);
        }

        $order_amount = $order_amount - $integral_amount;

        return [
            'goods' => $goods,
            'goods_amount' => $goods_amount,
            'security_amount' => $security_amount,
            'coupon' => $coupon ?? [],
            'coupon_price' => $coupon_amount,
            'use_integral' => $use_integral,
            'integral_amount' => $integral_amount,
            'preferential_amount' => $coupon_amount + $integral_amount,
            'order_amount' => $order_amount
        ];
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Orders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Orders::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Orders::field('*,status as status_text')
            ->with(['userInfo', 'goods', 'express'])
            ->findOrEmpty($id)
            ->toArray();
    }

    /**
     * @param $id
     * Date: 2023/12/14 22:24
     * Notes:获取订单商品详情
     */
    public static function orderGoodsDetail($id): array
    {
        return OrdersGoods::where('id', '=', $id)
            ->field('id,order_id,goods_id,goods_type,sku_id,goods_type,lease_type,goods_name,goods_image,sku_name,sku_image,price,num,security_amount,total_price,id as services_status,id as refund_status')
            ->with(['orderInfo'])
            ->findOrEmpty()
            ->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_DELIVER) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->confirm_time = time();
        $ordersInfo->save();

        //给用户增加积分
        if ($ordersInfo['give_integral'] > 0) {
            $userInfo = User::where('id', '=', $ordersInfo['user_id'])->findOrEmpty();
            $userInfo->user_integral += $ordersInfo['give_integral'];
            $userInfo->save();

            // 增加积分记录
            AccountLogLogic::add(
                $userInfo['id'],
                AccountLogEnum::UI_INC_BUY_GOODS,
                AccountLogEnum::INC,
                $ordersInfo['give_integral'],
                $ordersInfo['sn'],
                '购物赠送积分'
            );
        }

        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();

        // 已支付要退款
        if ($ordersInfo['pay_amount'] > 0) {
            if ($ordersInfo['pay_way'] == 2) {
                //费用退回微信
                $payService = (new WeChatPayService(1, $ordersInfo['user_id'] ?? null));
                $payService->refund([
                    'transaction_id' => $ordersInfo['transaction_id'],
                    'refund_sn' => $ordersInfo['transaction_id'],
                    'refund_amount' => $ordersInfo['refund_amount'],
                    'total_amount' => $ordersInfo['order_amount'],
                ]);
            } else {
                //费用退回余额
                $userInfo = User::where('id', '=', $ordersInfo['user_id'])->findOrEmpty();
                $userInfo->user_money += $ordersInfo['pay_amount'];
                $userInfo->save();
                AccountLogLogic::add(
                    $ordersInfo['user_id'],
                    AccountLogEnum::UM_INC_ORDERS_CANCEL,
                    AccountLogEnum::INC,
                    $ordersInfo['pay_amount'],
                    $ordersInfo['sn']
                );
            }
        }

        return true;
    }

}