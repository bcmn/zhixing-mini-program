<?php

namespace app\api\logic\orders;

use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\orders\OrdersCart;

/**
 * 服务订单
 * Class GoodsCateLogic
 */
class OrdersCartLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool|array
    {
        try {
            $result = OrdersCart::where('goods_id', '=', $params['goods_id'])
                ->where('user_id', '=', $params['user_id'])
                ->where('sku_id', '=', $params['sku_id'] ?? 0)
                ->findOrEmpty();
            if ($result->isEmpty()) {
                $price = Goods::where('id', '=', $params['goods_id'])->value('price');
                OrdersCart::create([
                    'user_id' => $params['user_id'],
                    'goods_id' => $params['goods_id'],
                    'price' => $price,
                    'sku_id' => $params['sku_id'] ?? 0,
                    'num' => $params['num'] ?? 1,
                    'is_check' => 1
                ]);
            } else {
                $result->num += $params['num'] ?? 1;
                $result->save();
            }

            return true;
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param $params
     * Date: 2023/10/17 23:55
     * Notes:减少购物车数量
     */
    public static function reduce($params): bool
    {
        $cartInfo = OrdersCart::findOrEmpty($params['id']);
        if ($cartInfo['num'] <= $params['num']) {
            OrdersCart::destroy($params['id']);
        } else {
            $cartInfo->num -= $params['num'];
            $cartInfo->save();
        }
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 删除购物车数据
     */
    public static function delete($params): bool
    {
        $id = explode(',', $params['id']);
        OrdersCart::destroy($id);
        return true;
    }

    /**
     * Date: 2023/10/27 22:45
     * Notes:清空购物车
     */
    public static function clear($user_id): bool
    {
        $ids = OrdersCart::where('user_id', '=', $user_id)->column('id');
        if (!empty($ids)) {
            OrdersCart::destroy($ids);
        }
        return true;
    }

}