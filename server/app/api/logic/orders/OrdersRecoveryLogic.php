<?php

namespace app\api\logic\orders;

use app\common\enum\orders\OrdersRecoveryEnum;
use app\common\enum\orders\OrdersRenewEnum;
use app\common\logic\BaseLogic;
use app\common\model\business\Hospital;
use app\common\model\business\Store;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\OrdersRecovery;
use app\common\model\orders\OrdersRenew;

/**
 * 设备预约订单(用户)
 */
class OrdersRecoveryLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            $devices = GoodsDevices::where('order_goods_id', '=', $params['order_goods_id'])
                ->where('is_active', '=', 1)
                ->findOrEmpty();
            if ($devices->isEmpty()) {
                self::$error = '查询不到设备信息';
                return false;
            }

            $goodsInfo = Goods::where('id', '=', $devices['goods_id'])->findOrEmpty();
            if ($goodsInfo->isEmpty()) {
                self::$error = '商品信息出错';
                return false;
            }

            $insert = OrdersRecovery::create([
                'sn' => generate_sn(OrdersRecovery::class, 'sn'),
                'devices_id' => $devices['id'],
                'goods_id' => $devices['goods_id'],
                'order_id' => $devices['order_id'],
                'order_sn' => $devices['order_sn'],
                'order_goods_id' => $devices['order_goods_id'],
                'refund_amount' => $devices['refund_amount'],
                'user_id' => $params['user_id'],
                'lease_type' => $goodsInfo['lease_type'],
                'goods_amount' => $params['order_amount'],
                'security_amount' => $params['security_amount'],
                'notes' => $params['notes'] ?? '',
                'images' => $params['images'] ?? '',
                'status' => OrdersRenewEnum::STATUS_WAIT,
                'store_user_id' => $params['user_id'],
                'store_id' => Store::where('user_id', '=', $devices['user_id'])->value('id') ?? 0,
            ]);
            return $insert->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        OrdersRecovery::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        $detail = OrdersRecovery::field('*,status as status_text')
            ->with(['userInfo', 'devices', 'goodsInfo'])
            ->findOrEmpty($id);
        $detail['store'] = Store::where('id', '=', $detail['store_id'])
            ->field('*,user_id as devices_num')
            ->findOrEmpty();
        return $detail->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = OrdersRecovery::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersRecoveryEnum::STATUS_SUCCESS;
        $ordersInfo->confirm_time = time();
        $ordersInfo->save();

        //处理设备转移给门店，压金转移等逻辑

        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = OrdersRecovery::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersRecoveryEnum::STATUS_FAIL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }

}