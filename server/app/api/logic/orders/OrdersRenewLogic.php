<?php

namespace app\api\logic\orders;

use app\common\enum\orders\OrdersEnum;
use app\common\enum\orders\OrdersRenewEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\goods\GoodsSku;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersRenew;
use app\common\model\user\User;

/**
 * 购买次数或时长订单
 */
class OrdersRenewLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function createOrder(array $params): bool|array
    {
        try {
            $devices = GoodsDevices::where('sn', '=', $params['sn'])
                ->where('is_active', '=', 1)
                ->findOrEmpty();
            if ($devices->isEmpty()) {
                self::$error = '查询不到设备信息';
                return false;
            }

            $goodsInfo = Goods::where('id', '=', $devices['goods_id'])->findOrEmpty();
            if ($goodsInfo->isEmpty()) {
                self::$error = '商品信息出错';
                return false;
            }

            $goods_amount = $devices['learn_price'];
            $goods_num = $params['num'] ?? 1;
            $insert = OrdersRenew::create([
                'sn' => generate_sn(OrdersRenew::class, 'sn'),
                'devices_id' => $devices['id'],
                'devices_sn' => $devices['sn'],
                'user_id' => $params['user_id'],
                'lease_type' => $goodsInfo['lease_type'],
                'goods_amount' => $goods_amount,
                'function_type' => $params['function_type'],
                'num' => $goods_num,
                'order_amount' => $goods_amount * $goods_num,
                'status' => OrdersRenewEnum::STATUS_WAIT
            ]);
            return $insert->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

}