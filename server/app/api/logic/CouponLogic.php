<?php

namespace app\api\logic;

use app\common\enum\CouponEnum;
use app\common\logic\BaseLogic;
use app\common\model\coupon\Coupon;
use app\common\model\coupon\CouponRecord;
use app\common\model\user\User;


/**
 * 优惠券逻辑
 */
class CouponLogic extends BaseLogic
{

    /**
     * Date: 2023/9/7 20:39
     * Notes：获取全部数据
     */
    public static function getAllData($params): array
    {
        $status = $params['status'] ?? 0;
        $where[] = ['user_id', '=', $params['user_id']];
        if ($status) {
            $where[] = ['status', '=', $status];
        }
        return CouponRecord::where($where)
            ->order(['id' => 'desc'])
            ->select()
            ->toArray();
    }

    /**
     * Date: 2023/10/27 21:57
     * Notes:发放优惠券
     */
    public static function grant($params): bool|string
    {
        $coupon = Coupon::where('is_default', '=', 1)->findOrEmpty();
        if ($coupon->isEmpty()) {
            self::$error = '查询不到优惠券信息';
            return false;
        }
        $grantNum = CouponRecord::where('coupon_id', '=', $coupon['id'])->count();
        $leaveNum = $coupon['num'] - $grantNum;
        if ($leaveNum < 1) {
            self::$error = '优惠券数量不足';
            return false;
        }

        $userInfo = User::where('id', '=', $params['user_id'])->findOrEmpty();
        if ($userInfo->isEmpty()) {
            self::$error = '查询不到用户信息';
            return false;
        }

        $saveData = [
            'user_id' => $userInfo['id'],
            'coupon_id' => $coupon['id'],
            'sn' => generate_sn(CouponRecord::class, 'sn'),
            'title' => $coupon['title'],
            'price' => $coupon['price'],
            'full' => $coupon['full'],
            'date' => $coupon['date'],
            'order_id' => 0,
            'status' => CouponEnum::STATUS_WAIT,
        ];

        try {
            (new CouponRecord())->save($saveData);
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
        return true;
    }

}