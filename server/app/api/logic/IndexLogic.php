<?php

namespace app\api\logic;

use app\common\enum\DefaultEnum;
use app\common\enum\goods\GoodsEnum;
use app\common\enum\UserEnum;
use app\common\logic\BaseLogic;
use app\common\model\advertise\Advertise;
use app\common\model\caregiver\Caregiver;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsCate;
use app\common\service\ConfigService;
use app\common\service\FileService;


/**
 * index
 * Class IndexLogic
 * @package app\api\logic
 */
class IndexLogic extends BaseLogic
{

    /**
     * @notes 首页数据
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 段誉
     * @date 2022/9/21 19:15
     */
    public static function getIndexData()
    {
        //轮播图
        $slides = Advertise::where('cate_id', '=', 3)
            ->where('status', '=', DefaultEnum::SHOW)
            ->order('sort desc,id desc')
            ->field('id,title,image,url,url_type')
            ->select();

        //轮播图下广告图
        $adImage = Advertise::where('cate_id', '=', 6)
            ->where('status', '=', DefaultEnum::SHOW)
            ->field('id,title,image,url,url_type')
            ->findOrEmpty();

        //普通商品
        $goods = Goods::where('type', '=', GoodsEnum::TYPE_NORMAL)
            ->where('status', '=', DefaultEnum::SHOW)
            ->order('sort desc,id desc')
            ->field('id,image,name,price,sales')
            ->limit(4)
            ->select();

        //租赁商品
        $goods_rent = Goods::where('type', '=', GoodsEnum::TYPE_RENT)
            ->where('status', '=', DefaultEnum::SHOW)
            ->order('sort desc,id desc')
            ->field('id,image,name,price,sales')
            ->limit(4)
            ->select();

        return [
            'slides' => $slides,
            'adImage' => $adImage,
            'goods' => $goods,
            'goods_rent' => $goods_rent,
        ];
    }

    /**
     * Date: 2023/11/2 22:04
     * Notes:商场接口
     */
    public static function getMallData()
    {
        //轮播图
        $slides = Advertise::where('cate_id', '=', 7)
            ->where('status', '=', DefaultEnum::SHOW)
            ->order('sort desc,id desc')
            ->field('id,title,image,url,url_type')
            ->select();

        //商品分类
        $cateLists = GoodsCate::where('status', '=', DefaultEnum::SHOW)
            ->order('sort desc,id desc')
            ->select();

        return [
            'slides' => $slides,
            'cateLists' => $cateLists,
        ];
    }


    /**
     * @notes 获取政策协议
     * @param string $type
     * @return array
     * @author 段誉
     * @date 2022/9/20 20:00
     */
    public static function getPolicyByType(string $type)
    {
        return [
            'title' => ConfigService::get('agreement', $type . '_title', ''),
            'content' => ConfigService::get('agreement', $type . '_content', ''),
        ];
    }

    /**
     * @notes 获取配置
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 段誉
     * @date 2022/9/21 19:38
     */
    public static function getConfigData()
    {
        // 登录配置
        $loginConfig = [
            // 登录方式
            'login_way' => ConfigService::get('login', 'login_way', config('project.login.login_way')),
            // 注册强制绑定手机
            'coerce_mobile' => ConfigService::get('login', 'coerce_mobile', config('project.login.coerce_mobile')),
            // 政策协议
            'login_agreement' => ConfigService::get('login', 'login_agreement', config('project.login.login_agreement')),
            // 第三方登录 开关
            'third_auth' => ConfigService::get('login', 'third_auth', config('project.login.third_auth')),
            // 微信授权登录
            'wechat_auth' => ConfigService::get('login', 'wechat_auth', config('project.login.wechat_auth')),
            // qq授权登录
            'qq_auth' => ConfigService::get('login', 'qq_auth', config('project.login.qq_auth')),
        ];
        // 网址信息
        $website = [
            'web_favicon' => ConfigService::get('website', 'web_favicon'),
            'web_logo' => FileService::getFileUrl(ConfigService::get('website', 'web_logo')),
            'join_image' => FileService::getFileUrl(ConfigService::get('website', 'join_image')),
        ];
        // H5配置
        $webPage = [
            // 渠道状态 0-关闭 1-开启
            'status' => ConfigService::get('web_page', 'status', 1),
            // 关闭后渠道后访问页面 0-空页面 1-自定义链接
            'page_status' => ConfigService::get('web_page', 'page_status', 0),
            // 自定义链接
            'page_url' => ConfigService::get('web_page', 'page_url', ''),
            'url' => request()->domain() . '/mobile'
        ];

        return [
            'domain' => FileService::getFileUrl(),
            'login' => $loginConfig,
            'website' => $website,
            'webPage' => $webPage,
            'version' => config('project.version')
        ];
    }

}