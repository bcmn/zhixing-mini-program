<?php

namespace app\api\logic\business;

use app\common\enum\DefaultEnum;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;

/**
 * 服务项目逻辑
 * Class GoodsCateLogic
 */
class BusinessLogic extends BaseLogic
{
    /**
     * @param $params
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        return Goods::findOrEmpty($params['id'])->toArray();
    }

}