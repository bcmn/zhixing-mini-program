<?php

namespace app\api\logic\business;

use app\common\logic\BaseLogic;
use app\common\model\business\Hospital;


/**
 * 合作医院逻辑
 */
class HospitalLogic extends BaseLogic
{

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        $detail = Hospital::field('*')
            ->where('id', '=', $params['id'])
            ->findOrEmpty()
            ->toArray();
        $detail['distance'] = 0;
        if (!empty($params['lng']) && !empty($params['lat'])) {
            $detail['distance'] = getDistance((float)$detail['lat'], (float)$detail['lng'], (float)$params['lat'], (float)$params['lng']);
        }
        return $detail;
    }

}