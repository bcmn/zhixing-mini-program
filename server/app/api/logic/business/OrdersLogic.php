<?php

namespace app\api\logic\business;

use app\common\enum\OrdersEnum;
use app\common\enum\PayEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\logic\AccountLogLogic;
use app\common\logic\BaseLogic;
use app\common\model\goods\Goods;
use app\common\model\goods\Orders;
use app\common\model\customer\Customer;
use app\common\model\user\User;
use app\common\service\pay\WeChatPayService;
use think\facade\Db;

/**
 * 服务订单
 * Class GoodsCateLogic
 */
class OrdersLogic extends BaseLogic
{
    /**
     * @param array $params
     * Date: 2023/9/6 21:17
     * Notes：新增
     */
    public static function add(array $params): bool|array
    {
        try {
            $business = Goods::where('id', '=', $params['business_id'])->findOrEmpty();
            if ($business->isEmpty()) {
                self::$error = '查询不到预约项目信息';
                return false;
            }
            $sn = generate_sn(Orders::class, 'sn');
            $result = Orders::create([
                'sn' => $sn,
                'user_id' => $params['user_id'],
                'caregiver_id' => $params['caregiver_id'] ?? 0,
                'hospital_id' => $params['hospital_id'] ?? 0,
                'business_id' => $params['business_id'],
                'appoint_time' => $params['appoint_time'],
                'customer_id' => $params['customer_id'],
                'customer_name' => Customer::where('id', '=', $params['customer_id'])->value('name'),
                'customer_address' => $params['customer_address'] ?? '',
                'customer_mobile' => $params['customer_mobile'] ?? '',
                'disease_history' => $params['disease_history'] ?? '',
                'genetic_history' => $params['genetic_history'] ?? '',
                'surgical_history' => $params['surgical_history'] ?? '',
                'order_amount' => $business['amount'],
                'status' => OrdersEnum::STATUS_WAIT
            ]);

            return $result->toArray();
        } catch (\Exception $e) {
            self::setError($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $params
     * Date: 2023/9/7 20:55
     * Notes：更新字段
     */
    public static function change(array $params): void
    {
        Orders::update([
            'id' => $params['id'],
            $params['field'] => $params['val']
        ]);
    }

    /**
     * @param array $params
     * Date: 2023/9/6 21:18
     * Notes：删除
     */
    public static function delete(array $params): void
    {
        Orders::destroy($params['id']);
    }


    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($id): array
    {
        return Orders::field('*,hospital_id as hospital_text,business_id as business_text,customer_id as customer_sex,customer_id as customer_age,business_id as business_image')->findOrEmpty($id)->toArray();
    }

    /**
     * @param $params
     * @return bool
     * 分配订单
     */
    public static function allocation($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->caregiver_id = $params['caregiver_id'];
        $ordersInfo->allocation_time = time();
        $ordersInfo->save();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 完成订单
     */
    public static function finish($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
            self::$error = '订单状态不正确';
            return false;
        }

        Db::startTrans();
        $ordersInfo->status = OrdersEnum::STATUS_FINISH;
        $ordersInfo->finish_time = time();
        $ordersInfo->save();

        //给陪护师增加余额
        if (!empty($ordersInfo['caregiver_id'])) {
            $caregiver = User::findOrEmpty($ordersInfo['caregiver_id']);
            $caregiver->user_money += $ordersInfo['pay_amount'];
            $caregiver->save();

            AccountLogLogic::add($ordersInfo['caregiver_id'], AccountLogEnum::UM_INC_ORDERS, AccountLogEnum::INC, $ordersInfo['pay_amount'], $ordersInfo['sn'], '订单收入');
        }
        Db::commit();
        return true;
    }

    /**
     * @param $params
     * @return bool
     * 取消订单
     */
    public static function cancel($params): bool
    {
        $ordersInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
        if ($ordersInfo->isEmpty()) {
            self::$error = '未查询到订单信息';
            return false;
        }

        $ordersInfo->status = OrdersEnum::STATUS_CANCEL;
        $ordersInfo->cancel_time = time();
        $ordersInfo->save();
        return true;
    }
}