<?php

namespace app\api\logic\business;

use app\common\logic\BaseLogic;
use app\common\model\business\Store;
use app\common\model\coupon\CouponRecord;


/**
 * 门店逻辑
 */
class StoreLogic extends BaseLogic
{

    /**
     * @return array
     * Date: 2023/9/6 21:18
     * Notes：详情
     */
    public static function detail($params): array
    {
        if (!empty($params['id'])) {
            $id = $params['id'];
        } else {
            $id = Store::where('user_id', '=', $params['user_id'])->value('id');
        }
        $detail = Store::field('*')
            ->where('id', '=', $id)
            ->field('*,user_id as devices_num')
            ->findOrEmpty()
            ->toArray();
        $detail['distance'] = 0;
        if (!empty($params['lng']) && !empty($params['lat'])) {
            $detail['distance'] = getDistance((float)$detail['lat'], (float)$detail['lng'], (float)$params['lat'], (float)$params['lng']);
        }
        return $detail;
    }

    /**
     * @param $params
     * @return bool
     * Date: 2023/12/3 19:24
     * Notes: 修改门店资料
     */
    public static function edit($params): bool
    {
        if (empty($params['id'])) {
            self::$error = '缺少必要的更新条件';
            return false;
        }
        Store::update($params);
        return true;
    }

}