<?php

namespace app\api\validate\user;

use app\common\validate\BaseValidate;

/**
 * 用户收货地址验证
 */
class UserAddressValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'name' => 'require',
        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
        'province' => 'require',
        'city' => 'require',
        'district' => 'require',
        'address' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'name.require' => '未填写姓名',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
        'province.require' => '未选择省份',
        'city.require' => '未选择城市',
        'district.require' => '未填写地区',
        'address.require' => '未填写具体地址信息',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): UserAddressValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): UserAddressValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): UserAddressValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): UserAddressValidate
    {
        return $this->only(['id']);
    }

}