<?php

namespace app\api\validate\user;

use app\common\validate\BaseValidate;

/**
 * 设备维修验证
 */
class UserRepairValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'sn' => 'require',
        'content' => 'require',
//        'name' => 'require',
//        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
//        'address' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'sn.require' => '缺少设备编号',
        'content.require' => '缺少故障描述',
        'name.require' => '未填写姓名',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
        'address.require' => '未填写地址信息',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): UserRepairValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): UserRepairValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): UserRepairValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): UserRepairValidate
    {
        return $this->only(['id']);
    }

}