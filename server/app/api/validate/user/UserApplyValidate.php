<?php

namespace app\api\validate\user;

use app\common\enum\user\UserApplyEnum;
use app\common\model\user\UserApply;
use app\common\validate\BaseValidate;

/**
 * 用户加盟申请验证
 */
class UserApplyValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'type' => 'require',
        'user_id' => 'require|checkRepeat',
        'name' => 'require',
        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
        'address' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'type.require' => '未选择加盟类型',
        'name.require' => '未填写姓名',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
        'address.require' => '未填写地址信息',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): UserApplyValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): UserApplyValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): UserApplyValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): UserApplyValidate
    {
        return $this->only(['id']);
    }

    public function checkRepeat($value): bool|string
    {
        $result = UserApply::where('user_id', '=', $value)->findOrEmpty();
        if (!$result->isEmpty()) {
            if ($result['status'] == UserApplyEnum::STATUS_PASS) {
                return '您已经加盟过啦';
            }
            if ($result['status'] == UserApplyEnum::STATUS_WAIT) {
                return '你的加盟正在申请中，请耐心等待';
            }
        }
        return true;
    }
}