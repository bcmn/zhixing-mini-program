<?php

namespace app\api\validate\user;

use app\common\validate\BaseValidate;

/**
 * 用户收货地址验证
 */
class UserBankValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'name' => 'require',
//        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
        'bank_name' => 'require',
        'bank_card' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'name.require' => '未填写姓名',
//        'mobile.require' => '未填写手机号码',
//        'mobile.length' => '手机号码格式不正确',
//        'mobile.regex' => '手机号码格式不正确',
        'bank_name.require' => '未填写银行名称',
        'bank_card.require' => '未填写银行卡号',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): UserBankValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): UserBankValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): UserBankValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): UserBankValidate
    {
        return $this->only(['id']);
    }

}