<?php

namespace app\api\validate\user;

use app\common\validate\BaseValidate;

/**
 * 用户收藏验证
 */
class UserFavoriteValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'goods_id' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'goods_id.require' => '缺少商品信息',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): UserFavoriteValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): UserFavoriteValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): UserFavoriteValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): UserFavoriteValidate
    {
        return $this->only(['id']);
    }

}