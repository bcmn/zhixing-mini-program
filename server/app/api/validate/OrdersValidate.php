<?php

namespace app\api\validate;

use app\common\validate\BaseValidate;

/**
 * 服务订单
 */
class OrdersValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'hospital_id' => 'require',
        'appoint_time' => 'require',
        'customer_id' => 'require',
        'customer_address' => 'require',
        'customer_mobile' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'hospital_id.require' => '未选择就诊医院',
        'appoint_time.require' => '未填写就诊时间',
        'customer_id.require' => '未选择服务对象',
        'customer_address.require' => '未填写接送地址',
        'customer_mobile.require' => '未填写手机号码',
        'customer_mobile.length' => '手机号码格式不正确',
        'customer_mobile.regex' => '手机号码格式不正确',
        'caregiver_id.require' => '未选择陪护师',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): OrdersValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @return OrdersValidate
     * 分配订单场景
     */
    public function sceneAllocation(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('caregiver_id', 'require');
    }

    /**
     * @return OrdersValidate
     * 完成订单
     */
    public function sceneFinish(): OrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @return OrdersValidate
     * 取消订单
     */
    public function sceneCancel(): OrdersValidate
    {
        return $this->only(['id']);
    }
}