<?php

namespace app\api\validate;

use app\common\model\customer\Customer;
use app\common\validate\BaseValidate;

/**
 * 服务对象验证
 */
class CustomerValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require|checkId',
        'user_id' => 'require',
        'name' => 'require',
        'sex' => 'require',
        'age' => 'require',
        'mobile' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'name.require' => '未填写姓名',
        'sex.require' => '未选择性别',
        'age.require' => '未填写年龄',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): CustomerValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): CustomerValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): CustomerValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): CustomerValidate
    {
        return $this->only(['id']);
    }

    public function checkId($value, $rule, $data): bool|string
    {
        $res = Customer::where('id', '=', $value)->findOrEmpty();
        if($res->isEmpty()){
            return '信息不存在';
        }
        return true;
    }

}