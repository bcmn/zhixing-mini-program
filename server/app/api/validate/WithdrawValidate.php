<?php

namespace app\api\validate;

use app\common\enum\UserEnum;
use app\common\model\caregiver\Caregiver;
use app\common\model\user\User;
use app\common\service\ConfigService;
use app\common\validate\BaseValidate;

/**
 * 申请成为陪护师验证
 */
class WithdrawValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'bank_card' => 'require',
        'name' => 'require',
        'mobile' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
        'amount' => 'require|checkBalance',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'bank_card.require' => '未填写账号',
        'name.require' => '未填写真实姓名',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
        'amount.require' => '未填写提现金额',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): WithdrawValidate
    {
        return $this->remove('id', true);
    }

    public function checkBalance($value, $rule, $data)
    {
        if ($value <= 0) {
            return '提现金额必须大于0';
        }
        $withdraw_min = ConfigService::get('withdraw', 'withdraw_min');
        if ($value < $withdraw_min) {
            return '提现金额必须大于' . $withdraw_min;
        }

        $user_money = User::where('id', '=', $data['user_id'])->value('user_money');
        if ($user_money < $value) {
            return "用户余额不足";
        }
        return true;
    }
}