<?php

namespace app\api\validate;

use app\common\validate\BaseValidate;

/**
 * 退款验证
 */
class RefundValidate extends BaseValidate
{
    protected $rule = [
        'order_id' => 'require',
        'refund_type' => 'require',
    ];

    protected $message = [
        'order_id.require' => '参数缺失',
        'refund_type.require' => '用户未登录',
    ];
}