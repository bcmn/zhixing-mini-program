<?php

namespace app\api\validate;

use app\common\validate\BaseValidate;

/**
 * 服务订单
 */
class PackageOrdersValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'package_id' => 'require',
        'customer_id' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'package_id.require' => '未选择医旅套餐',
        'customer_id.require' => '未选择服务对象',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): PackageOrdersValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): PackageOrdersValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): PackageOrdersValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): PackageOrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @return OrdersValidate
     * 完成订单
     */
    public function sceneFinish(): PackageOrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @return OrdersValidate
     * 取消订单
     */
    public function sceneCancel(): PackageOrdersValidate
    {
        return $this->only(['id']);
    }
}