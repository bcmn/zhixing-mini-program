<?php

namespace app\api\validate;

use app\common\enum\CaregiverEnum;
use app\common\model\caregiver\Caregiver;
use app\common\validate\BaseValidate;

/**
 * 申请成为陪护师验证
 */
class CaregiverValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require|checkRepeat',
        'image' => 'require',
        'name' => 'require',
        'sex' => 'require',
        'age' => 'require',
        'mobile' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
        'idcard_name' => 'require',
        'idcard' => 'require',
        'idcard_front' => 'require',
        'idcard_back' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'image.require' => '未上传形象照片',
        'name.require' => '未填写姓名',
        'sex.require' => '未选择性别',
        'age.require' => '未填写年龄',
        'mobile.require' => '未填写手机号码',
        'mobile.length' => '手机号码格式不正确',
        'mobile.regex' => '手机号码格式不正确',
        'idcard_name.require' => '未填写身份证姓名',
        'idcard.require' => '未填写身份证号',
        'idcard_front.require' => '未上传身份证正面照片',
        'idcard_back.require' => '未上传身份证反面照片',
    ];


    /**
     * @notes 添加场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): CaregiverValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @author 段誉
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): CaregiverValidate
    {
        return $this->only(['id']);
    }


    public function sceneEdit()
    {
    }

    public function sceneChange(): CaregiverValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @author 段誉
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): CaregiverValidate
    {
        return $this->only(['id']);
    }

    public function checkRepeat($value, $rule, $date)
    {
        $result = Caregiver::where('user_id', '=', $value)
            ->where('status', '<>', CaregiverEnum::STATUS_REFUSE)
            ->findOrEmpty();
        if (!$result->isEmpty()) {
            if ($result['status'] == 2) {
                return '您已经是陪诊员啦';
            } else {
                return '您的申请正在审核中，请等待审核完成';
            }
        }
        return true;
    }
}