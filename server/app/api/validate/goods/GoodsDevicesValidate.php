<?php

namespace app\api\validate\goods;

use app\common\validate\BaseValidate;

/**
 * 设备验证
 */
class GoodsDevicesValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'sn' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'sn.require' => '缺少设备编码',
        'mobile.require' => '缺少接收用户手机号码',
        'imsi.require' => '缺少物联卡号',
        'goods_id.require' => '缺少商品ID',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): GoodsDevicesValidate
    {
        return $this->remove('id', true)
            ->append('imsi', 'require')
            ->append('goods_id', 'require');
    }

    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneActive(): GoodsDevicesValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): GoodsDevicesValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit(): GoodsDevicesValidate
    {
        return $this->remove('mobile', true);
    }

    public function sceneChange(): GoodsDevicesValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): GoodsDevicesValidate
    {
        return $this->only(['id']);
    }

    /**
     * @notes 转让场景
     * @date 2022/5/26 9:53
     */
    public function sceneTransfer(): GoodsDevicesValidate
    {
        return $this->remove('id', true);
    }
}