<?php

namespace app\api\validate\orders;

use app\common\validate\BaseValidate;

/**
 * 用户收货地址验证
 */
class OrdersValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'goods' => 'require',
        'address_id' => 'require',
//        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'goods.require' => '缺少商品信息',
        'address_id.require' => '缺少收货信息',
    ];

    public function sceneFinish(): OrdersValidate
    {
        return $this->only(['id']);
    }


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): OrdersValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersValidate
    {
        return $this->only(['id']);
    }

    /**
     * @notes 取消订单场景
     * @date 2022/5/26 9:53
     */
    public function sceneCancel(): OrdersValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): OrdersValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersValidate
    {
        return $this->only(['id']);
    }

}