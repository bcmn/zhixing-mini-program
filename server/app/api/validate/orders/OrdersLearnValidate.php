<?php

namespace app\api\validate\orders;

use app\common\validate\BaseValidate;

/**
 * 用户收货地址验证
 */
class OrdersLearnValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'user_id' => 'require',
        'sn' => 'require',
        'num' => 'require',
//        'phone' => 'require|length:11|regex:/^1[3456789]\d{9}$/',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'sn.require' => '缺少设备编号',
        'num.require' => '缺少购买数量',
    ];

    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): OrdersLearnValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersLearnValidate
    {
        return $this->only(['id']);
    }


    public function sceneChange(): OrdersLearnValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersLearnValidate
    {
        return $this->only(['id']);
    }

}