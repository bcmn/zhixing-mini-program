<?php

namespace app\api\validate\orders;

use app\common\validate\BaseValidate;

/**
 * 用户收货地址验证
 */
class OrdersCartValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'require',
        'goods_id' => 'require',
    ];

    protected $message = [
        'id.require' => '参数缺失',
        'user_id.require' => '用户未登录',
        'goods_id.require' => '缺少商品ID',
    ];


    /**
     * @notes 添加场景
     * @date 2022/5/26 9:53
     */
    public function sceneAdd(): OrdersCartValidate
    {
        return $this->remove('id', true);
    }

    /**
     * @notes 详情场景
     * @date 2022/5/26 9:53
     */
    public function sceneDetail(): OrdersCartValidate
    {
        return $this->only(['id']);
    }


    /**
     * Date: 2023/10/17 17:16
     * Notes:编辑场景
     */
    public function sceneEdit()
    {
    }

    public function sceneChange(): OrdersCartValidate
    {
        return $this->only(['id'])
            ->append('field', 'require')
            ->append('val', 'require');
    }

    /**
     * @notes 删除场景
     * @date 2022/5/26 9:54
     */
    public function sceneDelete(): OrdersCartValidate
    {
        return $this->only(['id']);
    }

}