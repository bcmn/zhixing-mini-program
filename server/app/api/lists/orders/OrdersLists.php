<?php

namespace app\api\lists\orders;

use app\api\lists\BaseApiDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\orders\Orders;

/**
 * 订单列表
 */
class OrdersLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['sn', 'customer_name', 'urgent_name', 'urgent_phone']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['user_id', '=', $this->userId]
        ];

        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }

        if (isset($this->params['status']) && $this->params['status'] > 0) {
            $where[] = ['status', '=', $this->params['status']];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,sn,user_id,goods_amount,security_amount,coupon_amount,integral_amount,postage,order_amount,name,phone,address,pay_way,pay_amount,notes,express_time,express_company,express_sn,status,status as status_text')
            ->limit($this->limitOffset, $this->limitLength)
            ->with(['goods', 'userInfo', 'express'])
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}