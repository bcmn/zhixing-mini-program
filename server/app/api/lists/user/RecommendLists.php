<?php

namespace app\api\lists\user;

use app\api\lists\BaseApiDataLists;
use app\common\enum\user\AccountLogEnum;
use app\common\lists\ListsExtendInterface;
use app\common\lists\ListsSearchInterface;
use app\common\model\orders\Orders;
use app\common\model\user\User;
use app\common\model\user\UserAccountLog;

/**
 * 获取下级员工列表
 */
class RecommendLists extends BaseApiDataLists implements ListsSearchInterface, ListsExtendInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['name', 'mobile']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['first_leader', '=', $this->userId]
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        $lists = User::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,nickname,avatar,account,mobile,create_time')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('id desc')
            ->select();

        foreach ($lists as $key => $item) {
            $lists[$key]['avatar'] = getAvatar($item['avatar']);
            $lists[$key]['orders_num'] = Orders::where('user_id', '=', $item['id'])->count();
            $lists[$key]['commission_amount'] = UserAccountLog::where('source_user_id', '=', $item['id'])->sum('change_amount');
        }

        return $lists->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return User::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

    public function extend()
    {
        return [
            'total' => UserAccountLog::where('user_id', $this->userId)->where('change_type', 'in', AccountLogEnum::getCommissionIncomeType())->sum('change_amount'),
            'total_today' => UserAccountLog::where('user_id', $this->userId)->where('change_type', 'in', AccountLogEnum::getCommissionIncomeType())->whereTime('create_time', 'today')->sum('change_amount'),
        ];
    }


}