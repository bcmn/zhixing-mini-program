<?php

namespace app\api\lists\user;

use app\api\lists\BaseApiDataLists;
use app\common\enum\CaregiverEnum;
use app\common\lists\ListsSearchInterface;
use app\common\model\caregiver\Caregiver;
use app\common\model\user\User;

/**
 * 服务对象列表
 * Class GoodsCateLists
 */
class CaregiverLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '=' => ['sex'],
            '%like%' => ['name', 'mobile']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere($flag = true)
    {
        $where = [
            ['status', '=', CaregiverEnum::STATUS_PASS]
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Caregiver::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,image,name,sex,mobile,score,orders_num,complaint_num,notes')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('score desc,orders_num desc,complaint_num asc,id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Caregiver::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}