<?php

namespace app\api\lists\user;

use app\api\lists\BaseApiDataLists;
use app\common\enum\UserEnum;
use app\common\lists\ListsSearchInterface;
use app\common\model\caregiver\Caregiver;
use app\common\model\user\User;
use app\common\model\user\Withdraw;

/**
 * 服务对象列表
 * Class GoodsCateLists
 */
class WithdrawLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['name', 'mobile']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere($flag = true)
    {
        $where = [
            ['user_id', '=', $this->userId]
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Withdraw::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,bank_card,name,mobile,amount,status,create_time,check_time,reason')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Withdraw::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}