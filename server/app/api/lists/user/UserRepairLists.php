<?php

namespace app\api\lists\user;

use app\api\lists\BaseApiDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\user\UserRepair;

/**
 * 申请维修列表
 */
class UserRepairLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['name', 'phone']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['user_id', '=', $this->userId]
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return UserRepair::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('*,status as status_text')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return UserRepair::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}