<?php

namespace app\api\lists\goods;

use app\api\lists\BaseApiDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\goods\Orders;

/**
 * 订单列表
 */
class OrdersLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['sn', 'customer_name']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $type = $this->params['type'] ?? 0;
        if ($type == 1) {
            $where = [
                ['caregiver_id', '=', $this->userId]
            ];
        } else {
            $where = [
                ['user_id', '=', $this->userId]
            ];
        }
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }

        if (isset($this->params['status']) && $this->params['status'] > 0) {
            $where[] = ['status', '=', $this->params['status']];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,appoint_time,customer_name,customer_mobile,order_amount,status,status as status_text,business_id as business_text,hospital_id as hospital_text')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Orders::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}