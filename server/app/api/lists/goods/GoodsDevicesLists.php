<?php

namespace app\api\lists\goods;

use app\api\lists\BaseApiDataLists;
use app\api\logic\goods\GoodsLogic;
use app\api\logic\orders\RefundLogic;
use app\common\enum\RefundEnum;
use app\common\lists\ListsSearchInterface;
use app\common\model\business\Store;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\OrdersLearn;
use app\common\model\refund\RefundRecord;
use app\common\model\user\UserRepair;

/**
 * 我的设备列表
 */
class GoodsDevicesLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['top_id', 'cate_id', 'name']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [];

        // 用户ID，传user_id则获取用户的设备列表(用于获取医院和门店的设备，医院和门店都有绑定的用户ID)，不传则获取当前登录用户的设备列表
        if (!empty($this->params['user_id'])) {
            $where[] = ['is_active', '=', 1];
            $where[] = ['user_id', '=', $this->params['user_id']];

//            $devices_sn = RefundRecord::where('user_id', $this->params['user_id'])->column('devices_sn');
//            if (!empty($devices_sn)) {
//                $where[] = ['sn', 'not in', $devices_sn];
//            }

        } else {
            $where[] = ['user_id', '=', $this->userId];
        }

        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        $lists = GoodsDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,sn,user_id,order_sn,goods_id,order_goods_id,security_amount,goods_name,sku_id,sku_name,learn_price,is_active,end_time,status')
            ->with(['userInfo', 'goodsInfo', 'ordersInfo', 'orderGoodsInfo'])
            ->limit($this->limitOffset, $this->limitLength)
            ->order('sort desc,id desc')
            ->select()
            ->toArray();
        foreach ($lists as $key => $item) {
            $lists[$key]['sales_num'] = OrdersLearn::where('devices_id', '=', $item['id'])->count();
            $lists[$key]['is_services'] = RefundRecord::where('devices_sn', '=', $item['sn'])->count() ? 1 : 0;
            $refund_status = RefundRecord::where('devices_sn', '=', $item['sn'])->order('id desc')->value('refund_status');
            if (is_numeric($refund_status)) {
                $lists[$key]['refund_status'] = $refund_status;
                $lists[$key]['refund_status_text'] = RefundEnum::getStatusDesc($refund_status);
            }
            $lists[$key]['is_repair'] = UserRepair::where('sn', '=', $item['sn'])->where('status', 1)->count();

            $latestPrice = GoodsLogic::getLatestPrice($item['goods_id'], $item['sku_id']);
            $lists[$key]['price'] = $latestPrice['price'];
            $lists[$key]['learn_price'] = $latestPrice['learn_price'];
            $lists[$key]['security_amount'] = $latestPrice['security_amount'];
        }
        return $lists;
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return GoodsDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}