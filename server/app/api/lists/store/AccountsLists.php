<?php

namespace app\api\lists\store;

use app\api\lists\BaseApiDataLists;
use app\common\enum\user\AccountLogEnum;
use app\common\lists\ListsExtendInterface;
use app\common\lists\ListsSearchInterface;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersLearn;
use app\common\model\user\User;
use app\common\model\user\UserAccountLog;

/**
 * 门店销售订单列表
 */
class AccountsLists extends BaseApiDataLists implements ListsSearchInterface, ListsExtendInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['sn']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['user_id', '=', $this->userId],
            ['change_type', 'in', AccountLogEnum::getStoreIncomeType()]
        ];

        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }

        if (!empty($this->params['month'])) {

        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        $field = 'change_type,change_amount,action,create_time,remark';
        $lists = UserAccountLog::field($field)
            ->where($this->searchWhere)
            ->where($this->queryWhere())
            ->order('id', 'desc')
            ->limit($this->limitOffset, $this->limitLength)
            ->select()
            ->toArray();

        foreach ($lists as &$item) {
            $item['type_desc'] = AccountLogEnum::getChangeTypeDesc($item['change_type']);
            $symbol = $item['action'] == AccountLogEnum::DEC ? '-' : '+';
            $item['change_amount_desc'] = $symbol . $item['change_amount'];
        }

        return $lists;
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return UserAccountLog::where($this->searchWhere)->where($this->queryWhere())->count();
    }

    public function extend()
    {
        return [
            'user_money' => User::where('id', '=', $this->userId)->value('user_money'),
            'total_amount' => Orders::where('first_leader|store_user_id', '=', $this->userId)->whereTime('create_time', 'today')->sum('order_amount'),
            'total_amount2' => OrdersLearn::where('store_user_id', '=', $this->userId)->whereTime('create_time', 'today')->sum('order_amount'),
            'total_month' => UserAccountLog::where($this->searchWhere)->where($this->queryWhere())->sum('change_amount')
        ];
    }
}