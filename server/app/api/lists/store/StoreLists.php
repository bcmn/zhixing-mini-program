<?php

namespace app\api\lists\store;

use app\api\lists\BaseApiDataLists;
use app\common\enum\DefaultEnum;
use app\common\lists\ListsSearchInterface;
use app\common\model\business\Store;
use app\common\model\goods\GoodsDevices;

/**
 * 商品列表
 */
class StoreLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['top_id', 'cate_id', 'name']
        ];
    }

    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['status', '=', DefaultEnum::SHOW],
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        $lists = Store::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('*,status as status_text')
            ->hidden(['sort', 'create_time', 'update_time', 'delete_time'])
            ->limit($this->limitOffset, $this->limitLength)
            ->order('sort desc,id desc')
            ->select()
            ->toArray();
        foreach ($lists as $key => $item) {
            $lists[$key]['distance'] = 0;
            if (!empty($this->params['lat']) && !empty($this->params['lng'])) {
                $lists[$key]['distance'] = getDistance((float)$item['lat'], (float)$item['lng'], (float)$this->params['lat'], (float)$this->params['lng']);
            }
            $lists[$key]['devices'] = GoodsDevices::where('user_id', '=', $item['user_id'])
                ->where('is_active', '=', 1)
                ->findOrEmpty();
            $lists[$key]['devices_num'] = GoodsDevices::where('user_id', '=', $item['user_id'])
                ->where('is_active', '=', 1)
                ->count();
        }
        return $lists;
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Store::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}