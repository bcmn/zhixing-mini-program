<?php

namespace app\api\lists\store;

use app\api\lists\BaseApiDataLists;
use app\common\lists\ListsSearchInterface;
use app\common\model\orders\OrdersDevices;

/**
 * 设备预约订单列表
 */
class OrdersDevicesLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['sn']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere()
    {
        $where = [
            ['store_user_id', '=', $this->userId]
        ];

        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }

        if (isset($this->params['status']) && $this->params['status'] > 0) {
            $where[] = ['status', '=', $this->params['status']];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return OrdersDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,sn,user_id,devices_id,goods_id,lease_type,goods_amount,num,order_amount,notes,status,status as status_text')
            ->limit($this->limitOffset, $this->limitLength)
            ->with(['userInfo', 'devices', 'goodsInfo'])
            ->order('id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return OrdersDevices::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}