<?php

namespace app\api\lists\package;

use app\api\lists\BaseApiDataLists;
use app\common\enum\DefaultEnum;
use app\common\lists\ListsSearchInterface;
use app\common\model\package\Package;

/**
 * 医院列表
 * Class GoodsCateLists
 */
class PackageLists extends BaseApiDataLists implements ListsSearchInterface
{

    /**
     * @return array[]
     * Date: 2023/9/6 21:23
     * Notes：设置搜索条件
     */
    public function setSearch(): array
    {
        return [
            '%like%' => ['title', 'content']
        ];
    }

    /**
     * @param $flag
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：查询条件
     */
    public function queryWhere($flag = true)
    {
        $where = [
            ['status', '=', DefaultEnum::SHOW]
        ];
        if (!empty($this->params['start_time'])) {
            $where[] = ['create_time', '>=', strtotime($this->params['start_time'])];
        }
        if (!empty($this->params['end_time'])) {
            $where[] = ['create_time', '<=', strtotime($this->params['end_time'])];
        }
        return $where;
    }


    /**
     * @return array
     * Date: 2023/9/6 21:24
     * Notes：数据列表
     */
    public function lists(): array
    {
        return Package::where($this->searchWhere)
            ->where($this->queryWhere())
            ->field('id,title,image,amount')
            ->limit($this->limitOffset, $this->limitLength)
            ->order('sort desc,id desc')
            ->select()
            ->toArray();
    }


    /**
     * @return int
     * Date: 2023/9/9 7:52
     * Notes：获取数量
     */
    public function count(): int
    {
        return Package::where($this->searchWhere)
            ->where($this->queryWhere())
            ->count();
    }

}