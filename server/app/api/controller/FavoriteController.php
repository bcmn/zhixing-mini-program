<?php

namespace app\api\controller;

use app\api\lists\user\UserFavoriteLists;
use app\api\logic\user\UserApplyLogic;
use app\api\logic\user\UserFavoriteLogic;
use app\api\validate\user\UserFavoriteValidate;
use think\response\Json;

/**
 * 收藏控制器
 */
class FavoriteController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * Date: 2023/10/23 23:18
     * Notes:我的收藏列表
     */
    public function lists()
    {
        return $this->dataLists(new UserFavoriteLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：添加收藏
     */
    public function add(): Json
    {
        $params = (new UserFavoriteValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserFavoriteLogic::add($params);
        if (false === $result) {
            return $this->fail(UserFavoriteLogic::getError());
        }
        return $this->success(UserFavoriteLogic::getError());
    }

    /**
     * Date: 2023/10/23 23:21
     * Notes:删除收藏
     */
    public function delete(): Json
    {
        $params = (new UserFavoriteValidate())->post()->goCheck('delete');
        UserFavoriteLogic::delete($params['id']);
        return $this->success('删除成功', [], 1, 1);
    }

}