<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersRentLists;
use app\api\logic\orders\OrdersRentLogic;
use app\api\validate\orders\OrdersRentValidate;
use think\response\Json;

/**
 * 续租订单控制器
 */
class OrdersRentController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * @return Json
     * 订单列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersRentLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交订单
     */
    public function createOrder(): Json
    {
        $params = (new OrdersRentValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = OrdersRentLogic::createOrder($params);
        if (false === $result) {
            return $this->fail(OrdersRentLogic::getError());
        }
        return $this->success('提交成功', $result);
    }


}