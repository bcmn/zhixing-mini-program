<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\api\lists\goods\GoodsLists;
use app\api\logic\goods\GoodsLogic;


/**
 * 商品控制器
 */
class GoodsController extends BaseApiController
{

    public array $notNeedLogin = ['lists', 'detail'];

    /**
     * Date: 2023/10/16 22:17
     * Notes:获取商品列表
     */
    public function lists()
    {
        return $this->dataLists(new GoodsLists());
    }

    /**
     * Date: 2023/10/16 22:26
     * Notes:获取商品详情
     */
    public function detail()
    {
        $id = $this->request->get('id/d');
        $result = GoodsLogic::detail($id, $this->userId);
        return $this->data($result);
    }


}