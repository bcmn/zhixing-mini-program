<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersLists;
use app\api\lists\orders\OrdersRenewLists;
use app\api\logic\orders\OrdersLogic;
use app\api\logic\orders\OrdersRenewLogic;
use app\api\validate\orders\OrdersRenewValidate;
use app\api\validate\orders\OrdersValidate;
use think\response\Json;

/**
 * 购买次数或时长订单控制器
 */
class OrdersRenewController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * @return Json
     * 订单列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersRenewLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交订单
     */
    public function createOrder(): Json
    {
        $params = (new OrdersRenewValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = OrdersRenewLogic::createOrder($params);
        if (false === $result) {
            return $this->fail(OrdersRenewLogic::getError());
        }
        return $this->success('提交成功', $result);
    }


}