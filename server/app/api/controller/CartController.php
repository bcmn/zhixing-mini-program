<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersCartLists;
use app\api\logic\orders\OrdersCartLogic;
use app\api\validate\orders\OrdersCartValidate;
use think\response\Json;

/**
 * 购物车
 */
class CartController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * @return Json
     * 购物车列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersCartLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：添加至购物车
     */
    public function add(): Json
    {
        $params = (new OrdersCartValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = OrdersCartLogic::add($params);
        if (false === $result) {
            return $this->fail(OrdersCartLogic::getError());
        }
        return $this->success('加入成功');
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：清空购物车
     */
    public function reduce(): Json
    {
        OrdersCartLogic::reduce($this->userId);
        return $this->success('操作成功');
    }

    public function delete(): Json
    {
        $params = (new OrdersCartValidate())->post()->goCheck('delete', [
            'user_id' => $this->userId
        ]);
        $result = OrdersCartLogic::delete($params);
        if (false === $result) {
            return $this->fail(OrdersCartLogic::getError());
        }
        return $this->success('删除成功');
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：清空购物车
     */
    public function clear(): Json
    {
        OrdersCartLogic::clear($this->userId);
        return $this->success('操作成功');
    }
}