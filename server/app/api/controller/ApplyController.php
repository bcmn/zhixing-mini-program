<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersLists;
use app\api\logic\orders\OrdersLogic;
use app\api\logic\user\UserApplyLogic;
use app\api\validate\orders\OrdersValidate;
use app\api\validate\user\UserApplyValidate;
use think\response\Json;

/**
 * 加盟申请控制器
 */
class ApplyController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交申请
     */
    public function add(): Json
    {
        $params = (new UserApplyValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserApplyLogic::add($params);
        if (false === $result) {
            return $this->fail(UserApplyLogic::getError());
        }
        return $this->success('提交成功');
    }

    /**
     * Date: 2023/9/9 7:57
     * Notes：申请详情
     */
    public function detail(): Json
    {
        $result = UserApplyLogic::detail($this->userId);
        return $this->data($result);
    }

}