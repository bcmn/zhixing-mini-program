<?php

namespace app\api\controller;

use app\api\logic\orders\RefundLogic;
use app\api\validate\RefundValidate;
use think\response\Json;

/**
 * 退款售后控制器
 */
class RefundController extends BaseApiController
{
    /**
     * Date: 2023/5/17 23:48
     * Notes：退款申请
     */
    public function apply(): Json
    {
        $params = (new RefundValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = RefundLogic::refund($params);
        if (true === $result) {
            return $this->success('申请成功，请等待审核', [], 1, 1);
        }
        return $this->fail(RefundLogic::getError());
    }

    /**
     * Date: 2023/11/30 22:18
     * Notes:确认货物寄回
     */
    public function express()
    {
        $params = $this->request->param();
        $result = RefundLogic::express($params);
        if (true === $result) {
            return $this->success('操作成功', [], 1, 1);
        }
        return $this->fail(RefundLogic::getError());
    }


    /**
     * Date: 2023/11/21 22:17
     * Notes:获取退款订单详情
     */
    public function detail(): Json
    {
        $params = $this->request->param();
        $detail = RefundLogic::detail($params);
        return $this->data($detail);
    }

}