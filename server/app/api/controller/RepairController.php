<?php

namespace app\api\controller;

use app\api\lists\user\UserRepairLists;
use app\api\logic\user\UserRepairLogic;
use app\api\validate\user\UserRepairValidate;
use app\common\model\user\UserRepairLogs;
use think\response\Json;

/**
 * 申请维修控制器
 */
class RepairController extends BaseApiController
{

    public array $notNeedLogin = [];

    public function lists()
    {
        return $this->dataLists(new UserRepairLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交申请
     */
    public function add(): Json
    {
        $params = (new UserRepairValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserRepairLogic::add($params);
        if (false === $result) {
            return $this->fail(UserRepairLogic::getError());
        }
        return $this->success('提交成功');
    }

    /**
     * Date: 2023/9/9 7:57
     * Notes：申请详情
     */
    public function detail(): Json
    {
        $id = $this->request->param('id');
        $result = UserRepairLogic::detail($id);
        return $this->data($result);
    }


    /**
     * Date: 2023/12/29 22:17
     * Notes:维修日志列表
     */
    public function logsLists(): Json
    {
        $id = $this->request->param('id');
        if (empty($id)) {
            return $this->fail('参数错误');
        }

        $lists = UserRepairLogs::where('repair_id', $id)->select()->toArray();
        return $this->data($lists);
    }

}