<?php

namespace app\api\controller;

use app\common\model\goods\GoodsDevices;
use app\common\service\MachineService;

/**
 * 设备控制器
 */
class MachineController extends BaseApiController
{

    public array $notNeedLogin = ['notify', 'test'];


    public function test()
    {
//        $result = MachineService::sendMessage('460048857111166');
//        return $this->success($result);
    }

    /**
     * Date: 2023/10/27 0:09
     * Notes:OneNet平台回调
     */
    public function notify(): string
    {
//        $params = json_decode('{"msg":{"at":"1699533089415","imei":"861357062207834","type":"1","ds_id":"3200_0_5750","value":"time","dev_id":"2122530614"},"msg_signature":"hlrR0mOswotd0LYUO8lNsg==","nonce":"5SBFDPSb"}');

        $params = $this->request->param();
        $data = $params['msg'];
        $imei = $data['imei'] ?? '';
        if (!empty($imei)) {
            ob_end_clean();
            ob_start();
            echo json_encode($data);
            $size = ob_get_length();
            header("HTTP/1.1 200 OK");
            header("Content-Length: $size");
            header("Connection: close");
            header("Content-Type: application/json;charset=utf-8");
            ob_end_flush();
            if (ob_get_length()) {
                ob_flush();
            }
            flush();
            if (function_exists("fastcgi_finish_request")) {
                fastcgi_finish_request();
            }
            sleep(1);
            ignore_user_abort(true);
            set_time_limit(300);

            MachineService::handleDevices($params);
            return '';
        } else {
            $data = preg_replace('/(\'|\")/', '', $data);
            echo $data;
            die();
        }
    }

}