<?php

namespace app\api\controller;

use app\api\lists\goods\GoodsDevicesLists;
use app\api\logic\goods\GoodsDevicesLogic;
use app\api\validate\goods\GoodsDevicesValidate;
use think\response\Json;


/**
 * 商品控制器
 */
class DevicesController extends BaseApiController
{

    public array $notNeedLogin = ['detail'];

    /**
     * Date: 2023/10/16 22:17
     * Notes:获取设备列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new GoodsDevicesLists());
    }

    /**
     * Date: 2023/10/16 22:26
     * Notes:获取设备详情
     */
    public function detail(): Json
    {
        $sn = $this->request->param('sn');
        $user_id = $this->userId;
        $result = GoodsDevicesLogic::detail($sn, $user_id);
        return $this->data($result);
    }

    /**
     * Date: 2023/11/3 22:59
     * Notes:激活设备
     */
    public function active(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('active', [
            'user_id' => $this->userId
        ]);
        $result = GoodsDevicesLogic::active($params);
        if (false === $result) {
            return $this->fail(GoodsDevicesLogic::getError());
        }
        return $this->success('绑定成功');
    }

    /**
     * Date: 2024/4/23 19:36
     * Notes: 设备转让
     */
    public function transfer(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('transfer', [
            'user_id' => $this->userId
        ]);
        $result = GoodsDevicesLogic::transfer($params);
        if (false === $result) {
            return $this->fail(GoodsDevicesLogic::getError());
        }
        return $this->success('转让成功');
    }

    /**
     * Date: 2024/2/13 8:27
     * Notes:设备录入，具有权限的客户扫描二维码获取到设备的SN和IMEI后，选择对应的商品，然后录入设备信息
     */
    public function add(): Json
    {
        $params = (new GoodsDevicesValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = GoodsDevicesLogic::add($params);
        if (false === $result) {
            return $this->fail(GoodsDevicesLogic::getError());
        }
        return $this->success('录入成功');
    }
}