<?php

namespace app\api\controller;

use app\api\lists\store\AccountsLists;
use app\api\lists\store\StoreLists;
use app\api\lists\store\StoreOrdersLists;
use app\api\logic\business\StoreLogic;
use think\response\Json;

/**
 * 加盟申请控制器
 */
class StoreController extends BaseApiController
{

    public array $notNeedLogin = ['lists'];

    /**
     * Date: 2023/11/2 23:33
     * Notes:获取门店列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new StoreLists());
    }

    /**
     * Date: 2023/11/7 20:20
     * Notes:门店详情
     */
    public function detail(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $result = StoreLogic::detail($params);
        return $this->data($result);
    }

    /**
     * @return Json
     * 修改门店资料
     */
    public function edit(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $result = StoreLogic::edit($params);
        if ($result === false) {
            return $this->fail(StoreLogic::getError());
        }
        return $this->success('操作成功');
    }


    /**
     * @return Json
     * 销售订单列表
     */
    public function ordersLists(): Json
    {
        return $this->dataLists(new StoreOrdersLists());
    }

    /**
     * 门店财务管理
     */
    public function accountLogs(): Json
    {
        return $this->dataLists(new AccountsLists());
    }
}