<?php

namespace app\api\controller;

use app\api\lists\user\UserAddressLists;
use app\api\logic\user\UserAddressLogic;
use app\api\validate\user\UserAddressValidate;
use think\response\Json;


/**
 * 用户地址控制器
 */
class AddressController extends BaseApiController
{

    public array $notNeedLogin = ['lists', 'detail'];

    /**
     * Date: 2023/10/16 22:17
     * Notes:获取用户收货地址列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new UserAddressLists());
    }

    /**
     * Date: 2023/10/16 22:26
     * Notes:获取用户收货地址详情
     */
    public function detail(): Json
    {
        $id = $this->request->get('id/d');
        $result = UserAddressLogic::detail($id);
        return $this->data($result);
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new UserAddressValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserAddressLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(UserAddressLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new UserAddressValidate())->post()->goCheck('edit', [
            'user_id' => $this->userId
        ]);
        $result = UserAddressLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(UserAddressLogic::getError());
    }

    public function delete(): Json
    {
        $params = (new UserAddressValidate())->post()->goCheck('delete');
        UserAddressLogic::delete($params['id']);
        return $this->success('删除成功', [], 1, 1);
    }

    public function setDefault(): Json
    {
        $params = (new UserAddressValidate())->post()->goCheck('delete', [
            'user_id' => $this->userId
        ]);
        UserAddressLogic::setDefault($params);
        return $this->success('设置成功', [], 1, 1);
    }

}