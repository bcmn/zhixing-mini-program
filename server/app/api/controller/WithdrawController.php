<?php

namespace app\api\controller;

use app\api\lists\user\WithdrawLists;
use app\api\logic\user\WithdrawLogic;
use app\api\validate\WithdrawValidate;
use think\response\Json;

/**
 * 陪护师控制器
 */
class WithdrawController extends BaseApiController
{

    public array $notNeedLogin = ['lists', 'detail'];

    /**
     * @return Json
     * 获取提现列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new WithdrawLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：申请提现
     */
    public function apply(): Json
    {
        $params = (new WithdrawValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = WithdrawLogic::add($params);
        if (true === $result) {
            return $this->success('申请成功，请等待审核', [], 1, 1);
        }
        return $this->fail(WithdrawLogic::getError());
    }


}