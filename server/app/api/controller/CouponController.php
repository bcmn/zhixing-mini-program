<?php

namespace app\api\controller;

use app\api\logic\CouponLogic;
use think\response\Json;

/**
 * 优惠券
 */
class CouponController extends BaseApiController
{

    public array $notNeedLogin = [];


    /**
     * Date: 2023/5/17 23:48
     * Notes：获取用户的优惠券列表
     */
    public function lists(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $result = CouponLogic::getAllData($params);
        return $this->data($result);
    }
}