<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersLists;
use app\api\logic\orders\OrdersLogic;
use app\api\validate\orders\OrdersValidate;
use app\common\model\orders\OrdersExpressLogs;
use app\common\service\Kuaidi100Service;
use think\response\Json;

/**
 * 订单控制器
 */
class OrdersController extends BaseApiController
{

    public array $notNeedLogin = ['express'];

    /**
     * @return Json
     * 订单列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersLists());
    }

    public function confirmOrder(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $result = OrdersLogic::confirmOrder($params);
        return $this->data($result);
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交订单
     */
    public function createOrder(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = OrdersLogic::createOrder($params);
        if (false === $result) {
            return $this->fail(OrdersLogic::getError());
        }
        return $this->success('提交成功', $result);
    }

    /**
     * Date: 2023/9/9 7:57
     * Notes：订单详情
     */
    public function detail(): Json
    {
        $id = $this->request->param('id', 0);
        $result = OrdersLogic::detail($id);
        return $this->data($result);
    }

    public function orderGoodsDetail(): Json
    {
        $id = $this->request->param('id', 0);
        $result = OrdersLogic::orderGoodsDetail($id);
        return $this->data($result);
    }

    /**
     * @return Json
     * 确认收货
     */
    public function finish(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('finish', [
            'user_id' => $this->userId
        ]);
        $result = OrdersLogic::finish($params);
        if (false === $result) {
            return $this->fail(OrdersLogic::getError());
        }
        return $this->success('操作成功');
    }

    /**
     * @return Json
     * 取消订单
     */
    public function cancel(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('cancel', [
            'user_id' => $this->userId
        ]);
        $result = OrdersLogic::cancel($params);
        if (false === $result) {
            return $this->fail(OrdersLogic::getError());
        }
        return $this->success('操作成功');
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:删除订单
     */
    public function delete(): Json
    {
        $params = (new OrdersValidate())->post()->goCheck('delete', [
            'user_id' => $this->userId
        ]);
        OrdersLogic::delete($params);
        return $this->success('操作成功');
    }

    /**
     * Date: 2023/12/4 20:22
     * Notes:查询物流轨迹
     */
    public function express(): Json
    {
        $id = $this->request->param('order_id', 0);
        //查询订单上次查询的时间(超过30分钟才能再次查询更新)
        $express = OrdersExpressLogs::where('order_id', $id)->findOrEmpty();
        $express_result = [];
        if (!$express->isEmpty()) {
            $express_result['id'] = $express['id'];
            if ($express['update_time'] > (time() - 180)) {
                return $this->data(json_decode($express->content, true));
            }
        }
        $result = Kuaidi100Service::expresss($id, $express_result);
        return $this->data($result);
    }

}