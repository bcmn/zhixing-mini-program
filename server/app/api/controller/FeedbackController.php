<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersLists;
use app\api\logic\orders\OrdersLogic;
use app\api\logic\user\UserApplyLogic;
use app\api\logic\user\UserFeedbackLogic;
use app\api\validate\orders\OrdersValidate;
use app\api\validate\user\UserApplyValidate;
use app\api\validate\user\UserFeedbackValidate;
use think\response\Json;

/**
 * 意见反馈控制器
 */
class FeedbackController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交反馈
     */
    public function add(): Json
    {
        $params = (new UserFeedbackValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserFeedbackLogic::add($params);
        if (false === $result) {
            return $this->fail(UserFeedbackLogic::getError());
        }
        return $this->success('提交成功');
    }

    /**
     * Date: 2023/9/9 7:57
     * Notes：反馈详情
     */
    public function detail(): Json
    {
        $result = UserFeedbackLogic::detail($this->request->param('id'));
        return $this->data($result);
    }

}