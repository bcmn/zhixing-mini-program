<?php

namespace app\api\controller;

use app\api\lists\site\NewsLists;
use app\common\enum\goods\GoodsEnum;
use app\common\model\advertise\Advertise;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsCate;
use app\common\service\ConfigService;
use app\common\service\FileService;

/**
 * 收藏控制器
 */
class SiteController extends BaseApiController
{

    public array $notNeedLogin = ['index', 'newsLists', 'config', 'about'];

    /**
     * Date: 2023/12/19 22:45
     * Notes:网站首页
     */
    public function index()
    {
        //头部轮播图
        $slides = Advertise::where('cate_id', 8)->where('status', 1)->field('id,image,title')->select();

        //服务宗旨
        $service = [
            [
                'title' => ConfigService::get('site', 'service_text1'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image1'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text2'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image2'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text3'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image3'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text4'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image4'))
            ],
        ];

        //产品分类
        $goodsCate = GoodsCate::where('status', 1)->field('id,title,image')->select();

        //产品展示
        $goods = Goods::where('status', 1)
            ->where('type', GoodsEnum::TYPE_NORMAL)
            ->field('id,name,image,price,sales')
            ->limit(4)
            ->select();
        //租赁展示
        $goodsRent = Goods::where('status', 1)
            ->where('type', GoodsEnum::TYPE_RENT)
            ->field('id,name,image,price,sales')
            ->limit(4)
            ->select();

        //企业简介
        $about = [
            'content' => get_file_domain(ConfigService::get('site', 'about_content')),
            'image' => FileService::getFileUrl(ConfigService::get('site', 'about_logo'))
        ];

        //品牌实力
        $brand_title = ConfigService::get('site', 'brand_title');
        $brand = [
            [
                'title' => ConfigService::get('site', 'brand_text1'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'brand_image1')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'brand_note1'))
            ],
            [
                'title' => ConfigService::get('site', 'brand_text2'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'brand_image2')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'brand_note2'))
            ],
            [
                'title' => ConfigService::get('site', 'brand_text3'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'brand_image3')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'brand_note3'))
            ],
            [
                'title' => ConfigService::get('site', 'brand_text4'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'brand_image4')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'brand_note4'))
            ]
        ];

        $joinImage = FileService::getFileUrl(ConfigService::get('site', 'about_logo'));

        return $this->data(compact('slides', 'service', 'goodsCate', 'goods', 'goodsRent', 'about', 'brand_title', 'brand', 'joinImage'));
    }

    public function about()
    {
        //头部轮播图
        $slides = Advertise::where('cate_id', 9)->where('status', 1)->field('id,image,title')->select();

        //服务宗旨
        $service = [
            [
                'title' => ConfigService::get('site', 'service_text1'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image1'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text2'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image2'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text3'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image3'))
            ],
            [
                'title' => ConfigService::get('site', 'service_text4'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'service_image4'))
            ],
        ];

        //企业简介
        $about = [
            'content' => get_file_domain(ConfigService::get('site', 'about_content')),
            'image' => FileService::getFileUrl(ConfigService::get('site', 'about_logo'))
        ];

        //荣誉
        $honor_title = ConfigService::get('site', 'honor_title');
        $honor = [
            [
                'title' => ConfigService::get('site', 'honor_text1'),
                'num' => ConfigService::get('site', 'honor_num1')
            ],
            [
                'title' => ConfigService::get('site', 'honor_text2'),
                'num' => ConfigService::get('site', 'honor_num2')
            ],
            [
                'title' => ConfigService::get('site', 'honor_text3'),
                'num' => ConfigService::get('site', 'honor_num3')
            ],
            [
                'title' => ConfigService::get('site', 'honor_text4'),
                'num' => ConfigService::get('site', 'honor_num4')
            ],
        ];

        //企业文化
        $culture_title = ConfigService::get('site', 'culture_title');
        $culture = [
            [
                'title' => ConfigService::get('site', 'culture_text1'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image1')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note1'))
            ],
            [
                'title' => ConfigService::get('site', 'culture_text2'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image2')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note2'))
            ],
            [
                'title' => ConfigService::get('site', 'culture_text3'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image3')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note3'))
            ],
            [
                'title' => ConfigService::get('site', 'culture_text4'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image4')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note4'))
            ],
            [
                'title' => ConfigService::get('site', 'culture_text5'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image5')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note5'))
            ],
            [
                'title' => ConfigService::get('site', 'culture_text6'),
                'image' => FileService::getFileUrl(ConfigService::get('site', 'culture_image6')),
                'note' => FileService::getFileUrl(ConfigService::get('site', 'culture_note6'))
            ],
        ];

        return $this->data(compact('slides', 'service', 'about', 'honor_title', 'honor', 'culture_title', 'culture'));
    }

    /**
     * Date: 2023/12/19 23:00
     * Notes:配置信息
     */
    public function config()
    {
        return $this->data([
            'name' => ConfigService::get('site', 'name'),
            'web_favicon' => FileService::getFileUrl(ConfigService::get('site', 'web_favicon')),
            'web_logo' => FileService::getFileUrl(ConfigService::get('site', 'web_logo')),
            'contact_title' => ConfigService::get('site', 'contact_title'),
            'contact' => [
                [
                    'title' => ConfigService::get('site', 'contact_text1'),
                    'image' => FileService::getFileUrl(ConfigService::get('site', 'contact_image1')),
                    'note' => ConfigService::get('site', 'contact_note1'),
                ],
                [
                    'title' => ConfigService::get('site', 'contact_text2'),
                    'image' => FileService::getFileUrl(ConfigService::get('site', 'contact_image2')),
                    'note' => ConfigService::get('site', 'contact_note2'),
                ],
            ],
            'footer_qrcode' => FileService::getFileUrl(ConfigService::get('site', 'footer_qrcode')),
            'copyright' => ConfigService::get('site', 'copyright'),
            'icp' => ConfigService::get('site', 'icp'),
        ]);
    }

    /**
     * Date: 2023/10/23 23:18
     * Notes:新闻列表
     */
    public function newsLists()
    {
        return $this->dataLists(new NewsLists());
    }

}