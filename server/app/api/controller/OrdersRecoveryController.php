<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersRecoveryLists;
use app\api\logic\orders\OrdersRecoveryLogic;
use app\api\logic\orders\RefundLogic;
use app\api\validate\orders\OrdersRecoveryValidate;
use app\api\validate\RefundValidate;
use app\common\enum\RefundEnum;
use app\common\model\goods\GoodsDevices;
use app\common\model\goods\GoodsDevicesTransfer;
use app\common\model\refund\RefundRecord;
use think\response\Json;

/**
 * 回收订单控制器
 */
class OrdersRecoveryController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * @return Json
     * 回收订单列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersRecoveryLists());
    }


    /**
     * Date: 2023/9/9 7:57
     * Notes：订单详情
     */
    public function detail(): Json
    {
        $params = $this->request->param();
        $result = RefundLogic::detail($params);
        return $this->data($result);
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:门店确认回收
     */
    public function confirm(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $params['status'] = RefundEnum::REFUND_CONFIRM;
        RefundLogic::confirm($params);
        return $this->success('操作成功');
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:门店拒绝回收
     */
    public function cancel(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $params['status'] = RefundEnum::REFUND_ERROR;
        RefundLogic::confirm($params);
        return $this->success('操作成功');
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:用户确认收款
     */
    public function finish(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $params['status'] = RefundEnum::REFUND_SUCCESS;
        RefundLogic::confirm($params);

        $device_sn = RefundRecord::where('id', '=', $params['id'])->value('devices_sn');
        $device = GoodsDevices::where('sn', '=', $device_sn)->findOrEmpty();
        if (!$device->isEmpty()) {
            // 创建设备转移记录
            GoodsDevicesTransfer::create([
                'devices_id' => $device['id'],
                'sn' => $device['sn'],
                'user_id' => $device['user_id'],
                'receive_id' => $this->userId
            ]);

            //更改设备所属人
            GoodsDevices::update([
                'id' => $device['id'],
                'user_id' => $this->userId
            ]);
        }


        return $this->success('操作成功');
    }
}