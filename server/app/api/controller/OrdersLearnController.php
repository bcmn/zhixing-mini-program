<?php

namespace app\api\controller;

use app\api\lists\orders\OrdersLearnLists;
use app\api\logic\orders\OrdersLearnLogic;
use app\api\validate\orders\OrdersLearnValidate;
use think\response\Json;

/**
 * 设备预约订单控制器
 */
class OrdersLearnController extends BaseApiController
{

    public array $notNeedLogin = [];

    /**
     * @return Json
     * 预约订单列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new OrdersLearnLists());
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：提交订单
     */
    public function createOrder(): Json
    {
        $params = (new OrdersLearnValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = OrdersLearnLogic::createOrder($params);
        if (false === $result) {
            return $this->fail(OrdersLearnLogic::getError());
        }
        return $this->success('提交成功', $result);
    }

    /**
     * Date: 2023/9/9 7:57
     * Notes：订单详情
     */
    public function detail(): Json
    {
        $id = $this->request->param('id', 0);
        $result = OrdersLearnLogic::detail($id);
        return $this->data($result);
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:核销完成订单
     */
    public function finish(): Json
    {
        $params = (new OrdersLearnValidate())->post()->goCheck('delete', [
            'user_id' => $this->userId
        ]);
        OrdersLearnLogic::finish($params);
        return $this->success('操作成功');
    }

    /**
     * Date: 2023/11/2 23:14
     * Notes:删除订单
     */
    public function delete(): Json
    {
        $params = (new OrdersLearnValidate())->post()->goCheck('delete', [
            'user_id' => $this->userId
        ]);
        OrdersLearnLogic::delete($params);
        return $this->success('操作成功');
    }

    /**
     * @return Json
     * 取消订单
     */
    public function cancel(): Json
    {
        $params = (new OrdersLearnValidate())->post()->goCheck('cancel', [
            'user_id' => $this->userId
        ]);
        $result = OrdersLearnLogic::cancel($params);
        if (false === $result) {
            return $this->fail(OrdersLearnLogic::getError());
        }
        return $this->success('操作成功');
    }
}