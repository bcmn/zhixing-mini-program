<?php

namespace app\api\controller;

use app\api\lists\user\UserBankLists;
use app\api\logic\user\UserBankLogic;
use app\api\validate\user\UserBankValidate;
use think\response\Json;


/**
 * 用户提现账户控制器
 */
class BankController extends BaseApiController
{

    public array $notNeedLogin = ['lists', 'detail'];

    /**
     * Date: 2023/10/16 22:17
     * Notes:获取用户收货地址列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new UserBankLists());
    }

    /**
     * Date: 2023/10/16 22:26
     * Notes:获取用户收货地址详情
     */
    public function detail(): Json
    {
        $id = $this->request->get('id/d');
        $result = UserBankLogic::detail($id);
        return $this->data($result);
    }

    /**
     * Date: 2023/5/17 23:48
     * Notes：添加数据
     */
    public function add(): Json
    {
        $params = (new UserBankValidate())->post()->goCheck('add', [
            'user_id' => $this->userId
        ]);
        $result = UserBankLogic::add($params);
        if (true === $result) {
            return $this->success('添加成功', [], 1, 1);
        }
        return $this->fail(UserBankLogic::getError());
    }


    /**
     * Date: 2023/5/17 23:49
     * Notes：编辑
     */
    public function edit(): Json
    {
        $params = (new UserBankValidate())->post()->goCheck('edit', [
            'user_id' => $this->userId
        ]);
        $result = UserBankLogic::edit($params);
        if (true === $result) {
            return $this->success('编辑成功', [], 1, 1);
        }
        return $this->fail(UserBankLogic::getError());
    }

    public function delete(): Json
    {
        $params = (new UserBankValidate())->post()->goCheck('delete');
        UserBankLogic::delete($params['id']);
        return $this->success('删除成功', [], 1, 1);
    }

}