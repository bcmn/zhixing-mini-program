<?php

namespace app\api\controller;

use app\api\lists\HospitalLists;
use app\api\logic\business\HospitalLogic;
use think\response\Json;

/**
 * 合作医院
 */
class HospitalController extends BaseApiController
{

    public array $notNeedLogin = [];


    /**
     * Date: 2023/5/17 23:48
     * Notes：获取合作的医院列表
     */
    public function lists(): Json
    {
        return $this->dataLists(new HospitalLists());
    }

    /**
     * Date: 2023/11/7 20:20
     * Notes:门店详情
     */
    public function detail(): Json
    {
        $params = $this->request->param();
        $params['user_id'] = $this->userId;
        $result = HospitalLogic::detail($params);
        return $this->data($result);
    }
}