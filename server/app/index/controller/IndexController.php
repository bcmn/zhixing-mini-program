<?php

namespace app\index\controller;

use app\BaseController;
use app\common\model\goods\GoodsDevices;
use app\common\service\JsonService;
use think\facade\Request;
use think\facade\View;

class IndexController extends BaseController
{

    /**
     * @notes 主页
     * @param string $name
     * @return \think\response\Json|\think\response\View
     * @author 段誉
     * @date 2022/10/27 18:12
     */
    public function index($name = '你好,PHP')
    {
        $template = app()->getRootPath() . 'public/web/index.html';
//        if (Request::isMobile()) {
//            $template = app()->getRootPath() . 'public/mobile/index.html';
//        }
        if (file_exists($template)) {
            return view($template);
        }
        return JsonService::success($name);
    }


    /**
     * Date: 2023/12/23 20:46
     * Notes:设备地图
     */
    public function map()
    {
        return view();
    }

    public function mapData()
    {
        $devices = GoodsDevices::where('is_active', 1)
            ->whereNotNull('lng')
            ->whereNotNull('lat')
            ->field('id,goods_name,goods_image,lng,lat,user_id as userInfo')
            ->select()
            ->toArray();
        return JsonService::data($devices);
    }

}
