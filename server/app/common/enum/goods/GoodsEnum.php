<?php

namespace app\common\enum\goods;

/**
 * 商品枚举
 */
class GoodsEnum
{
    const TYPE_NORMAL = 10;
    const TYPE_RENT = 20;
    const TYPE_PART = 30;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取商品类型
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::TYPE_NORMAL => '普通商品',
            self::TYPE_RENT => '租赁商品',
            self::TYPE_PART => '配件',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}