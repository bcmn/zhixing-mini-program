<?php

namespace app\common\enum\goods;

/**
 * 商品设备枚举
 */
class GoodsDevicesEnum
{
    const ACTIVE_YES = 1;
    const ACTIVE_NO = 0;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取商品类型
     */
    public static function getActiveDesc($value = true)
    {
        $data = [
            self::ACTIVE_YES => '已激活',
            self::ACTIVE_NO => '未激活',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}