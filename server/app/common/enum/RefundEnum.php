<?php

namespace app\common\enum;

class RefundEnum
{

    // 退款类型
    const TYPE_MONEY = 1;
    const TYPE_GOODS_MONEY = 2;
    const TYPE_SECURITY = 3;
    const TYPE_SECURITY_STORE = 4;

    // 退款状态
    const REFUND_ING = 0;//退款中
    const REFUND_CONFIRM = 1;//等待退货
    const REFUND_POSTAGE = 2;//用户已发货，等待收货
    const REFUND_PAY = 3;//已收到货，等待打款 / 门店确认回收，等待用户确认收款
    const REFUND_SUCCESS = 4;//退款成功
    const REFUND_ERROR = 5;//退款失败

    // 退款方式
    const REFUND_BALANCE = 1; // 余额退款
    const REFUND_WEIXIN = 2; // 微信退款


    // 退款订单类型
    const ORDER_TYPE_ORDER = 'order'; // 普通订单
    const ORDER_TYPE_RECHARGE = 'recharge'; // 充值订单


    /**
     * @notes 退款类型描述
     * @param bool $value
     * @return string|string[]
     * @author 段誉
     * @date 2022/12/1 10:40
     */
    public static function getTypeDesc($value = true)
    {
        $data = [
            self::TYPE_MONEY => '仅退款',
            self::TYPE_GOODS_MONEY => '退货退款',
            self::TYPE_SECURITY => '退压金',
            self::TYPE_SECURITY_STORE => '门店退压金',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }


    /**
     * @notes 退款状态
     * @param bool $value
     * @return string|string[]
     * @date 2022/12/1 10:43
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::REFUND_ING => '退款审核中',
            self::REFUND_CONFIRM => '等待用户退货',
            self::REFUND_POSTAGE => '等待商家收货',
            self::REFUND_PAY => '等待商家打款',
            self::REFUND_SUCCESS => '售后退款成功',
            self::REFUND_ERROR => '售后退款失败',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }


    /**
     * @notes 退款方式
     * @param bool $value
     * @return string|string[]
     * @date 2022/12/1 10:43
     */
    public static function getWayDesc($value = true)
    {
        $data = [
            self::REFUND_BALANCE => '余额退款',
            self::REFUND_WEIXIN => '微信退款',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }


    /**
     * @notes 通过支付方式获取退款方式
     * @param $payWay
     * @return int
     * @author 段誉
     * @date 2022/12/6 10:31
     */
    public static function getRefundWayByPayWay($payWay)
    {
        if (in_array($payWay, [PayEnum::ALI_PAY, PayEnum::WECHAT_PAY])) {
            return self::REFUND_WEIXIN;
        }
        return self::REFUND_BALANCE;
    }

}