<?php

namespace app\common\enum;

/**
 * 商品枚举
 */
class CouponEnum
{
    const STATUS_WAIT = 1;
    const STATUS_USED = 2;
    const STATUS_FAIL = 3;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取商品类型
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::STATUS_WAIT => '待使用',
            self::STATUS_USED => '已使用',
            self::STATUS_FAIL => '已失效',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}