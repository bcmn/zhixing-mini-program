<?php

namespace app\common\enum\orders;

/**
 * 设备预约订单枚举
 */
class OrdersLearnEnum
{
    const STATUS_WAIT = 1;
    const STATUS_PAY = 2;
    const STATUS_SUCCESS = 3;
    const STATUS_CANCEL = 4;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取状态
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::STATUS_WAIT => '待支付',
            self::STATUS_PAY => '待使用',
            self::STATUS_SUCCESS => '已核销',
            self::STATUS_CANCEL => '已取消',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}