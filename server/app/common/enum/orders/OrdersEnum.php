<?php

namespace app\common\enum\orders;

/**
 * 订单枚举
 */
class OrdersEnum
{
    const STATUS_WAIT = 1;
    const STATUS_PAY = 2;
    const STATUS_DELIVER = 3;
    const STATUS_FINISH = 4;
    const STATUS_CANCEL = 20;
    const STATUS_REFUND = 30;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取申请状态
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::STATUS_WAIT => '待支付',
            self::STATUS_PAY => '待发货',
            self::STATUS_DELIVER => '已发货',
            self::STATUS_FINISH => '已完成',
            self::STATUS_CANCEL => '已取消',
            self::STATUS_REFUND => '售后',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}