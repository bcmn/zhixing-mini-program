<?php

namespace app\common\enum\orders;

/**
 * 次数或时长购买订单枚举
 */
class OrdersRentEnum
{
    const STATUS_WAIT = 1;
    const STATUS_PAY = 2;
    const STATUS_SUCCESS = 3;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取状态
     */
    public static function getStatusDesc($value = true)
    {
        $data = [
            self::STATUS_WAIT => '待支付',
            self::STATUS_PAY => '待使用',
            self::STATUS_SUCCESS => '已完成',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}