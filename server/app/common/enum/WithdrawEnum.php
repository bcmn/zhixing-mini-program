<?php

namespace app\common\enum;

/**
 * 陪护师申请枚举
 */
class WithdrawEnum
{
    const STATUS_WAIT = 0;
    const STATUS_PASS = 1;
    const STATUS_REFUSE = 2;

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取申请状态
     */
    public static function getStausDesc($value = true)
    {
        $data = [
            self::STATUS_WAIT => '待审核',
            self::STATUS_PASS => '已通过',
            self::STATUS_REFUSE => '已拒绝'
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}