<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\common\enum\user;

/**
 * 用户账户流水变动表枚举
 * Class AccountLogEnum
 * @package app\common\enum
 */
class AccountLogEnum
{
    /**
     * 变动类型命名规则：对象_动作_简洁描述
     * 动作 DEC-减少 INC-增加
     */

    /**
     * 变动对象
     * UM 用户余额(user_money)
     * UI 用户积分(user_integral)
     */
    const UM = 1;
    const UI = 2;

    /**
     * 动作
     * INC 增加
     * DEC 减少
     */
    const INC = 1;
    const DEC = 2;


    /**
     * 用户余额减少类型
     */
    const UM_DEC_ADMIN = 100;
    const UM_DEC_RECHARGE_REFUND = 101;
    const UM_DEC_WITHDRAW = 102;
    const UM_DEC_BUY_GOODS = 103;

    /**
     * 用户余额增加类型
     */
    const UM_INC_ADMIN = 200;
    const UM_INC_RECHARGE = 201;
    const UM_INC_ORDERS = 203;
    const UM_INC_ORDERS_DEVICES = 204;
    const UM_INC_ORDERS_SALES = 205;
    const UM_INC_COMMISSION = 206;
    const UM_INC_COMMISSION_ORDER = 207;
    const UM_INC_COMMISSION_LEARN = 208;
    const UM_INC_COMMISSION_RENT = 209;
    const UM_INC_COMMISSION_RENEW = 210;
    const UM_INC_ORDERS_CANCEL = 211;
    const UM_INC_ORDERS_LEARN_CANCEL = 212;
    const UM_INC_ORDERS_REFUND = 213;

    const UM_INC_WITHDRAW = 214;


    /**
     * 用户余额（减少类型汇总）
     */
    const UM_DEC = [
        self::UM_DEC_ADMIN,
        self::UM_DEC_RECHARGE_REFUND,
        self::UM_DEC_WITHDRAW,
        self::UM_DEC_BUY_GOODS
    ];


    /**
     * 用户余额（增加类型汇总）
     */
    const UM_INC = [
        self::UM_INC_ADMIN,
        self::UM_INC_RECHARGE,
        self::UM_INC_ORDERS,
        self::UM_INC_ORDERS_DEVICES,
        self::UM_INC_ORDERS_SALES,
        self::UM_INC_COMMISSION,
        self::UM_INC_COMMISSION_ORDER,
        self::UM_INC_COMMISSION_LEARN,
        self::UM_INC_COMMISSION_RENT,
        self::UM_INC_COMMISSION_RENEW,
        self::UM_INC_ORDERS_CANCEL,
        self::UM_INC_ORDERS_LEARN_CANCEL,
        self::UM_INC_ORDERS_REFUND,
        self::UM_INC_WITHDRAW
    ];


    /**
     * 用户积分减少类型
     */
    const UI_DEC_ADMIN = 300;
    const UI_DEC_BUY_GOODS = 301;

    /**
     * 用户积分增加类型
     */
    const UI_INC_ADMIN = 400;
    const UI_INC_BUY_GOODS = 401;

    /**
     * 用户余额（减少类型汇总）
     */
    const UI_DEC = [
        self::UI_DEC_ADMIN,
        self::UI_DEC_BUY_GOODS,
    ];


    /**
     * 用户余额（增加类型汇总）
     */
    const UI_INC = [
        self::UI_INC_ADMIN,
        self::UI_INC_BUY_GOODS,
    ];

    /**
     * @notes 动作描述
     * @param $action
     * @param false $flag
     * @return string|string[]
     * @author 段誉
     * @date 2023/2/23 10:07
     */
    public static function getActionDesc($action, $flag = false)
    {
        $desc = [
            self::DEC => '减少',
            self::INC => '增加',
        ];
        if ($flag) {
            return $desc;
        }
        return $desc[$action] ?? '';
    }


    /**
     * @notes 变动类型描述
     * @param $changeType
     * @param false $flag
     * @return string|string[]
     * @author 段誉
     * @date 2023/2/23 10:07
     */
    public static function getChangeTypeDesc($changeType, $flag = false)
    {
        $desc = [
            self::UM_DEC_ADMIN => '平台减少余额',
            self::UM_INC_ADMIN => '平台增加余额',
            self::UM_INC_ORDERS => '订单收入',
            self::UM_INC_RECHARGE => '充值增加余额',
            self::UM_DEC_RECHARGE_REFUND => '充值订单退款减少余额',
            self::UM_DEC_WITHDRAW => '提现支出',
            self::UM_DEC_BUY_GOODS => '购物减少余额',
            self::UI_DEC_ADMIN => '平台减少积分',
            self::UI_DEC_BUY_GOODS => '购物积分抵扣',
            self::UI_INC_ADMIN => '平台增加积分',
            self::UI_INC_BUY_GOODS => '购物赠送积分',
            self::UM_INC_ORDERS_DEVICES => '设备预约订单收入',
            self::UM_INC_ORDERS_SALES => '设备租赁回收收入',
            self::UM_INC_COMMISSION => '推荐佣金收入',
            self::UM_INC_COMMISSION_ORDER => '订单佣金收入',
            self::UM_INC_COMMISSION_LEARN => '预约体验订单佣金收入',
            self::UM_INC_COMMISSION_RENT => '续租订单佣金收入',
            self::UM_INC_COMMISSION_RENEW => '购买次数或时长佣金收入',
            self::UM_INC_ORDERS_CANCEL => '订单取消退回',
            self::UM_INC_ORDERS_LEARN_CANCEL => '预约订单取消退回',
            self::UM_INC_ORDERS_REFUND => '订单售后退款',
            self::UM_INC_WITHDRAW => '提现拒绝退回',
        ];
        if ($flag) {
            return $desc;
        }
        return $desc[$changeType] ?? '';
    }

    /**
     * Date: 2024/5/7 16:08
     * Notes: 获取门店收入类型
     */
    public static function getStoreIncomeType()
    {
        return [
            self::UM_INC_ORDERS_DEVICES,
            self::UM_INC_ORDERS_SALES,
            self::UM_INC_COMMISSION,
            self::UM_INC_COMMISSION_ORDER,
            self::UM_INC_COMMISSION_LEARN,
            self::UM_INC_COMMISSION_RENT,
            self::UM_INC_COMMISSION_RENEW,
        ];
    }

    /**
     * Date: 2024/5/7 16:08
     * Notes: 获取分红收入类型
     */
    public static function getCommissionIncomeType()
    {
        return [
            self::UM_INC_COMMISSION_ORDER,
            self::UM_INC_COMMISSION_LEARN,
            self::UM_INC_COMMISSION_RENT,
            self::UM_INC_COMMISSION_RENEW,
        ];
    }


    /**
     * @notes 获取用户余额类型描述
     * @return string|string[]
     * @author 段誉
     * @date 2023/2/23 10:08
     */
    public static function getUserMoneyChangeTypeDesc()
    {
        $UMChangeType = self::getUserMoneyChangeType();
        $changeTypeDesc = self::getChangeTypeDesc('', true);
        return array_filter($changeTypeDesc, function ($key) use ($UMChangeType) {
            return in_array($key, $UMChangeType);
        }, ARRAY_FILTER_USE_KEY);
    }


    /**
     * @notes 获取用户余额变动类型
     * @return int[]
     * @author 段誉
     * @date 2023/2/23 10:08
     */
    public static function getUserMoneyChangeType(): array
    {
        return array_merge(self::UM_DEC, self::UM_INC);
    }

    public static function getUserIntegralChangeTypeDesc()
    {
        $UIChangeType = self::getUserIntegralChangeType();
        $changeTypeDesc = self::getChangeTypeDesc('', true);
        return array_filter($changeTypeDesc, function ($key) use ($UIChangeType) {
            return in_array($key, $UIChangeType);
        }, ARRAY_FILTER_USE_KEY);
    }

    public static function getUserIntegralChangeType(): array
    {
        return array_merge(self::UI_DEC, self::UI_INC);
    }


    /**
     * @notes 获取变动对象
     * @param $changeType
     * @return false
     * @author 段誉
     * @date 2023/2/23 10:10
     */
    public static function getChangeObject($changeType)
    {
        // 用户余额
        $um = self::getUserMoneyChangeType();
        if (in_array($changeType, $um)) {
            return self::UM;
        }

        // 用户积分
        $ui = self::getUserIntegralChangeType();
        if (in_array($changeType, $ui)) {
            return self::UI;
        }

        return false;
    }
}