<?php

namespace app\common\enum\user;

/**
 * 用户加盟申请枚举
 */
class UserRepairEnum
{
    const STATUS_WAIT = 1;
    const STATUS_PASS = 2;
    const STATUS_FAIL = 3;


    /**
     * @notes 状态描述
     * @param bool $from
     * @return string|string[]
     * @date 2022/9/7 15:05
     */
    public static function getStatusDesc($from = true): array|string
    {
        $desc = [
            self::STATUS_WAIT => '待处理',
            self::STATUS_PASS => '已完成',
            self::STATUS_FAIL => '已拒绝',
        ];
        if (true === $from) {
            return $desc;
        }
        return $desc[$from] ?? '';
    }
}