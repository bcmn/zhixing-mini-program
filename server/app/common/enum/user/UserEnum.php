<?php

namespace app\common\enum\user;

/**
 * 用户枚举
 */
class UserEnum
{

    /**
     * 性别
     * SEX_OTHER = 未知
     * SEX_MEN =  男
     * SEX_WOMAN = 女
     */
    const SEX_OTHER = 0;
    const SEX_MEN = 1;
    const SEX_WOMAN = 2;

    const LEVEL_NORMAL = 0;
    const LEVEL_MEMBER = 10;
    const LEVEL_STAFF = 20;
    const LEVEL_PARTNER = 30;
    const LEVEL_STORE = 40;


    /**
     * @notes 性别描述
     * @param bool $from
     * @return string|string[]
     * @author 段誉
     * @date 2022/9/7 15:05
     */
    public static function getSexDesc($from = true)
    {
        $desc = [
            self::SEX_OTHER => '未知',
            self::SEX_MEN => '男',
            self::SEX_WOMAN => '女',
        ];
        if (true === $from) {
            return $desc;
        }
        return $desc[$from] ?? '';
    }

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:00
     * Notes：获取用户等级
     */
    public static function getLevelDesc($value = true)
    {
        $data = [
            self::LEVEL_NORMAL => '普通用户',
            self::LEVEL_MEMBER => '会员',
            self::LEVEL_STAFF => '员工',
            self::LEVEL_PARTNER => '合伙人',
            self::LEVEL_STORE => '门店店主',
        ];
        if ($value === true) {
            return $data;
        }
        return $data[$value];
    }
}