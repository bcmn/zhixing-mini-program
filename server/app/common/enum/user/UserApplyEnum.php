<?php

namespace app\common\enum\user;

/**
 * 用户加盟申请枚举
 */
class UserApplyEnum
{
    const STATUS_WAIT = 1;
    const STATUS_PASS = 2;
    const STATUS_FAIL = 3;

    const TYPE_PARTNER = 30;
    const TYPE_STORE = 40;


    /**
     * @notes 状态描述
     * @param bool $from
     * @return string|string[]
     * @date 2022/9/7 15:05
     */
    public static function getStatusDesc($from = true): array|string
    {
        $desc = [
            self::STATUS_WAIT => '待审核',
            self::STATUS_PASS => '已通过',
            self::STATUS_FAIL => '已拒绝',
        ];
        if (true === $from) {
            return $desc;
        }
        return $desc[$from] ?? '';
    }

    /**
     * @param $from
     * Date: 2024/4/22 21:29
     * Notes: 加盟类型
     */
    public static function getTypeDesc($from = true): array|string
    {
        $desc = [
            self::TYPE_PARTNER => '合伙人',
            self::TYPE_STORE => '门店',
        ];
        if (true === $from) {
            return $desc;
        }
        return $desc[$from] ?? '';
    }
}