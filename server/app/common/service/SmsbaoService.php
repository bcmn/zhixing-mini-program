<?php

namespace app\common\service;


use app\common\enum\notice\SmsEnum;
use app\common\model\notice\SmsLog;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersExpress;
use app\common\model\orders\OrdersGoods;

class SmsbaoService
{
    private static string $error = '';
    private static string $username = 'cnxinyikeji';
    private static string $key = 'c719db21f36044dc87852ad52596f1a3';

    /**
     * Date: 2023/12/26 20:27
     * Notes:获取错误信息
     */
    public static function getError()
    {
        return self::$error;
    }

    /**
     * Date: 2023/12/4 20:24
     * Notes:短信宝短信
     */
    public static function send($mobile, $content, $code = '')
    {
        try {
            $apiUrl = 'https://api.smsbao.com/sms?u=' . self::$username . '&p=' . self::$key . '&m=' . $mobile . '&c=' . $content;
            $result = self::getURL($apiUrl);
            if ($result == '0') {
                SmsLog::create([
                    'scene_id' => SmsEnum::SMSBAO,
                    'mobile' => $mobile,
                    'content' => $content,
                    'code' => $code,
                    'send_status' => SmsEnum::SEND_SUCCESS,
                    'send_time' => time(),
                ]);
                return true;
            }
            file_put_contents('./smsbao.log', json_encode($result) . PHP_EOL, FILE_APPEND);
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
    }


    /**
     * 发起一个post请求到指定接口
     *
     * @param string $api 请求的接口
     * @param array $data post参数
     * @param int $timeout 超时时间
     * @return string 请求结果
     */
    protected static function getURL(string $api, array $data = [], int $timeout = 30)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api);
        // 以返回的形式接收信息
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置为POST方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        // 不验证https证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        // 发送数据
        $response = curl_exec($ch);
        // 不要忘记释放资源
        curl_close($ch);
        return $response;
    }

}