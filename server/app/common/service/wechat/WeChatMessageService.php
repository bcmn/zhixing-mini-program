<?php

namespace app\common\service\wechat;

use app\common\enum\user\UserTerminalEnum;
use app\common\model\user\UserAuth;
use EasyWeChat\OfficialAccount\Application;

/**
 * 微信消息服务层
 * Class WechatMessageService
 * @package app\common\service
 * $wechatMessage = new WeChatMessageService($user, NoticeEnum::OA);
 * $wechatMessage->send([
 * 'template_id' => 'u5ZGt6kvpTmnM6CBglIEbK7rwcslVv62kX1H85w_qUE',
 * 'url' => '',
 * 'data' => [
 * 'first' => ['value' => '您有一份新的需求信息需要审核'],
 * 'keyword1' => ['value' => $params['name']], //真实姓名
 * 'keyword2' => ['value' => '实名认证'],    //需求标题
 * 'keyword3' => ['value' => '实名认证'], //需求类型
 * 'keyword4' => ['value' => date('Y-m-d H:i:s')], //提交时间
 * 'remark' => ['value' => '请尽快登陆后台查看'],
 * ]
 * ]);
 * }
 */
class WeChatMessageService
{
    /** Easychat实例
     * @var null
     */
    protected $app = null;

    protected $config = null;
    protected $openid = null;
    protected $templateId = null;
    protected $notice = null;

    /**
     * @notes 架构方法
     * @param $userId
     * @author Tab
     * @date 2021/8/20 14:21
     */
    public function __construct($userId)
    {
        //公众平台
        $terminal = UserTerminalEnum::WECHAT_MMP;
        $this->config = WeChatConfigService::getOaConfig();
        $this->app = new Application($this->config);

        $userAuth = UserAuth::where([
            'user_id' => $userId,
            'terminal' => $terminal
        ])->findOrEmpty()->toArray();
        $this->openid = $userAuth['openid'];
    }

    /**
     * @notes 发送消息
     * @param $params
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author Tab
     * @date 2021/8/20 16:42
     */
    public function send($params)
    {
        try {
            if (empty($this->openid)) {
                throw new \Exception('openid不存在');
            }

            if ($params['template_id'] == '') {
                throw new \Exception('模板ID不存在');
            } else {
                $this->templateId = $params['template_id'];
            }

            $template = $this->oaTemplate($params);

            $accessToken = $this->app->getAccessToken(); // 使用easywechat自带的方法,获取访问令牌
            $token = $accessToken->getToken(); // string
            $token = $this->checkWXToken($token);
            $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $token;

            $res = $this->app->getClient()->postJson($url, $template);
            return $res->toArray();
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @notes 公众号消息模板
     * @param $params
     * @param $sceneConfig
     * @return array
     * @author Tab
     * @date 2021/8/20 16:46
     */
    public function oaTemplate($params)
    {
        $domain = request()->domain();
        $tpl = [
            'touser' => $this->openid,
            'template_id' => $this->templateId,
            'url' => $domain . $params['url'],
            'data' => $params['data']
        ];
        return $tpl;
    }

    //验证access_token是否有效
    public function checkWXToken($access_token)
    {
        $ipurl = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=" . $access_token;
        $ipresult = $this->app->getClient()->postJson($ipurl);
        $ipdata = json_decode($ipresult, true);
        if (isset($ipdata['errcode']) && $ipdata['errcode'] == '40001') {
            file_put_contents('access_token.txt', date('Y-m-d H:i:s') . ' access_token提前失效，进入二次获取token' . PHP_EOL, FILE_APPEND);
            $accessToken = $this->app->getAccessToken(); // 使用easywechat自带的方法,获取访问令牌
            $access_token = $accessToken->refresh(); // string
        }
        return $access_token;
    }

}