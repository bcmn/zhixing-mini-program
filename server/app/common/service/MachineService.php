<?php

namespace app\common\service;


use app\common\model\goods\GoodsDevices;
use app\common\model\user\User;
use think\facade\Db;

class MachineService
{
    public static $error = '';

    private static string $apiURL = 'https://iot-api.heclouds.com'; // 接口地址
    private static string $apiKEY = '=KZw1NKgTAugkC1GWNRaMR9bWAE='; // 弃用
    private static string $accessKEY = 'wI/VAwPk9RW3HxXcdhDfIC0aUP8x+Pj0TOtqd+jmM+ad7mirmHoNI+6/2uKo1Tn9qmKyvmaSFVSy8h3wSv/1rw=='; // 用户级KEY
    private static int $userId = 275988;    // 用户ID
    private static string $version = '2022-05-01';  // 鉴权版本
    private static string $method = 'sha1'; // 加密方式
    private static string $oneLinkApiURL = 'https://api.iot.10086.cn/v5';      // onelink平台接口地址
    private static string $oneLinkAppId = 'C5010531BIOT2023042009380902851';    // onelink平台APPID
    private static string $oneLinkPass = '*llsuS4TvZVJ';    //  onelink平台密钥

    /**
     * @param $params
     * Date: 2024/4/26 13:50
     * Notes: 处理设备回调
     */
    public static function handleDevices($params): bool
    {
        $data = $params['msg'];
        $imei = $data['imei'] ?? '';
        $ds = explode('_', $data['ds_id']);
        $obj_id = $ds[0] ?? 0;
        $obj_ins_id = $ds[1] ?? 0;
        $mode = $data['type'] ?? 1;
        $apiURL = self::$apiURL . '/nb-iot?imei=' . $imei . '&obj_id=' . $obj_id . '&obj_inst_id=' . $obj_ins_id . '&mode=' . $mode;

        $val = '';
        switch ($data['value']) {
            case 'time':
                //从服务器读取可用剩余租赁时间
                $val = self::convertAscii('time', self::getDevicesTime($imei, 'time'));
                break;
            case 'numbA':
                //从服务器读取A功能可使用次数
                $val = self::convertAscii('numA', self::getDevicesTime($imei, 'A'));
                break;
            case 'numbASUB':
                //从服务器A的功能可能次数减1
                $val = self::convertAscii('numB', self::getDevicesTime($imei, 'A', 'sub'));
                break;
            case 'numbB':
                //从服务器读取B功能可使用次数
                $val = self::convertAscii('numC', self::getDevicesTime($imei, 'B'));
                break;
            case 'numbBSUB':
                //从服务器B的功能可能次数减1
                $val = self::convertAscii('numD', self::getDevicesTime($imei, 'B', 'sub'));
                break;
            case 'NumbC':
                //从服务器读取C功能可使用次数
                $val = self::convertAscii('numE', self::getDevicesTime($imei, 'C'));
                break;
            case 'numbCSUB':
                //从服务器C的功能可能次数减1
                $val = self::convertAscii('numF', self::getDevicesTime($imei, 'C', 'sub'));
                break;
            case 'numbD':
                //从服务器读取D功能可使用次数
                $val = self::convertAscii('numG', self::getDevicesTime($imei, 'D'));
                break;
            case 'NumbDSUB':
                //从服务器D的功能可能次数减1
                $val = self::convertAscii('numH', self::getDevicesTime($imei, 'D', 'sub'));
                break;
            case 'numbE':
                //从服务器读取E功能可使用次数
                $val = self::convertAscii('numI', self::getDevicesTime($imei, 'E'));
                break;
            case 'numbESUB':
                //从服务器E的功能可能次数减1
                $val = self::convertAscii('numJ', self::getDevicesTime($imei, 'E', 'sub'));
                break;
            case 'help':
                //远程求救
                $val = self::convertAscii('HELP', 66666666);
                self::sendMessage($imei);
                break;
            case 'QRcode':
                //向服务器要设备二维码
                $val = self::convertAscii('CODE', '');
                break;
            case 'Realtime':
                //从服务器读取实时实钟
                $val = self::convertAscii('Real', date('ymdHis'));
                break;
            default:
                break;
        }

        $postData = [
            'data' => [
                ['res_id' => $ds[2] ?? 0, 'val' => $val]
            ]
        ];
        $resp = self::postURL($apiURL, json_encode($postData, true), self::getAuthorization());

        Db::name('MachineLogs')->insert([
            'params' => json_encode($params, true),
            'content' => json_encode([
                'imei' => $imei,
                'obj_id' => $obj_id,
                'obj_inst_id' => $obj_ins_id,
                'mode' => $mode,
                'data' => $postData['data']
            ], true),
            'response' => $resp,
            'create_time' => time()
        ]);
        return true;
    }

    /**
     * @param $sn
     * 获取设备的剩余使用时间/次数
     */
    protected static function getDevicesTime($sn, $type, $action = ''): string
    {
        $device = GoodsDevices::where('sn', '=', $sn)->findOrEmpty();
        if ($device->isEmpty()) {
            return 0;
        }

        if ($type == 'time') {
            //获取时间
            $time = max(0, $device['end_time'] - time());
            return round($time / 60, 0);
        } else {
            // 获取各功能剩余次数或时长(单位分钟)
            $num = $device[$type . '_num'];
            if ($action == 'sub') {
                // 次数减1或者时长减1分钟
                GoodsDevices::where('id', '=', $device['id'])->dec($type . '_num', 1)->save();
                $num--;
            }
            return max(0, $num);
        }
    }

    /**
     * @param $params
     * Date: 2024/4/26 14:04
     * Notes: 创建平台设备
     */
    public static function createDevices($params): bool|array
    {
        $postData = [
            'product_id' => $params['product_id'],
            'device_name' => $params['device_name'],
            'imei' => $params['imei'],
            'imsi' => $params['imsi'],
        ];
        $authorization = self::getAuthorization();
        $resp = self::postURL(self::$apiURL . '/device/create', json_encode($postData, true), $authorization);
        $resp = json_decode($resp, true);
        if ($resp['code'] == 0) {
            return true;
        }
        self::$error = $resp['msg'];
        return false;
    }

    /**
     * @param $params
     * Date: 2024/4/26 14:04
     * Notes: 删除平台设备
     */
    public static function deleteDevices($params): bool|array
    {
        $postData = [
            'imei' => $params['imei'],
        ];
        $authorization = self::getAuthorization();
        $resp = self::postURL(self::$apiURL . '/device/delete', json_encode($postData, true), $authorization);
        $resp = json_decode($resp, true);
        if ($resp['code'] == 0) {
            return true;
        }
        self::$error = $resp['msg'];
        return false;
    }


    /**
     * Date: 2024/5/4 14:48
     * Notes: 获取新版鉴权字符串
     */
    protected static function getAuthorization()
    {
        $timeout = time() + 24 * 60 * 60;
        $stringForSignature = $timeout . PHP_EOL . self::$method . PHP_EOL . "userid/" . self::$userId . PHP_EOL . self::$version;
        $sign = base64_encode(hash_hmac(self::$method, $stringForSignature, base64_decode(self::$accessKEY), true));
        return "version=" . self::$version . "&res=" . urlencode("userid/" . self::$userId) . "&et=" . $timeout . "&method=" . self::$method . "&sign=" . urlencode($sign);
    }

    /**
     * @param $string
     * @param $number
     * Date: 2024/4/26 13:51
     * Notes: 转换为十六进制ASCII码
     * @return string
     */
    protected static function convertAscii($string, $number): string
    {
        $str = '';
        if (!empty($string)) {
            for ($i = 0; $i < strlen($string); $i++) {
                $str .= dechex(ord($string[$i]));
            }
        }

        $num = '';
        if (!empty($number)) {
            $length = strlen($number);
            if ($length < 9) {
                $number = str_pad($number, 8, "0", STR_PAD_LEFT);
            }
            for ($i = 0; $i < strlen($number); $i++) {
                $num .= dechex(ord($number[$i]));
            }
        }

        return strtoupper($str . $num);
    }

    /**
     * =======================================================================================================================================================
     * =======================================================================================================================================================
     * ========================================================       oneLink 平台          ===================================================================
     * =======================================================================================================================================================
     * =======================================================================================================================================================
     */

    /**
     * Date: 2023/11/25 23:34
     * Notes:发送求救信息
     */
    public static function sendMessage($sn): bool|string
    {
        $devices = GoodsDevices::where('sn', '=', $sn)->findOrEmpty();
        if (!$devices->isEmpty() && !empty($devices['imsi'])) {
            $resp = self::getOneLinkPosition($devices['imsi']);
            if ($resp['status'] == 0) {
                $data = $resp['result'];
                if ($data[0]['status'] == 0) {
                    GoodsDevices::where('sn', '=', $sn)
                        ->update([
                            'lng' => $data[0]['lastLon'],
                            'lat' => $data[0]['lastLat']
                        ]);

                    //逆地理解析
                    $mapUrl = 'https://apis.map.qq.com/ws/geocoder/v1/?key=EABBZ-74X67-7GCXR-PNBQQ-22CWV-FRBVH&get_poi=1&poi_options=address_format=short;policy=4&location=' . $data[0]['lastLat'] . ',' . $data[0]['lastLon'];
                    $result = self::postURL($mapUrl, '', '', 60, 'get');
                    $address = '平台定位出错';
                    if ($result) {
                        $res = json_decode($result, true);
                        if ($res['status'] == 0) {
                            $address = $res['result']['address'];
                        } else {
                            return $result;
                        }
                    }
                    //发送短信
                    $userInfo = User::where('id', $devices['user_id'])->findOrEmpty();
                    if (!$userInfo->isEmpty()) {
                        $mobile = empty($userInfo['urgent_phone']) ? $userInfo['mobile'] : $userInfo['urgent_phone'];
                        SmsbaoService::send($mobile, '设备使用人向您发布求助信息，所在位置：' . $address . '，请打开地图搜索位置。');
                    }
                    return '请求成功';
                }
                return json_encode($data);
            }
            return json_encode($resp);
        }
        return '查询不到设备信息';
    }

    /**
     * @param $imsi
     * Date: 2023/11/17 23:13
     * Notes:获取物联卡最后一次定位位置
     * {"status":"0","message":"正确","result":[{"lastTime":"2023-11-12 11:16:31","lastLon":"118.43365","lastLat":"34.97895","imsi":"460048857111164","message":"正确","status":"0"}]}
     * @return string|array
     * @throws \Exception
     */
    protected static function getOneLinkPosition($imsi): string|array
    {
        $tokenResp = self::getOneLinkToken();
        $tokenResp = json_decode($tokenResp, true);
        if ($tokenResp['status'] == 0) {
            $token = $tokenResp['result'][0]['token'];
            $postData = [
                'token' => $token,
                'transid' => self::$oneLinkAppId . date('YmdHis') . random_int(10000000, 99999999),
                'imsis' => $imsi
            ];

            $resp = self::postURL(self::$oneLinkApiURL . '/ec/query/last-position-location/batch', json_encode($postData, true));
            return json_decode($resp, true);
        } else {
            return $tokenResp['message'];
        }
    }


    /**
     * Date: 2023/11/17 22:46
     * Notes:获取oneLink平台TOKEN
     */
    protected static function getOneLinkToken(): string
    {
        $postData = [
            'appid' => self::$oneLinkAppId,
            'password' => self::$oneLinkPass,
            'transid' => self::$oneLinkAppId . date('YmdHis') . random_int(10000000, 99999999)
        ];
        $resp = self::postURL(self::$oneLinkApiURL . '/ec/get/token', json_encode($postData, true));
        return $resp;
    }



    /**
     * =======================================================================================================================================================
     * =======================================================================================================================================================
     * ========================================================        公共请求方法          ===================================================================
     * =======================================================================================================================================================
     * =======================================================================================================================================================
     */


    /**
     * 发起一个post请求到指定接口
     *
     * @param string $api 请求的接口
     * @param string $data post参数
     * @param int $timeout 超时时间
     * @return string 请求结果
     */
    protected static function postURL(string $api, string $data = '', string $authorization = '', int $timeout = 30, string $method = 'post'): string
    {
        $header = [
            'Accept: application/json, text/plain, */*',
            'Content-Type: application/json;charset=UTF-8'
        ];
        if (!empty($authorization)) {
            $header[] = 'authorization:' . $authorization;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api);
        // 以返回的形式接收信息
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置为POST方式
        if ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        // 不验证https证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        // 发送数据
        $response = curl_exec($ch);
        // 不要忘记释放资源
        curl_close($ch);
        return $response;
    }

}