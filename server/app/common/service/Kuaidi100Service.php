<?php

namespace app\common\service;


use app\common\model\orders\Orders;
use app\common\model\orders\OrdersExpress;
use app\common\model\orders\OrdersExpressLogs;
use app\common\model\orders\OrdersGoods;

class Kuaidi100Service
{
    public static $error = '';
    private static $key = 'BQMUVvZf2450';
    private static $customer = '898DA0072CD2B8CA3429140DD96CFB1A';
    private static $secret = '2a0a7de30f9a4ecfb0a39f39d882d0f8';
    private static $tempid = 'e2582cef4c884963bd63d45b355d63e4';
    private static $ssid = 'KX100LAF0DB1FD4C9';
    private static $shunfeng = ['code' => 'shunfeng', 'sn' => '5391864430', 'partnerKey' => 'ZXYLKIWGS_KD100', 'partnerSecret' => 'oWhltb2e8m7AHicT6NzO2BXEqUyrgh1L', 'tmpId' => 'e2582cef4c884963bd63d45b355d63e4'];


    /**
     * Date: 2024/1/19 10:32
     * Notes:电子面单发货
     */
    public static function delivery($orderInfo)
    {
        $orderGoods = OrdersGoods::where('order_id', $orderInfo['id'])->column('goods_name');
        $params = json_encode([
            "partnerId" => self::$shunfeng['sn'],
            "partnerKey" => self::$shunfeng['partnerKey'],
            "partnerSecret" => self::$shunfeng['partnerSecret'],
            "code" => "sf_secret",
            "kuaidicom" => self::$shunfeng['code'],
            "recMan" => [
                "name" => $orderInfo['name'],
                "mobile" => $orderInfo['phone'],
                "printAddr" => $orderInfo['address'],
                "company" => ""
            ],
            "sendMan" => [
                "name" => ConfigService::get('website', 'delivery_name', ''),
                "mobile" => ConfigService::get('website', 'delivery_mobile', ''),
                "printAddr" => ConfigService::get('website', 'delivery_address', ''),
                "company" => ""
            ],
            "cargo" => implode(',', $orderGoods),
            "tempId" => self::$shunfeng['tmpId'],
            "childTempId" => "",
            "backTempId" => "",
            "payType" => "SHIPPER",
            "expType" => "顺丰标快",
            "remark" => $orderInfo['sn'],
            "collection" => "0",
            "needChild" => "0",
            "needBack" => "0",
            "count" => 1,
            "printType" => "CLOUD",
            'direction' => '0',
            "siid" => self::$ssid,
            "needDesensitization" => true,
            "needLogo" => false,
            "needOcr" => false
        ], JSON_UNESCAPED_UNICODE);

        file_put_contents('./logs/kuaidi100.txt', json_encode($params) . "\n", FILE_APPEND);

        list($msec, $sec) = explode(' ', microtime());
        $t = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);    // 当前时间戳

        $sign = md5($params . $t . self::$key . self::$secret);

        $apiData = [
            'method' => 'order',
            'key' => self::$key,
            'sign' => strtoupper($sign),
            't' => $t,
            'param' => $params
        ];
        $result = self::postURL('https://api.kuaidi100.com/label/order', $apiData);
        $result = json_decode($result, true);
        if ($result['code'] == 200) {
            return $result['data'];
        } else {
            file_put_contents('./logs/kuaidi100.txt', json_encode($result) . "\n", FILE_APPEND);
            self::$error = $result['message'];
            return false;
        }
    }

    /**
     * Date: 2023/12/4 20:24
     * Notes:查询物流轨迹
     */
    public static function expresss($order_id, $express)
    {
        $orderInfo = Orders::where('id', '=', $order_id)->findOrEmpty();
        $expressInfo = OrdersExpress::where('id', '=', $orderInfo['express_company'])->findOrEmpty();
        if (!$expressInfo->isEmpty()) {
            $param = [
                'com' => $expressInfo['code'],
                'num' => $orderInfo['express_sn'],
                'phone' => $orderInfo['phone'],
            ];

            //请求参数
            $post_data = array();
            $post_data['param'] = json_encode($param, JSON_UNESCAPED_UNICODE);
            $post_data['customer'] = self::$customer;
            $sign = md5($post_data['param'] . self::$key . self::$customer);
            $post_data['sign'] = strtoupper($sign);

            $result = self::postURL('https://poll.kuaidi100.com/poll/query.do', $post_data);

            if (!empty($express)) {
                OrdersExpressLogs::update([
                    'id' => $express['id'],
                    'update_time' => time(),
                    'content' => $result
                ]);
            } else {
                OrdersExpressLogs::create([
                    'order_id' => $orderInfo['id'],
                    'sn' => $orderInfo['express_sn'],
                    'company' => $orderInfo['express_company'],
                    'content' => $result,
                ]);
            }

            return json_decode($result, true);
        } else {
            return [];
        }
    }

    public static function print($order_id)
    {
        $orderInfo = Orders::where('id', '=', $order_id)->findOrEmpty();
        $tempid = OrdersExpress::where('id', '=', $orderInfo['express_company'])->value('tempid');
        $tempid = empty($tempid) ? self::$tempid : $tempid;
        if (!$orderInfo->isEmpty()) {
            list($msec, $sec) = explode(' ', microtime());
            $t = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);    // 当前时间戳
            $param = array(
                'tempid' => $tempid,                    // 电子面单模板编码，通过后台模板管理页面获取：https://api.kuaidi100.com/manager/page/template/eletemplate
                'printType' => 'CLOUD',                 // 打印类型（IMAGE,CLOUD,HTML）。IMAGE:生成图片短链；HTML:生成html短链；CLOUD:使用快递100云打印机打印
                'siid' => self::$ssid,                      // 设备编码
                'direction' => '0',                // 打印方向，0：正方向（默认）； 1：反方向；只有printType为CLOUD时该参数生效
                'callBackUrl' => 'http://zx.linruanwangluo.com/callback.php',               // 打印状态回调地址，默认仅支持http
                'customParam' => [
                    'kuaidinum' => $orderInfo['express_sn'],
                    'recname' => $orderInfo['name'],
                    'recmobile' => $orderInfo['phone'],
                    'recaddr2' => $orderInfo['address'],
                    'sendname' => ConfigService::get('website', 'delivery_name', ''),
                    'sendmobile' => ConfigService::get('website', 'delivery_mobile', ''),
                    'sendaddr2' => ConfigService::get('website', 'delivery_address', ''),
                    'cargo' => OrdersGoods::where('order_id', '=', $order_id)->column('goods_name'),
                    'pkgcount' => OrdersGoods::where('order_id', '=', $order_id)->sum('num'),
                ]
            );

            //请求参数
            $post_data = array();
            $post_data['param'] = json_encode($param, JSON_UNESCAPED_UNICODE);
            $post_data['key'] = self::$key;
            $post_data['t'] = $t;
            $sign = md5($post_data['param'] . $t . self::$key . self::$secret);
            $post_data['sign'] = strtoupper($sign);
            self::postURL('https://api.kuaidi100.com/label/order?method=custom', $post_data);
        }
    }


    /**
     * 发起一个post请求到指定接口
     *
     * @param string $api 请求的接口
     * @param string $data post参数
     * @param int $timeout 超时时间
     * @return string 请求结果
     */
    protected static function postURL(string $api, array $data = [], int $timeout = 30)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api);
        // 以返回的形式接收信息
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置为POST方式
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        // 不验证https证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        // 发送数据
        $response = curl_exec($ch);
        // 不要忘记释放资源
        curl_close($ch);
        return $response;
    }

}