<?php

namespace app\common\model\coupon;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use app\common\model\orders\Orders;
use think\model\concern\SoftDelete;

/**
 * 优惠券领取列表
 */
class CouponRecord extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return $value && $value == 2 ? '已使用' : '未使用';
    }

    public function getOrderSnAttr($value)
    {
        return $value ? Orders::where('id', '=', $value)->value('sn') : '-';
    }

}