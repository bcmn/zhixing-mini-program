<?php

namespace app\common\model\coupon;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 优惠券
 */
class Coupon extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return DefaultEnum::getShowDesc($value);
    }

    /**
     * @param $value
     * Date: 2023/10/25 22:23
     * Notes:优惠券领取数量
     */
    public function getGetNumAttr($value)
    {
        return empty($value) ? 0 : CouponRecord::where('coupon_id', '=', $value)->count();
    }

}