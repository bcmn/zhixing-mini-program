<?php

namespace app\common\model\advertise;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 广告内容
 * Class Advertise
 */
class Advertise extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value)
    {
        return DefaultEnum::getShowDesc($value);
    }

    public function getCateTitleAttr($value)
    {
        return $value ? AdvertiseCate::where('id', $value)->value('title') : '';
    }

    public function setUrlTypeAttr($value)
    {
        return $value ? json_encode($value, true) : '';
    }

    public function getUrlTypeAttr($value)
    {
        return $value ? json_decode($value, true) : '';
    }
}