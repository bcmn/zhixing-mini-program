<?php

namespace app\common\model\user;

use app\common\model\BaseModel;
use app\common\model\goods\Goods;

/**
 * 用户收藏信息模型
 */
class UserFavorite extends BaseModel
{

    /**
     * Date: 2023/10/23 23:17
     * Notes:获取商品信息
     */
    public function goods()
    {
        return $this->hasOne(Goods::class, 'id', 'goods_id')
            ->field('id,name,image,price,stock');
    }
}