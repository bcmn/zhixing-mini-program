<?php

namespace app\common\model\user;

use app\common\model\BaseModel;
use app\common\service\FileService;

/**
 * 用户反馈模型
 */
class UserFeedback extends BaseModel
{

    /**
     * Date: 2023/11/3 0:21
     * Notes:用户信息
     */
    public function userInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param $value
     * Date: 2023/10/15 19:39
     * Notes:设置图册
     */
    public function setImagesAttr($value): bool|string
    {
        $arr = [];
        if ($value) {
            foreach ($value as $key => $item) {
                $arr[$key] = FileService::setFileUrl($item);
            }
        }
        return json_encode($arr, true);
    }

    /**
     * @param $value
     * Date: 2023/10/15 19:41
     * Notes:获取图册
     */
    public function getImagesAttr($value): array
    {
        $arr = [];
        if ($value) {
            $value = json_decode($value, true);
            foreach ($value as $key => $item) {
                $arr[$key] = FileService::getFileUrl($item);
            }
        }
        return $arr;
    }
}