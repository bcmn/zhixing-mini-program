<?php

namespace app\common\model\user;

use app\common\enum\user\UserApplyEnum;
use app\common\model\BaseModel;

/**
 * 用户加盟申请模型
 */
class UserApply extends BaseModel
{

    /**
     * @param $value
     * Date: 2024/4/22 21:30
     * Notes: 加盟状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return $value ? UserApplyEnum::getStatusDesc($value) : '';
    }

    /**
     * @param $value
     * Date: 2024/4/22 21:29
     * Notes: 加盟类型描述
     */
    public function getTypeTextAttr($value)
    {
        return $value ? UserApplyEnum::getTypeDesc($value) : '';
    }

    /**
     * @param $value
     * Date: 2024/4/22 21:31
     * Notes: 获取申请用户姓名
     */
    public function getUserNameAttr($value)
    {
        return $value ? User::where('id', $value)->value('nickname') : '';
    }
}