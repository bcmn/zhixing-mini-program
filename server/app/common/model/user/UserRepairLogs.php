<?php

namespace app\common\model\user;

use app\common\model\BaseModel;
use app\common\service\FileService;

/**
 * 申请维修模型
 */
class UserRepairLogs extends BaseModel
{

    /**
     * @param $value
     * @Time: 2023/4/13 10:55
     * @Desc:设置写入图片方法
     */
    public function setImagesAttr($value): bool|string
    {
        $images = [];
        if (is_array($value) && !empty($value)) {
            foreach ($value as $key => $item) {
                $images[$key] = trim($item) ? FileService::setFileUrl($item) : '';
            }
        }
        return json_encode($images, true);
    }

    /**
     * @param $value
     * @return false|string[]
     * @Time: 2023/4/13 8:52
     * @Desc:解析图片
     */
    public function getImagesAttr($value): array|bool
    {
        $images = [];
        if (!empty($value)) {
            $value_arr = json_decode($value, true);
            foreach ($value_arr as $key => $item) {
                $images[$key] = trim($item) ? FileService::getFileUrl($item) : '';
            }
        }
        return $images;
    }
}