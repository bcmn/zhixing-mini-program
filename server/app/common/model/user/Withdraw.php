<?php

namespace app\common\model\user;

use app\common\enum\UserEnum;
use app\common\enum\WithdrawEnum;
use app\common\model\BaseModel;
use app\common\service\FileService;
use think\model\concern\SoftDelete;

/**
 * 充值
 * Class Withdraw
 */
class Withdraw extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value)
    {
        return WithdrawEnum::getStausDesc($value);
    }

}