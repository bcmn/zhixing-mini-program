<?php

namespace app\common\model\business;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use app\common\service\FileService;
use think\model\concern\SoftDelete;

/**
 * 医院
 * Class Hospital
 */
class Hospital extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value)
    {
        return DefaultEnum::getShowDesc($value);
    }

    public function setImagesAttr($value): bool|string
    {
        $data = [];
        if ($value) {
            if (!is_array($value)) {
                $value = explode(',', $value);
            }
            foreach ($value as $key => $item) {
                $data[$key] = FileService::setFileUrl($item);
            }
        }
        return json_encode($data, true);
    }

    public function getImagesAttr($value): array
    {
        $data = [];
        if ($value) {
            $arr = json_decode($value, true);
            foreach ($arr as $key => $item) {
                $data[$key] = FileService::getFileUrl($item);
            }
        }
        return $data;
    }
}