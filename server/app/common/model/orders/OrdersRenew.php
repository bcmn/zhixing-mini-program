<?php

namespace app\common\model\orders;

use app\common\enum\orders\OrdersRenewEnum;
use app\common\model\BaseModel;
use app\common\model\goods\GoodsDevices;
use app\common\model\user\User;

/**
 * 购买次数或时长订单
 */
class OrdersRenew extends BaseModel
{

    /**
     * Date: 2023/10/21 9:55
     * Notes:用户信息
     */
    public function userInfo(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Date: 2023/10/21 11:04
     * Notes:订单商品信息
     */
    public function devices(): \think\model\relation\HasOne
    {
        return $this->hasOne(GoodsDevices::class, 'id', 'devices_id');
    }

    /**
     * @param $value
     * Date: 2023/10/21 9:59
     * Notes:获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return $value ? OrdersRenewEnum::getStatusDesc($value) : '';
    }

}