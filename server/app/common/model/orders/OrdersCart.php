<?php

namespace app\common\model\orders;

use app\api\logic\goods\GoodsLogic;
use app\common\model\BaseModel;
use app\common\model\goods\GoodsSku;

/**
 * 购物车
 */
class OrdersCart extends BaseModel
{

    public function getGoodsInfoAttr($value)
    {
        return $value ? GoodsLogic::detail($value, 0) : '';
    }

    public function getSkuInfoAttr($value)
    {
        return $value ? GoodsSku::where('id', '=', $value)->findOrEmpty() : [];
    }
}