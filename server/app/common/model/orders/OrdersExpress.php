<?php

namespace app\common\model\orders;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 快递公司模型
 */
class OrdersExpress extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value)
    {
        return DefaultEnum::getShowDesc($value);
    }

}