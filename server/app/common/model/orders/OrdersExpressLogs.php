<?php

namespace app\common\model\orders;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 快递查询记录模型
 */
class OrdersExpressLogs extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

}