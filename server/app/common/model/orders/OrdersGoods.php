<?php

namespace app\common\model\orders;

use app\common\enum\RefundEnum;
use app\common\model\BaseModel;
use app\common\model\refund\RefundRecord;
use app\common\service\FileService;

/**
 * 订单商品
 */
class OrdersGoods extends BaseModel
{
    public function getGoodsImageAttr($value): string
    {
        return $value ? FileService::getFileUrl($value) : '';
    }

    public function getSkuImageAttr($value): string
    {
        return $value ? FileService::getFileUrl($value) : '';
    }

    public function getServicesStatusAttr($value)
    {
        return $value ? (RefundRecord::where('order_goods_id', '=', $value)->count() ? 1 : 0) : 0;
    }

    public function getRefundStatusAttr($value)
    {
        return $value ? RefundRecord::where('order_goods_id', '=', $value)->order('id', 'desc')->value('refund_status') : 0;
    }

    public function getRefundStatusTextAttr($value)
    {
        $text = '';
        if ($value) {
            $status = RefundRecord::where('order_goods_id', '=', $value)->order('id', 'desc')->value('refund_status');
            $status = $status ?? 0;
            $text = RefundEnum::getStatusDesc($status);
            $reason = RefundRecord::where('order_goods_id', '=', $value)->order('id', 'desc')->value('reason');
            if ($reason) {
                $text .= '：' . $reason;
            }
        }
        return $text;
    }

    public function orderInfo()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'id');
    }
}