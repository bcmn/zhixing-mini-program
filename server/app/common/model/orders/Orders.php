<?php

namespace app\common\model\orders;

use app\common\enum\orders\OrdersEnum;
use app\common\model\BaseModel;
use app\common\model\user\User;
use think\model\concern\SoftDelete;

/**
 * 订单
 */
class Orders extends BaseModel
{
    use SoftDelete;

    /**
     * Date: 2023/10/21 9:55
     * Notes:用户信息
     */
    public function userInfo(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Date: 2023/10/21 11:04
     * Notes:订单商品信息
     */
    public function goods(): \think\model\relation\HasMany
    {
        return $this->hasMany(OrdersGoods::class, 'order_id', 'id')
            ->field('id,order_id,goods_id,goods_type,sku_id,goods_type,lease_type,goods_name,goods_image,sku_name,sku_image,price,num,security_amount,total_price,id as services_status,id as refund_status,id as refund_status_text');
    }

    public function express(): \think\model\relation\HasOne
    {
        return $this->hasOne(OrdersExpress::class, 'id', 'express_company')
            ->field('id,title,code,notes');
    }

    /**
     * @param $value
     * Date: 2023/10/21 9:59
     * Notes:获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return $value ? OrdersEnum::getStatusDesc($value) : '';
    }

}