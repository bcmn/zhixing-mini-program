<?php

namespace app\common\model\orders;

use app\common\enum\orders\OrdersRenewEnum;
use app\common\model\BaseModel;
use app\common\model\goods\Goods;
use app\common\model\goods\GoodsDevices;
use app\common\model\user\User;

/**
 * 设备预约订单
 */
class OrdersLearn extends BaseModel
{

    /**
     * Date: 2023/10/21 9:55
     * Notes:用户信息
     */
    public function userInfo(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Date: 2023/10/21 11:04
     * Notes:订单商品信息
     */
    public function devices(): \think\model\relation\belongsTo
    {
        return $this->belongsTo(GoodsDevices::class, 'devices_id', 'id');
    }

    public function goodsInfo(): \think\model\relation\belongsTo
    {
        return $this->belongsTo(Goods::class, 'goods_id', 'id');
    }

    /**
     * @param $value
     * Date: 2023/10/21 9:59
     * Notes:获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return $value ? OrdersRenewEnum::getStatusDesc($value) : '';
    }

    public function getAppointTimeAttr($value): string
    {
        return $value ? date('Y-m-d H:i:s', $value) : '';
    }

}