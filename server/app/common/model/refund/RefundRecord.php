<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\common\model\refund;


use app\common\enum\RefundEnum;
use app\common\model\BaseModel;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\user\User;
use app\common\service\FileService;


/**
 * 退款记录模型
 * Class RefundRecord
 * @package app\common\model\refund
 */
class RefundRecord extends BaseModel
{

    /**
     * @notes 退款类型描述
     * @param $value
     * @param $data
     * @return string|string[]
     * @author 段誉
     * @date 2022/12/1 10:41
     */
    public function getRefundTypeTextAttr($value, $data): array|string
    {
        return RefundEnum::getTypeDesc($data['refund_type']);
    }


    /**
     * @notes 退款状态描述
     * @param $value
     * @param $data
     * @return string|string[]
     * @date 2022/12/1 10:44
     */
    public function getRefundStatusTextAttr($value, $data): array|string
    {
        return RefundEnum::getStatusDesc($data['refund_status']);
    }

    /**
     * Date: 2023/10/21 9:55
     * Notes:用户信息
     */
    public function userInfo(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Date: 2023/10/21 11:04
     * Notes:订单商品信息
     */
    public function goodsInfo(): \think\model\relation\HasOne
    {
        return $this->hasOne(OrdersGoods::class, 'id', 'order_goods_id')
            ->field('id,order_id,goods_id,goods_type,sku_id,goods_type,lease_type,goods_name,goods_image,sku_name,sku_image,price,num,security_amount,total_price,id as services_status');
    }

    /**
     * Date: 2023/10/21 11:04
     * Notes:订单商品信息
     */
    public function orderInfo(): \think\model\relation\HasOne
    {
        return $this->hasOne(Orders::class, 'id', 'order_id')
            ->field('*');
    }

    /**
     * @notes 退款方式描述
     * @param $value
     * @param $data
     * @return string|string[]
     * @date 2022/12/6 11:08
     */
    public function getRefundWayTextAttr($value, $data): array|string
    {
        return RefundEnum::getWayDesc($data['refund_way']);
    }

    /**
     * @param $value
     * @Time: 2023/4/13 10:55
     * @Desc:设置写入图片方法
     */
    public function setImagesAttr($value): bool|string
    {
        $value = explode(',', $value);
        $images = [];
        if (is_array($value) && !empty($value)) {
            foreach ($value as $key => $item) {
                $images[$key] = trim($item) ? FileService::setFileUrl($item) : '';
            }
        }
        return json_encode($images, true);
    }

    /**
     * @param $value
     * @return false|string[]
     * @Time: 2023/4/13 8:52
     * @Desc:解析图片
     */
    public function getImagesAttr($value): array|bool
    {
        $images = [];
        if (!empty($value)) {
            $value_arr = json_decode($value, true);
            foreach ($value_arr as $key => $item) {
                $images[$key] = trim($item) ? FileService::getFileUrl($item) : '';
            }
        }
        return $images;
    }
}
