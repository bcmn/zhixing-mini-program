<?php

namespace app\common\model\goods;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use think\model\concern\SoftDelete;

/**
 * 商品分类
 */
class GoodsCate extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return DefaultEnum::getShowDesc($value);
    }

}