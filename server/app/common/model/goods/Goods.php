<?php

namespace app\common\model\goods;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use app\common\service\FileService;
use think\model\concern\SoftDelete;

/**
 * 商品
 */
class Goods extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return DefaultEnum::getShowDesc($value);
    }

    /**
     * @param $value
     * Date: 2023/10/15 21:53
     * Notes:获取分类名称
     */
    public function getCateAttr($value)
    {
        return $value ? GoodsCate::where('id', '=', $value)->value('title') : '';
    }

    /**
     * @param $value
     * Date: 2023/10/15 19:39
     * Notes:设置图册
     */
    public function setImagesAttr($value): bool|string
    {
        $arr = [];
        if ($value) {
            foreach ($value as $key => $item) {
                $arr[$key] = FileService::setFileUrl($item);
            }
        }
        return json_encode($arr, true);
    }

    /**
     * @param $value
     * Date: 2023/10/15 19:41
     * Notes:获取图册
     */
    public function getImagesAttr($value): array
    {
        $arr = [];
        if ($value) {
            $value = json_decode($value, true);
            foreach ($value as $key => $item) {
                $arr[$key] = FileService::getFileUrl($item);
            }
        }
        return $arr;
    }

}