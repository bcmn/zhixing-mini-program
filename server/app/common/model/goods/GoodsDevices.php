<?php

namespace app\common\model\goods;

use app\common\enum\DefaultEnum;
use app\common\model\BaseModel;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\user\User;
use app\common\service\FileService;
use think\model\concern\SoftDelete;

/**
 * 设备
 */
class GoodsDevices extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * @param $value
     * @return string|string[]
     * Date: 2023/9/9 9:39
     * Notes：获取状态描述
     */
    public function getStatusTextAttr($value): array|string
    {
        return DefaultEnum::getShowDesc($value);
    }

    public function getGoodsImageAttr($value)
    {
        return $value ? FileService::getFileUrl($value) : '';
    }

//    public function getUserInfoAttr($value)
//    {
//        $userInfo = User::where('id', '=', $value)->findOrEmpty();
//        if ($userInfo) {
//            return [
//                'name' => $userInfo['nickname'],
//                'mobile' => $userInfo['mobile']
//            ];
//        } else {
//            return [
//                'name' => '',
//                'mobile' => ''
//            ];
//        }
//    }

    /**
     * Date: 2023/10/21 15:44
     * Notes: 用户信息
     */
    public function userInfo()
    {
        $userInfo = $this->belongsTo(User::class, 'user_id', 'id')
            ->field('id,avatar,nickname,mobile');
        return $userInfo ?? [];
    }

    public function goodsInfo()
    {
        return $this->belongsTo(Goods::class, 'goods_id', 'id')
            ->field('id,name,cate_id,cate_id as cate,image,images,type,desc,lease_type,learn_price,security_amount,price,sales,stock,content,notes');
    }

    public function orderGoodsInfo()
    {
        return $this->belongsTo(OrdersGoods::class, 'order_goods_id', 'id')
            ->field('id,order_id,goods_id,goods_type,sku_id,goods_type,lease_type,goods_name,goods_image,sku_name,sku_image,price,num,security_amount,total_price,id as services_status,create_time');
    }

    public function ordersInfo()
    {
        return $this->belongsTo(Orders::class, 'order_sn', 'sn')
            ->field('*');
    }
}