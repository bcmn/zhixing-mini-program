<?php

namespace app\common\model\goods;

use app\common\model\BaseModel;

/**
 * 商品规格
 */
class GoodsAttribute extends BaseModel
{

    /**
     * @param $value
     * Date: 2023/10/15 21:02
     * Notes:设置写入规格项
     */
    public function setValuesAttr($value)
    {
        return $value ? json_encode($value, true) : '';
    }

    /**
     * @param $value
     * Date: 2023/10/15 21:02
     * Notes:设置读取规格项
     */
    public function getValuesAttr($value)
    {
        return $value ? json_decode($value, true) : [];
    }
}