<?php

namespace app\common\model\goods;

use app\common\model\BaseModel;
use app\common\model\user\User;
use think\model\concern\SoftDelete;

/**
 * 设备转让记录
 */
class GoodsDevicesTransfer extends BaseModel
{
    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * Date: 2023/10/21 15:44
     * Notes: 转出用户信息
     */
    public function userInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')
            ->field('id,avatar,nickname,mobile');
    }

    /**
     * Date: 2023/10/21 15:44
     * Notes: 接收用户信息
     */
    public function receiveInfo()
    {
        return $this->belongsTo(User::class, 'receive_id', 'id')
            ->field('id,avatar,nickname,mobile');
    }

}