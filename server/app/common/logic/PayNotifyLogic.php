<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\common\logic;

use app\api\logic\orders\OrdersLogic;
use app\common\enum\orders\OrdersEnum;
use app\common\enum\orders\OrdersRentEnum;
use app\common\enum\PayEnum;
use app\common\enum\user\AccountLogEnum;
use app\common\model\goods\GoodsDevices;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersLearn;
use app\common\model\orders\OrdersRenew;
use app\common\model\orders\OrdersRent;
use app\common\model\recharge\RechargeOrder;
use app\common\model\user\User;
use think\facade\Db;
use think\facade\Log;

/**
 * 支付成功后处理订单状态
 * Class PayNotifyLogic
 * @package app\api\logic
 */
class PayNotifyLogic extends BaseLogic
{

    public static function handle($action, $orderSn, $extra = [])
    {
        Db::startTrans();
        try {
            self::$action($orderSn, $extra);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
            Log::write(implode('-', [
                __CLASS__,
                __FUNCTION__,
                $e->getFile(),
                $e->getLine(),
                $e->getMessage()
            ]));
            self::setError($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * @notes 充值回调
     * @param $orderSn
     * @param array $extra
     * @author 段誉
     * @date 2023/2/27 15:28
     */
    public static function recharge($orderSn, $extra = [])
    {
        $order = RechargeOrder::where('sn', $orderSn)->findOrEmpty();
        // 增加用户累计充值金额及用户余额
        $user = User::findOrEmpty($order->user_id);
        $user->total_recharge_amount += $order->order_amount;
        $user->user_money += $order->order_amount;
        $user->save();

        // 记录账户流水
        AccountLogLogic::add(
            $order->user_id,
            AccountLogEnum::UM_INC_RECHARGE,
            AccountLogEnum::INC,
            $order->order_amount,
            $order->sn,
            '用户充值'
        );

        // 更新充值订单状态
        $order->transaction_id = $extra['transaction_id'] ?? '';
        $order->pay_status = PayEnum::ISPAID;
        $order->pay_time = time();
        $order->save();
    }

    /**
     * @param $orderSn
     * @param $extra
     * @return void
     * 订单回调
     */
    public static function orders($orderSn, $extra = [])
    {
        $order = Orders::where('sn', $orderSn)->findOrEmpty();

        // 更新订单状态
        $order->transaction_id = $extra['transaction_id'] ?? '';
        $order->pay_status = PayEnum::ISPAID;
        $order->pay_time = time();
        $order->status = OrdersEnum::STATUS_PAY;
        $order->pay_amount = $order['order_amount'];

        //自动发货
        $is_auto_delivery = config('project.is_auto_delivery', 0);
        if ($is_auto_delivery == 1) {
            DeliveryLogic::delivery($order);
        }
        $order->save();
    }

    /**
     * @param $orderSn
     * @param $extra
     * @return void
     * 购买次数或时长订单回调
     */
    public static function ordersRenew($orderSn, $extra = [])
    {
        $order = OrdersRenew::where('sn', $orderSn)->findOrEmpty();

        // 更新订单状态
        $order->transaction_id = $extra['transaction_id'] ?? '';
        $order->pay_status = PayEnum::ISPAID;
        $order->pay_time = time();
        $order->status = OrdersEnum::STATUS_FINISH;
        $order->pay_amount = $order['order_amount'];
        $order->save();

        //更新设备使用次数或时长
        $devices = GoodsDevices::where('id', '=', $order['devices_id'])->findOrEmpty();
        if (!$devices->isEmpty()) {
            // 按时长的购买是按小时购买，数据库中存分钟数
            $num = $devices[$order['function_type'] . '_num'] + ($order['lease_type'] == 1 ? $order['num'] : $order['num'] * 60);
            $devices->where('id', '=', $devices['id'])->update([$order['function_type'] . '_num' => $num]);
        }
    }

    /**
     * @param $orderSn
     * @param $extra
     * @return void
     * 预约订单回调
     */
    public static function ordersLearn($orderSn, $extra = [])
    {
        $order = OrdersLearn::where('sn', $orderSn)->findOrEmpty();

        // 更新订单状态
        $order->transaction_id = $extra['transaction_id'] ?? '';
        $order->pay_status = PayEnum::ISPAID;
        $order->pay_time = time();
        $order->status = OrdersEnum::STATUS_PAY;
        $order->pay_amount = $order['order_amount'];
        $order->save();
    }

    /**
     * @param $orderSn
     * @param $extra
     * @return void
     * 续租订单回调
     */
    public static function ordersRent($orderSn, $extra = [])
    {
        try {
            $order = OrdersRent::where('sn', $orderSn)->findOrEmpty();

            // 更新订单状态
            $order->transaction_id = $extra['transaction_id'] ?? '';
            $order->pay_status = PayEnum::ISPAID;
            $order->pay_time = time();
            $order->status = OrdersRentEnum::STATUS_SUCCESS;
            $order->pay_amount = $order['order_amount'];
            $order->save();


            //更新设备租赁到期时间
            $devices = GoodsDevices::where('id', '=', $order['devices_id'])->findOrEmpty();
            if (!$devices->isEmpty()) {
                // 更新设备的到期时间戳
                $devices->end_time += (int)($order['num'] * 30 * 24 * 60 * 60);
                $devices->save();
            }
        } catch (\Exception $e) {
            file_put_contents('./ordersRent.log', $e->getMessage() . PHP_EOL, FILE_APPEND);
        }
    }
}
