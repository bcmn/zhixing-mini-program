<?php

namespace app\common\logic;

use app\common\enum\orders\OrdersEnum;
use app\common\service\Kuaidi100Service;

/**
 * 发货逻辑
 */
class DeliveryLogic extends BaseLogic
{

    /**
     * Date: 2023/12/2 13:55
     * Notes:电子面单发货
     */
    public static function delivery($ordersInfo, $type = 1, $express = []): bool
    {
        try {
            if ($ordersInfo->isEmpty()) {
                self::$error = '订单不存在，请确认';
                return false;
            }
            if ($ordersInfo['status'] != OrdersEnum::STATUS_PAY) {
                self::$error = '订单状态异常，请确认';
                return false;
            }

            // type1 电子面单自动发货，自动打印快递单
            if ($type == 1) {
                $result = Kuaidi100Service::delivery($ordersInfo);
                $express_status = 1;
                $express_message = '发货成功';
                if ($result === false) {
                    $express_status = 2;
                    $express_message = Kuaidi100Service::$error;
                }

                if ($express_status == 1) {
                    $ordersInfo->status = OrdersEnum::STATUS_DELIVER;
                    $ordersInfo->express_company = 1;
                    $ordersInfo->express_sn = $result['kuaidinum'];
                    $ordersInfo->express_task_id = $result['taskId'];
                    $ordersInfo->express_time = time();
                }
                $ordersInfo->express_status = $express_status;
                $ordersInfo->express_message = $express_message;
                $ordersInfo->save();
            } else {
                $ordersInfo->status = OrdersEnum::STATUS_DELIVER;
                $ordersInfo->express_company = $express['express_company'];
                $ordersInfo->express_sn = $express['express_sn'];
                $ordersInfo->express_time = time();
                $ordersInfo->save();

                //打印快递单
                Kuaidi100Service::print($ordersInfo['id']);
            }

            return true;
        } catch (\Exception $e) {
            self::$error = $e->getMessage();
            return false;
        }
    }
}