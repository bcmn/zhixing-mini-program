<?php

namespace app\common\logic;

use app\common\enum\user\AccountLogEnum;
use app\common\enum\user\UserEnum;
use app\common\model\goods\Goods;
use app\common\model\orders\Orders;
use app\common\model\orders\OrdersGoods;
use app\common\model\orders\OrdersLearn;
use app\common\model\orders\OrdersRenew;
use app\common\model\orders\OrdersRent;
use app\common\model\user\User;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 用户分红逻辑
 */
class CommissionLogic extends BaseLogic
{

    /**
     * Date: 2023/12/2 13:55
     * Notes:分红结算
     */
    public static function settlement($params): void
    {
        $user_id = '';
        $amount = 0;
        $change_type = '';
        $sn = '';
        $remark = '';
        switch ($params['type']) {
            case 'orders':
                $orderInfo = Orders::where('id', '=', $params['id'])->findOrEmpty();
                if (!$orderInfo->isEmpty() && $orderInfo['is_commission'] == 0) {
                    $user_id = $orderInfo['user_id'];
                    $amount = self::getAmount(['type' => $params['type'], 'orderInfo' => $orderInfo, 'user_id' => $user_id]);
                    $change_type = AccountLogEnum::UM_INC_COMMISSION_ORDER;
                    $sn = $orderInfo['sn'];
                    $remark = '来自' . User::where('id', '=', $user_id)->value('nickname') . '的订单分佣';
                }
                break;
            case 'orderLearn':
                $orderInfo = OrdersLearn::where('id', '=', $params['id'])->findOrEmpty();
                if (!$orderInfo->isEmpty() && $orderInfo['is_commission'] == 0) {
                    $user_id = $orderInfo['user_id'];
                    $amount = self::getAmount(['type' => $params['type'], 'orderInfo' => $orderInfo, 'user_id' => $user_id]);
                    $change_type = AccountLogEnum::UM_INC_COMMISSION_LEARN;
                    $sn = $orderInfo['sn'];
                    $remark = '来自' . User::where('id', '=', $user_id)->value('nickname') . '的预约体验订单的分佣';
                }
                break;
            case 'orderRenew':
                $orderInfo = OrdersRenew::where('id', '=', $params['id'])->findOrEmpty();
                if (!$orderInfo->isEmpty() && $orderInfo['is_commission'] == 0) {
                    $user_id = $orderInfo['user_id'];
                    $amount = self::getAmount(['type' => $params['type'], 'orderInfo' => $orderInfo, 'user_id' => $user_id]);
                    $change_type = AccountLogEnum::UM_INC_COMMISSION_RENEW;
                    $sn = $orderInfo['sn'];
                    $remark = '来自' . User::where('id', '=', $user_id)->value('nickname') . '的购买次数或时长订单的分佣';
                }
                break;
            case 'orderRent':
                $orderInfo = OrdersRent::where('id', '=', $params['id'])->findOrEmpty();
                if (!$orderInfo->isEmpty() && $orderInfo['is_commission'] == 0) {
                    $user_id = $orderInfo['user_id'];
                    $amount = self::getAmount(['type' => $params['type'], 'orderInfo' => $orderInfo, 'user_id' => $user_id]);
                    $change_type = AccountLogEnum::UM_INC_COMMISSION_RENT;
                    $sn = $orderInfo['sn'];
                    $remark = '来自' . User::where('id', '=', $user_id)->value('nickname') . '的续租订单分佣';
                }
                break;
            default:
                break;
        }

        if (!empty($user_id) && !empty($amount)) {
            $commission_user_id = User::where('id', '=', $user_id)->value('first_leader');
            if (!empty($commission_user_id)) {
                AccountLogLogic::add(
                    $commission_user_id,
                    $change_type,
                    AccountLogEnum::INC,
                    $amount,
                    $sn,
                    $remark,
                    [],
                    $user_id
                );

                User::where('id', '=', $commission_user_id)->inc('user_money', $amount)->save();

                print_r($commission_user_id);
            }
        }
    }

    /**
     * @param $params
     * Date: 2024/5/7 16:01
     * Notes: 获取分红金额
     * @return float|int
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    private static function getAmount($params): float|int
    {
        $commission_user_id = User::where('id', '=', $params['user_id'])->value('first_leader');
        $commission_user_level = User::where('id', $commission_user_id)->value('level');

        $amount = 0;

        $orderInfo = $params['orderInfo'] ?? [];
        if (empty($orderInfo)) {
            return $amount;
        }

        $goods_rate = 0;
        $type = $params['type'] ?? 'orders';
        if ($type == 'orders') {
            $orderGoods = OrdersGoods::where('order_id', '=', $orderInfo['id'])->field('goods_id,total_price')->select()->toArray();
            foreach ($orderGoods as $item) {
                if ($commission_user_level == UserEnum::LEVEL_STORE) {
                    $goods_rate = Goods::where('id', $item['goods_id'])->value('percent_store');
                } elseif ($commission_user_level == UserEnum::LEVEL_PARTNER) {
                    $goods_rate = Goods::where('id', $item['goods_id'])->value('percent_partner');
                }
                $amount += $item['total_price'] * ($goods_rate / 100);
            }
        } else {
            if ($commission_user_level == UserEnum::LEVEL_STORE) {
                $goods_rate = Goods::where('id', $orderInfo['goods_id'])->value('percent_store');
            } elseif ($commission_user_level == UserEnum::LEVEL_PARTNER) {
                $goods_rate = Goods::where('id', $orderInfo['goods_id'])->value('percent_partner');
            }
            $amount += $orderInfo['total_amount'] * ($goods_rate / 100);
        }

        return $amount;
    }
}