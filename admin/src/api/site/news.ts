import request from '@/utils/request'

// 文章列表
export function articleLists(params?: any) {
    return request.get({ url: '/site.news/lists', params })
}
// 文章列表
export function articleAll(params?: any) {
    return request.get({ url: '/site.news/all', params })
}

// 添加文章
export function articleAdd(params: any) {
    return request.post({ url: '/site.news/add', params })
}

// 编辑文章
export function articleEdit(params: any) {
    return request.post({ url: '/site.news/edit', params })
}

// 删除文章
export function articleDelete(params: any) {
    return request.post({ url: '/site.news/delete', params })
}

// 文章详情
export function articleDetail(params: any) {
    return request.get({ url: '/site.news/detail', params })
}

// 文章状态
export function articleStatus(params: any) {
    return request.post({ url: '/site.news/updateStatus', params })
}
