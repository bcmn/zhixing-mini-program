import request from '@/utils/request'
/** 续租订单 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/orders.rent/lists', params }, { ignoreCancelToken: true })
}


// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/orders.rent/delete', params })
}
