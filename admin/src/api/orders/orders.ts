import request from '@/utils/request'
/** 订单 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/orders.orders/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/orders.orders/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/orders.orders/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/orders.orders/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/orders.orders/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/orders.orders/detail', params })
}

// 订单发货
export function dataDelivery(params: any) {
    return request.post({ url: '/orders.orders/delivery', params })
}

// 订单确认收货
export function dataConfirm(params: any) {
    return request.post({ url: '/orders.orders/confirm', params })
}

// 取消订单
export function dataCancel(params: any) {
    return request.post({ url: '/orders.orders/cancel', params })
}

// 打印快递
export function printOrder(params: any) {
    return request.post({ url: '/orders.orders/printOrder', params })
}

