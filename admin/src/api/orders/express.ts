import request from '@/utils/request'
/** 快递公司 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/orders.express/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/orders.express/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/orders.express/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/orders.express/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/orders.express/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/orders.express/detail', params })
}

// 全部数据
export function dataAll(params: any) {
    return request.get({ url: '/orders.express/all', params })
}
