import request from '@/utils/request'
/** 商品 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/goods.goods/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/goods.goods/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/goods.goods/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/goods.goods/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/goods.goods/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/goods.goods/detail', params })
}

// 全部数据
export function dataAll(params: any) {
    return request.get({ url: '/goods.goods/all', params })
}
