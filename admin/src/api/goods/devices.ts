import request from '@/utils/request';
/** 商品设备 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/goods.devices/lists', params }, { ignoreCancelToken: true });
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/goods.devices/add', params });
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/goods.devices/edit', params });
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/goods.devices/change', params });
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/goods.devices/delete', params });
}

// 重置
export function dataReset(params: any) {
    return request.post({ url: '/goods.devices/reset', params });
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/goods.devices/detail', params });
}

// 全部数据
export function dataAll(params: any) {
    return request.get({ url: '/goods.devices/all', params });
}
