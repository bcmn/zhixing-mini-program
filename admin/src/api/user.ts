import config from '@/config'
import request from '@/utils/request'

// 登录
export function login(params: Record<string, any>) {
    return request.post({ url: '/login/account', params: { ...params, terminal: config.terminal } })
}

// 退出登录
export function logout() {
    return request.post({ url: '/login/logout' })
}

// 用户信息
export function getUserInfo() {
    return request.get({ url: '/auth.admin/mySelf' })
}

// 编辑管理员信息
export function setUserInfo(params: any) {
    return request.post({ url: '/auth.admin/editSelf', params })
}


// 用户列表
export function getUserList(params: any) {
    return request.get({ url: '/user.user/lists', params }, { ignoreCancelToken: true })
}

// 用户详情
export function getUserDetail(params: any) {
    return request.get({ url: '/user.user/detail', params })
}

// 用户编辑
export function userEdit(params: any) {
    return request.post({ url: '/user.user/edit', params })
}

// 用户编辑
export function adjustMoney(params: any) {
    return request.post({ url: '/user.user/adjustMoney', params })
}

// 所有用户列表
export function getUserAll(params: any) {
    return request.get({ url: '/user.user/all', params })
}