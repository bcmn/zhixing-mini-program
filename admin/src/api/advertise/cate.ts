import request from '@/utils/request'
/** 广告类目 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/advertise.cate/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/advertise.cate/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/advertise.cate/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/advertise.cate/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/advertise.cate/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/advertise.cate/detail', params })
}

// 全部数据
export function dataAll(params: any) {
    return request.get({ url: '/advertise.cate/all', params })
}
