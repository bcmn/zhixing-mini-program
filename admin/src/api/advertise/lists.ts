import request from '@/utils/request'
/** 广告内容 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/advertise.advertise/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/advertise.advertise/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/advertise.advertise/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/advertise.advertise/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/advertise.advertise/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/advertise.advertise/detail', params })
}
