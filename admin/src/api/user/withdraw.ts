import request from '@/utils/request';
/** 提现管理 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/user.withdraw/lists', params }, { ignoreCancelToken: true });
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/user.withdraw/change', params });
}
