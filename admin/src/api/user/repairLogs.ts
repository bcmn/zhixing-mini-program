import request from '@/utils/request'
/** 维修申请 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/user.repairLogs/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/user.repairLogs/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/user.repairLogs/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/user.repairLogs/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/user.repairLogs/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/user.repairLogs/detail', params })
}
