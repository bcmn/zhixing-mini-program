import request from '@/utils/request'
/** 加盟申请 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/user.apply/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/user.apply/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/user.apply/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/user.apply/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/user.apply/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/user.apply/detail', params })
}
