import request from '@/utils/request'
/** 意见反馈 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/user.feedback/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/user.feedback/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/user.feedback/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/user.feedback/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/user.feedback/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/user.feedback/detail', params })
}
