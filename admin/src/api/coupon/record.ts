import request from '@/utils/request'
/** 优惠券领取记录列表 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/coupon.record/lists', params }, { ignoreCancelToken: true })
}

