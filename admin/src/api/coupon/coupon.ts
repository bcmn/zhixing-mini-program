import request from '@/utils/request'
/** 优惠券 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/coupon.coupon/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/coupon.coupon/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/coupon.coupon/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/coupon.coupon/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/coupon.coupon/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/coupon.coupon/detail', params })
}

// 全部数据
export function dataAll(params: any) {
    return request.get({ url: '/coupon.coupon/all', params })
}

// 发放优惠券
export function dataGrant(params: any) {
    return request.post({ url: '/coupon.coupon/grant', params })
}
