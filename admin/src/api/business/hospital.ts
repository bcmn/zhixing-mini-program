import request from '@/utils/request'
/** 服务项目 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/business.hospital/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/business.hospital/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/business.hospital/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/business.hospital/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/business.hospital/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/business.hospital/detail', params })
}
