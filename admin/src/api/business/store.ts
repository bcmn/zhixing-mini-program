import request from '@/utils/request'
/** 服务项目 */

// 列表
export function dataLists(params: any) {
    return request.get({ url: '/business.store/lists', params }, { ignoreCancelToken: true })
}

// 添加
export function dataAdd(params: any) {
    return request.post({ url: '/business.store/add', params })
}

// 编辑
export function dataEdit(params: any) {
    return request.post({ url: '/business.store/edit', params })
}

// 更新字段
export function dataChange(params: any) {
    return request.post({ url: '/business.store/change', params })
}

// 删除
export function dataDelete(params: any) {
    return request.post({ url: '/business.store/delete', params })
}

// 详情
export function dataDetail(params: any) {
    return request.get({ url: '/business.store/detail', params })
}
